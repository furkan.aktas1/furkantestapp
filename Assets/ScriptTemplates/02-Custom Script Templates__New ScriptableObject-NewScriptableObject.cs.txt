using UnityEngine;

namespace GybeGames.
{
    [CreateAssetMenu(fileName = "#SCRIPTNAME#", menuName = "ScriptableObject/#SCRIPTNAME#")]
    public class #SCRIPTNAME# : ScriptableObject
    {
       
    }
}
