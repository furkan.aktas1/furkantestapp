﻿using DG.Tweening;
using GybeGames.Systems;
using GybeGames.Systems.AudioSystems.MusicSystem;
using GybeGames.Systems.AudioSystems.SoundSystem;
using GybeGames.Systems.PopUp;
using GybeGames.Systems.Vibration;
using Sirenix.OdinInspector;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace GybeGames.Settings
{
    public class SettingsPopUp : BasePopUp
    {

        protected override void Initialize()
        {
            base.Initialize();
            contentsRectTransform.localScale = Vector3.zero;
            InitializeVibration();
            InitializeSound();
            InitializeMusic();
            InitializePrivacyPolicy();
            InitializeRestorePurchase();
        }

        protected override IEnumerator AppearProgress()
        {
            bool openning = false;
            contentsRectTransform.DOScale(1, 0.3f).SetEase(Ease.OutBack).OnComplete(() => openning = true);
            yield return new WaitUntil(() => openning);
        }


        #region Vibration
        [Title("Vibration")]
        [SerializeField] private Toggle vibrationToggle = null;

        private void InitializeVibration()
        {
            vibrationToggle.isOn = Gybe.GetSystem<VibrationSystem>().IsVibrationEnabled;
            vibrationToggle.onValueChanged.AddListener((x) => ChangeVibrationStatus());
        }

        private void ChangeVibrationStatus()
        {
            bool isVibrationOn = Gybe.GetSystem<VibrationSystem>().ChangeVibrationStatus();

            Debug.Log($"Vibraiton status changed to: {isVibrationOn}");
        }

        #endregion

        #region Sound

        [Title("Sound")]
        [SerializeField] private Toggle soundToggle = null;

        private void InitializeSound()
        {
            soundToggle.isOn = Gybe.GetSystem<SoundSystem>().IsSoundEnabled;
            soundToggle.onValueChanged.AddListener((x) => ChangeSoundStatus());
        }

        private void ChangeSoundStatus()
        {
            bool isSoundOn = Gybe.GetSystem<SoundSystem>().ChangeSoundStatus();
            Debug.Log($"Music sound status changed to: {isSoundOn}");
        }

        #endregion

        #region Music

        [Title("Music")]
        [SerializeField] private Toggle musicToggle = null;

        private void InitializeMusic()
        {

            musicToggle.isOn = Gybe.GetSystem<MusicSystem>().IsMusicEnabled;
            musicToggle.onValueChanged.AddListener((x) => ChangeMusicStatus());
        }

        private void ChangeMusicStatus()
        {
            bool isMusicOn = Gybe.GetSystem<MusicSystem>().ChangeMusicStatus();

            Debug.Log($"Music sound status changed to: {isMusicOn}");
        }

        #endregion

        #region

        [Title("Privacy Policy")]
        [SerializeField] private Button privacyPolicyButton = null;

        private void InitializePrivacyPolicy()
        {
            privacyPolicyButton.onClick.AddListener(() => OpenPrivacyPolicy());
        }

        private void OpenPrivacyPolicy()
        {

        }

        #endregion

        #region Restore Purchase

        [Title("Restore")]
        [SerializeField] private Button restorePuchaseButton = null;
        private void InitializeRestorePurchase()
        {
            restorePuchaseButton.onClick.AddListener(() => RestorePurchase());
        }


        private void RestorePurchase()
        {

        }

        #endregion

    }
}
