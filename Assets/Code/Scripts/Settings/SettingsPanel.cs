﻿using GybeGames.Systems;
using GybeGames.Systems.PopUp;
using GybeGames.Utility.Extentions;
using UnityEngine;

namespace GybeGames.Settings
{
    public class SettingsPanel : MonoBehaviour
    {
        public void _OpenSettingsPopUp()
        {
            Gybe.GetSystem<PopUpSystem>().ForceGet<SettingsPopUp>().Show();
        }
    }
}
