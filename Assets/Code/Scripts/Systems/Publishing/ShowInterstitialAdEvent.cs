﻿using GybeGames.Systems.EventSystem;

namespace GybeGames.Systems.Publishing
{
    public class ShowInterstitialAdEvent : CustomEvent
    {
        public static ShowInterstitialAdEvent Create()
        {
            return new ShowInterstitialAdEvent();
        }
    }
}
