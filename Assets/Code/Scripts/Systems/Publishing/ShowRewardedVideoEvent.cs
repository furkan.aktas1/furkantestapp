﻿using GybeGames.Systems.EventSystem;
using System;

namespace GybeGames.Systems.Publishing
{
    public class ShowRewardedVideoEvent : CustomEvent
    {
        public Action OnCompletedCallback { get; private set; }

        public Action OnFailedCallback { get; private set; }

        public Action OnSkipCallback { get; private set; }

        public Action OnEditorCallback { get; private set; }

        public static ShowRewardedVideoEvent Create(Action onCompletedCallback, Action onFailedCallback, Action onSkipCallback, Action onEditorCallback = null)
        {
            ShowRewardedVideoEvent showRewardedVideoEvent = new ShowRewardedVideoEvent();

            showRewardedVideoEvent.OnCompletedCallback = onCompletedCallback;

            showRewardedVideoEvent.OnFailedCallback = onFailedCallback;

            showRewardedVideoEvent.OnSkipCallback = onSkipCallback;

            showRewardedVideoEvent.OnEditorCallback = onEditorCallback;

            return showRewardedVideoEvent;
        }
    }
}
