﻿using GybeGames.Systems.EventSystem;
using System;

namespace GybeGames.Systems.Publishing
{
    public class IsRewardedAdReadyEvent : CustomEvent
    {
        public Action<bool> OnRewardedVideoReadyCallback { get; private set; }

        public static IsRewardedAdReadyEvent Create(Action<bool> onRewardedVideoReadyCallback)
        {
            IsRewardedAdReadyEvent rewardedAdReadyEvent = new IsRewardedAdReadyEvent();

            rewardedAdReadyEvent.OnRewardedVideoReadyCallback = onRewardedVideoReadyCallback;

            return rewardedAdReadyEvent;
        }
    }
}

