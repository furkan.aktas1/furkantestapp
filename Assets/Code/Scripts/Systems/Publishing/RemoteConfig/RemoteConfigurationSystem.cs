﻿using GybeGames.Systems;
using GybeGames.Systems.Levels;
using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GybeGames.Systems.Publishing.RemoteConfig
{
    /// <summary>
    /// Remote configuraiton variables & funcitons will be used from there.
    /// </summary>
    public class RemoteConfigurationSystem : BaseSystem<RemoteConfigurationSystem>
    {
        private LevelSystem levelSystem { get => _levelSystem ?? (_levelSystem = Gybe.GetSystem<LevelSystem>()); }

        private LevelSystem _levelSystem = null;

        public int RateUsLevel { get; set; } = 2;

        public static T TryGetJson<T>(string json, T defaultValue)
        {
            if (json == null || json == string.Empty)
            {
                return defaultValue;
            }
            T deserialized;
            try
            {
                deserialized = JsonConvert.DeserializeObject<T>(json);
            }
            catch
            {
                deserialized = defaultValue;
            }
            return deserialized;
        }
    }
}

