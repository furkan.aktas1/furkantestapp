﻿//#define GYBE_FACEBOOK
//#define GYBE_GAMEANALYTICS
//#define GYBE_ELEPHANT
//#define GYBE_ELEPHANT_MONETIZATION
//#define GYBE_FIREBASE

using GybeGames.Systems.EventSystem;
using GybeGames.Systems.GameStates.Events;
using GybeGames.Systems.Levels;
using GybeGames.Systems.Levels.Data;
using GybeGames.Systems.Levels.Events;
using GybeGames.Systems.Publishing.RemoteConfig;
using GybeGames.Systems.Windows;
using GybeGames.Systems.Windows.MainUIWindows;
using GybeGames.UI.GameOpenLoadings;
using GybeGames.Utility.Extentions;
using System;
using UnityEngine;
#if GYBE_FACEBOOK
using Facebook.Unity;
#endif
#if GYBE_GAMEANALYTICS
using GameAnalyticsSDK;
#endif
namespace GybeGames.Systems.Publishing
{
    public partial class FrameworkSystem : BaseSystem<FrameworkSystem>
#if GYBE_GAMEANALYTICS
, IGameAnalyticsATTListener
#endif
    {
        [SerializeField] private bool editorAdIsReady = true;

        private LevelSystem levelSystem { get => _levelSystem ?? (_levelSystem = Gybe.GetSystem<LevelSystem>()); }
        private RemoteConfigurationSystem remoteConfigurationSystem { get => _remoteConfigurationSystem ?? (_remoteConfigurationSystem = Gybe.GetSystem<RemoteConfigurationSystem>()); }

        private LevelSystem _levelSystem = null;
        private RemoteConfigurationSystem _remoteConfigurationSystem = null;


        public override void Initialize()
        {
            EventManager.Send(SetGameOpenLoadingPercentageEvent.Create(25));
            base.Initialize();
            InitiliazeMonetization();
            EventManager.Send(SetGameOpenLoadingPercentageEvent.Create(40));
            SetRemoteConfigs();
            EventManager.Send(SetGameOpenLoadingPercentageEvent.Create(60));
            ListenEvents();
            InitializeFirebase();
            InitializeFacebook();
            InitializeGameAnalytics();
            EventManager.Send(SetGameOpenLoadingPercentageEvent.Create(80));
        }

        public override void DeInitialize()
        {
            base.DeInitialize();
            RemoveEventListeners();

        }

        private void InitializeGameAnalytics()
        {
#if GYBE_GAMEANALYTICS
            if (Application.platform == RuntimePlatform.IPhonePlayer)
            {
                GameAnalytics.RequestTrackingAuthorization(this);
            }
            else
            {
                GameAnalytics.Initialize();
            }
#endif
        }

        private void InitializeFacebook()
        {
#if GYBE_FACEBOOK
            if (!FB.IsInitialized)
            {
                FB.Init(OnFbInitComplete);
            }
            else
            {
                FB.ActivateApp();
            }
#endif
        }

        private void InitializeFirebase()
        {
#if GYBE_FIREBASE
            FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task =>
            {
                FirebaseAnalytics.SetAnalyticsCollectionEnabled(true);

                var dependencyStatus = task.Result;
                if (dependencyStatus == Firebase.DependencyStatus.Available)
                {
                    // Create and hold a reference to your FirebaseApp,
                    // where app is a Firebase.FirebaseApp property of your application class.
                    // Crashlytics will use the DefaultInstance, as well;
                    // this ensures that Crashlytics is initialized.
                    Firebase.FirebaseApp app = Firebase.FirebaseApp.DefaultInstance;

                    // Set a flag here for indicating that your project is ready to use Firebase.
                }
                else
                {
                    UnityEngine.Debug.LogError(System.String.Format(
                        "Could not resolve all Firebase dependencies: {0}", dependencyStatus));
                    // Firebase Unity SDK is not safe to use here.
                }
            });

            //MoPubManager.OnImpressionTrackedEventBg += OnImpressionTrackedEvent;
#endif
        }

        private void ListenEvents()
        {
            EventManager.RegisterHandler<LevelLoadedEvent>(OnLevelLoaded);
            EventManager.RegisterHandler<GameWonEvent>(OnGameWon);
            EventManager.RegisterHandler<GamePlayingEvent>(OnGamePlaying);
            EventManager.RegisterHandler<GameFailedEvent>(OnGameFailed);
            EventManager.RegisterHandler<ShowInterstitialAdEvent>(OnShowInterstitial);
            EventManager.RegisterHandler<ShowRewardedVideoEvent>(OnShowRewardedVideo);
            EventManager.RegisterHandler<IsRewardedAdReadyEvent>(OnRewardedVideoReady);
        }

        private void RemoveEventListeners()
        {
            EventManager.UnregisterHandler<LevelLoadedEvent>(OnLevelLoaded);
            EventManager.UnregisterHandler<GameWonEvent>(OnGameWon);
            EventManager.UnregisterHandler<GamePlayingEvent>(OnGamePlaying);
            EventManager.UnregisterHandler<GameFailedEvent>(OnGameFailed);
            EventManager.UnregisterHandler<ShowInterstitialAdEvent>(OnShowInterstitial);
            EventManager.UnregisterHandler<ShowRewardedVideoEvent>(OnShowRewardedVideo);
            EventManager.UnregisterHandler<IsRewardedAdReadyEvent>(OnRewardedVideoReady);
        }

        private void SetRemoteConfigs()
        {

        }


        #region Level Events

        private int currentLevelIndex;
        private int highestLevelNumber;
        private string levelName;

        private void OnLevelLoaded(LevelLoadedEvent levelLoadedEvent)
        {
            currentLevelIndex = levelSystem.CurrentLevelNumber;
            highestLevelNumber = levelSystem.HighestLevelNumber;
            levelName = ((LevelData)levelLoadedEvent.LevelData).name;
        }

        private void OnGamePlaying(GamePlayingEvent gamePlayingEvent)
        {
#if GYBE_GAMEANALYTICS
            GameAnalytics.NewProgressionEvent(GAProgressionStatus.Start, levelName, highestLevelNumber);
#endif
#if GYBE_ELEPHANT
            Elephant.LevelStarted(highestLevelNumber);
#endif
        }

        private void OnGameWon(GameWonEvent gameWonEvent)
        {
#if GYBE_GAMEANALYTICS
            GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, levelName, highestLevelNumber);
#endif
#if GYBE_ELEPHANT
            Elephant.LevelCompleted(highestLevelNumber);
#endif
        }

        private void OnGameFailed(GameFailedEvent gameFailedEvent)
        {
#if GYBE_GAMEANALYTICS
            GameAnalytics.NewProgressionEvent(GAProgressionStatus.Fail, levelName, highestLevelNumber);
#endif
#if GYBE_ELEPHANT
            Elephant.LevelFailed(highestLevelNumber);
#endif
        }


        #endregion


        #region Monetization

#if UNITY_IOS
        private readonly string[] _bannerAdUnits =
            { "" };

        private readonly string[] _interstitialAdUnits =
            { ""};

        private readonly string[] _rewardedVideoAdUnits =
            { "" };

        private readonly string[] _rewardedRichMediaAdUnits = { };
#elif UNITY_ANDROID || UNITY_EDITOR
        private readonly string[] _bannerAdUnits = { "" };
        private readonly string[] _interstitialAdUnits = { "" };
        private readonly string[] _rewardedVideoAdUnits = { "" };
        private readonly string[] _rewardedRichMediaAdUnits = { "" }; // bunun değişmesi lazım
#endif
        private int interstitialFrequency = 30;
        private int interstitialStartLevel = 2;
        private float lastTimeInterstitialShown = 0;

        private void InitiliazeMonetization()
        {
            lastTimeInterstitialShown = Time.time;
#if GYBE_ELEPHANT_MONETIZATION
            RLAdvertisementManager.Instance.init(_rewardedVideoAdUnits, _bannerAdUnits, _interstitialAdUnits);
            RLAdvertisementManager.Instance.rewardedAdResultCallback = RewardedAdResultCallback;
            RLAdvertisementManager.OnRollicAdsSdkInitializedEvent += OnAdsInitialized;
#endif
        }

        private void OnAdsInitialized(string obj)
        {
#if GYBE_ELEPHANT_MONETIZATION
            RLAdvertisementManager.OnRollicAdsInterstitialShownEvent += OnInsterstitialShown;
            RLAdvertisementManager.OnRollicAdsAdLoadedEvent += OnBannerLoaded;
            RLAdvertisementManager.Instance.loadBanner();
#endif
        }

        private void OnBannerLoaded(string obj)
        {
#if GYBE_ELEPHANT_MONETIZATION
            RLAdvertisementManager.Instance.showBanner();
#endif
        }

        private void OnInsterstitialShown(string obj)
        {
            lastTimeInterstitialShown = Time.time;
        }

#if GYBE_ELEPHANT_MONETIZATION
        private Action rewardedCompletedCallback = null;
        private Action rewardedSkippedCallback = null;
        private Action rewardedFailedCompletedCallback = null;
#endif

        private void OnShowRewardedVideo(ShowRewardedVideoEvent showRewardedVideoEvent)
        {
            if (Application.isEditor)
            {
                showRewardedVideoEvent.OnEditorCallback?.Invoke();
                return;
            }
#if GYBE_ELEPHANT_MONETIZATION
            if (RLAdvertisementManager.Instance.isRewardedVideoAvailable())
            {
                rewardedCompletedCallback = showRewardedVideoEvent.OnCompletedCallback;
                rewardedFailedCompletedCallback = showRewardedVideoEvent.OnFailedCallback;
                rewardedSkippedCallback = showRewardedVideoEvent.OnSkipCallback;
                RLAdvertisementManager.Instance.showRewardedVideo();
            }
            else
            {
                ShowAdIsNotReadyToastMessage();
                showRewardedVideoEvent.OnFailedCallback?.Invoke();
            }
#endif
        }

        private void OnRewardedVideoReady(IsRewardedAdReadyEvent isRewardedAdReadyEvent)
        {

            if (Application.isEditor)
            {
                if (!editorAdIsReady)
                {
                    ShowAdIsNotReadyToastMessage();
                }
                isRewardedAdReadyEvent.OnRewardedVideoReadyCallback(editorAdIsReady);
                return;
            }
#if GYBE_ELEPHANT_MONETIZATION
            bool isAdReady = RLAdvertisementManager.Instance.isRewardedVideoAvailable();
            if (!isAdReady)
            {
                ShowAdIsNotReadyToastMessage();
            }
            isRewardedAdReadyEvent.OnRewardedVideoReadyCallback?.Invoke(isAdReady);
#endif
        }

        private void ShowAdIsNotReadyToastMessage()
        {
            ToastMessageWindow toastMessageWindow = Gybe.GetSystem<WindowSystem>().ForceGet<ToastMessageWindow>();
            toastMessageWindow.Show();
            toastMessageWindow.ShowPanel("Ad is not ready!");
        }
#if GYBE_ELEPHANT_MONETIZATION
        private void RewardedAdResultCallback(RLRewardedAdResult rLRewardedAdResult)
        {
            switch (rLRewardedAdResult)
            {
                case RLRewardedAdResult.Failed:
                    rewardedFailedCompletedCallback?.Invoke();
                    break;
                case RLRewardedAdResult.Skipped:
                    rewardedSkippedCallback?.Invoke();
                    break;
                case RLRewardedAdResult.Finished:
                    rewardedCompletedCallback?.Invoke();
                    lastTimeInterstitialShown = Time.time;
                    break;
            }
        }
#endif

        private void OnShowInterstitial(ShowInterstitialAdEvent showInterstitialAdEvent)
        {
            if (Time.time - lastTimeInterstitialShown < interstitialFrequency)
            {
                return;
            }
            if (highestLevelNumber < interstitialStartLevel)
            {
                return;
            }
#if GYBE_ELEPHANT_MONETIZATION
            if (RLAdvertisementManager.Instance.isInterstitialReady())
            {
                RLAdvertisementManager.Instance.showInterstitial();
                  lastTimeInterstitialShown = Time.time;
            }
#endif
        }

        #endregion
        //        private void OnImpressionTrackedEvent(string adUnitId, MoPub.ImpressionData impressionData)
        //        {
        //            var publisherRevenue = impressionData.PublisherRevenue != null ?
        //                (double)impressionData.PublisherRevenue : 0;

        //            var impressionParameters = new[] {
        //            new Parameter("ad_platform", "MoPub"),
        //            new Parameter("ad_source", impressionData.NetworkName),
        //            new Parameter("ad_unit_name", impressionData.AdUnitName),
        //            new Parameter("ad_format", impressionData.AdUnitFormat),
        //            new Parameter("value", publisherRevenue),
        //            new Parameter("currency", impressionData.Currency),
        //            new Parameter("precision", impressionData.Precision)
        //        };
        //#if GYBE_FIREBASE
        //            FirebaseAnalytics.LogEvent("ad_impression", impressionParameters);
        //            FirebaseAnalytics.LogEvent("custom_ad_impression", impressionParameters);
        //#endif
        //        }

#if GYBE_FACEBOOK
        private void OnFbInitComplete()
        {
            if (FB.IsInitialized)
            {
                FB.ActivateApp();
            }
            else
            {
                Debug.Log("Failed to Initialize the Facebook SDK");
            }
        }
#endif

#if GYBE_GAMEANALYTICS
        public void GameAnalyticsATTListenerNotDetermined()
        {
            GameAnalytics.Initialize();
        }
        public void GameAnalyticsATTListenerRestricted()
        {
            GameAnalytics.Initialize();
        }
        public void GameAnalyticsATTListenerDenied()
        {
            GameAnalytics.Initialize();
        }
        public void GameAnalyticsATTListenerAuthorized()
        {
            GameAnalytics.Initialize();
        }
#endif
    }

}

