﻿using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Lean.Pool;
using Sirenix.OdinInspector;
using GybeGames.Utility.Extentions;
using GybeGames.Systems.Feedback.Emoji;
using GybeGames.Systems.Feedback.Text;
using GybeGames.Systems.Windows;
using GybeGames.Systems.Windows.MainUIWindows;

namespace GybeGames.Systems.Feedback
{
    [DisallowMultipleComponent]
    public class FeedbackSystem : BaseSystem<FeedbackSystem>
    {
        [Title("References")]
        [SerializeField] private List<BaseFeedback> feedbackPrefabs = new List<BaseFeedback>();

        public Canvas Canvas { get => canvas ?? (canvas = Gybe.GetSystem<WindowSystem>().Get<StaticMainCanvas>().Canvas); }

        private Canvas canvas = null;

        public T Create<T>(Canvas canvas = null, IFeedback prefab = null) where T : IFeedback
        {
            var feedback = default(T);
            prefab = prefab ?? feedbackPrefabs.OfType<T>().FirstOrDefault();
            var canvasToParent = canvas ?? Canvas;

            if (prefab == null)
            {
                Debug.LogError("Feedback prefab can not be found on feedbackPrefabs List. Please add prefab to list on FeedbackService!");
                return feedback;
            }

            feedback = (T)LeanPool.Spawn((BaseFeedback)prefab, canvasToParent.transform, false).GetComponent<IFeedback>();
            feedback?.RectTransform.SetAsLastSibling();
            return feedback;
        }

#if UNITY_EDITOR
        [Title("Examples")]
        [Button()]
        public void Editor_ShowShyEmoji()
        {
            Transform transform = GameObject.Find("Cube").transform;
            Create<EmojiFeedback>().SetEmojiType(EmojiType.Shy).SetAppearPosition(transform.position).SetRotationAngle(35).Show();
        }
        [Button()]
        public void Editor_ShowTextFeedback()
        {
            Transform transform = GameObject.Find("Cube").transform;
            //Create<TextFeedback>().SetAppearPosition(transform.position).Show();
            TextFeedback tf = Create<TextFeedback>();
            tf.Color = Color.green;
            tf.SetAppearPosition(transform.position).Show();
        }
#endif
    }
}
