﻿using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using DG.Tweening;

namespace GybeGames.Systems.Feedback.Text
{
    public class TextFeedback : BaseFeedback
    {

        [Title("Text Feedback References")]
        [SerializeField] protected TextMeshProUGUI textMesh;

        public string Text { get; set; }

        public float CharacterSpacing { get; set; }
        public Color Color { get; set; }

        protected override void SetDefaults()
        {
            base.SetDefaults();
            Text = "Feedback";
            CharacterSpacing = 0f;
            Color = Color.black;
        }

        protected override void OnPreAppear()
        {
            base.OnPreAppear();
            textMesh.SetText(Text);
            textMesh.color = Color;
        }

        protected override void OnAppearingComplete()
        {
            base.OnAppearingComplete();
            DOTween.To(() => textMesh.characterSpacing, x => textMesh.characterSpacing = x, CharacterSpacing, Duration).SetId(content).OnComplete(() => textMesh.characterSpacing = 0f); ;
        }

        public void SetText(string text)
        {
            this.Text = text;
        }
    }
}