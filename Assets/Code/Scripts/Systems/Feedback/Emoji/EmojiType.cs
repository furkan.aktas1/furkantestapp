﻿namespace GybeGames.Systems.Feedback.Emoji
{
    public enum EmojiType
    {
        None = 0,
        Shy = 1,
        LipOutFace = 2,
        HeartEyes = 3,
        GlassFace = 4,
        SphereEyeLaugh = 5,
        SlitEye = 6,
        CrySmiling = 7,
        ElipticalEye = 8,
        SlitEye2 = 9,
        SweatingFace = 10,
        LoudCrying = 11,
        OneEyeSlit = 12,
        Muck = 13,
        RollCrySmiling = 14,
        SimpleSmile = 15,
        Sad = 16
    }
}