﻿using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;

namespace GybeGames.Systems.Feedback.Emoji
{
    public class EmojiFeedback : BaseFeedback
    {
        [Title("Emoji Feedback References")]
        [SerializeField] Image image = null;
        [SerializeField] private EmojiSpriteData emojiSpriteData = null;

        private EmojiType emojiType = EmojiType.None;

        public EmojiFeedback SetEmojiType(EmojiType emojiType)
        {
            this.emojiType = emojiType;
            return this;
        }

        protected override void SetDefaults()
        {
            base.SetDefaults();
            if (emojiType == EmojiType.None)
                isInitialized = false;
        }

        protected override void OnPreAppear()
        {
            base.OnPreAppear();
            Debug.Log(emojiType);
            image.sprite = emojiSpriteData.GetSprite(emojiType);
        }


    }
}
