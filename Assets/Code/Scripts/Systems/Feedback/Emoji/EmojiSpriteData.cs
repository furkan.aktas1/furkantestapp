﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace GybeGames.Systems.Feedback.Emoji
{
    [CreateAssetMenu(fileName = "EmojiSpriteData", menuName = "ScriptableObject/Feedback/EmojiSpriteData")]
    public class EmojiSpriteData : ScriptableObject
    {
        [SerializeField] private List<EmojiDetail> emojiDetails = new List<EmojiDetail>();

        public Sprite GetSprite(EmojiType emojiType)
        {
            return emojiDetails.FirstOrDefault(p => p.type == emojiType).emojiSprite;
        }
    }
}
