﻿using System;
using UnityEngine;

namespace GybeGames.Systems.Feedback.Emoji
{
    [Serializable]
    public struct EmojiDetail
    {
        public Sprite emojiSprite;
        public EmojiType type;
    }
}

