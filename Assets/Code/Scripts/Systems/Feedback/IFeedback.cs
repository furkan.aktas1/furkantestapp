﻿using GybeGames.Systems.Windows;
using UnityEngine;

namespace GybeGames.Systems.Feedback
{
    public interface IFeedback : IWindow
    {
        float Duration { get; set; }
        float Scale { get; set; }
        float RotationAngle { get; set; }
        int RotationRepeat { get; set; }
        Vector3 AppearPosition { get; set; }
        Vector2 DisappearAncOffset { get; set; }
        bool HasFadeOffDisappear { get; set; }
    }
}
