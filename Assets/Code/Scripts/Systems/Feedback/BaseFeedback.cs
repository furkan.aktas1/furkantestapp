﻿using DG.Tweening;
using GybeGames.Systems.Windows;
using GybeGames.Utility.Extentions;
using Lean.Pool;
using Sirenix.OdinInspector;
using System.Collections;
using UnityEngine;

namespace GybeGames.Systems.Feedback
{
    public class BaseFeedback : BaseWindow, IFeedback, IPoolable
    {
        [Title("Base Feedback References")]
        [SerializeField] protected RectTransform content = null;

        public float Duration { get => duration; set => duration = value; }
        public float Scale { get => scale; set => scale = value; }
        public float RotationAngle { get => rotationAngle; set => rotationAngle = value; }
        public int RotationRepeat { get => rotationRepeat; set => rotationRepeat = value; }
        public Vector3 AppearPosition { get => appearPosition; set => appearPosition = value; }
        public Vector2 DisappearAncOffset { get => disappearAncOffset; set => disappearAncOffset = value; }
        public bool HasFadeOffDisappear { get => hasFadeOffDisappear; set => hasFadeOffDisappear = value; }

        private Vector2 disappearAncPosition { get => DisappearAncOffset + appearAncPosition; }

        private float duration = 0.25f;
        private float scale = 1;
        private float rotationAngle = 30f;
        private int rotationRepeat = 5;
        private Vector3 appearPosition = Vector3.zero;
        private Vector2 appearAncPosition = Vector2.zero;
        private Vector2 disappearAncOffset = Vector3.up * 50;
        private bool hasFadeOffDisappear = true;
        protected bool isInitialized = false;

        protected override void Initialize()
        {
            base.Initialize();
            SetDefaults();
            isInitialized = true;
        }

        protected virtual void SetDefaults()
        {
            CanvasGroup.alpha = 1f;
            content.localScale = Vector3.zero;
        }

        protected override void OnPreAppear()
        {
            base.OnPreAppear();
            DOTween.Kill(content);
            RotationAngle *= (Random.Range(0, 2) == 0 ? 1 : -1);
            SetAppearPosition();
        }

        private void SetAppearPosition()
        {
            RectTransform.position = AppearPosition;
            RectTransform.anchoredPosition3D = RectTransform.anchoredPosition3D.ZeroZ();
            appearAncPosition = RectTransform.anchoredPosition;
        }

        protected override IEnumerator AppearProgress()
        {
            var sequence = DOTween.Sequence().SetId(content);

            sequence.Append(content.DOScale(Vector3.one * Scale, .25f).SetEase(Ease.OutBack));
            float duration = Duration / Mathf.Clamp(RotationRepeat, 1, int.MaxValue);

            if (!HasFadeOffDisappear)
            {
                sequence.Join(content.DOLocalRotate(Vector3.forward * RotationAngle, duration * .5f).SetEase(Ease.InOutSine).SetDelay(.1f));
                sequence.Append(content.DOLocalRotate(Vector3.forward * -RotationAngle, duration).SetLoops(RotationRepeat, LoopType.Yoyo).SetEase(Ease.InOutSine));
                sequence.Append(content.DOLocalRotate(Vector3.zero, duration * .5f).SetEase(Ease.InOutSine));
                sequence.Append(content.DOScale(Vector3.zero, .25f).SetEase(Ease.InBack));
            }
            else
            {
                sequence.Join(content.DOLocalRotate(Vector3.forward * RotationAngle, duration * .5f).SetEase(Ease.InOutSine).SetDelay(.1f));
                sequence.Append(content.DOLocalRotate(Vector3.forward * -RotationAngle, duration).SetLoops(RotationRepeat, LoopType.Yoyo).SetEase(Ease.InOutSine));
                sequence.Join(content.DOAnchorPos(disappearAncPosition, RotationRepeat * duration + .5f).SetEase(Ease.OutSine));
                sequence.Join(CanvasGroup.DOFade(0f, RotationRepeat * duration + .5f));
            }

            yield return sequence.WaitForCompletion();
        }

        protected override void OnAppearingComplete()
        {
            LeanPool.Despawn(this);
        }

        public void OnSpawn()
        {
            DOTween.Kill(content);
            SetDefaults();
        }

        public void OnDespawn()
        {
            DOTween.Kill(content);
            SetDefaults();
        }
    }
}