using System;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace GybeGames.Systems.AddressableSystem.AssetReferenceTypes
{
    /// <summary>
    /// Asset referance for Material
    /// </summary>
    [Serializable]
    public class AssetReferenceMaterial : AssetReferenceT<Material>
    {
        public AssetReferenceMaterial(string guid) : base(guid) { }
    }
}
