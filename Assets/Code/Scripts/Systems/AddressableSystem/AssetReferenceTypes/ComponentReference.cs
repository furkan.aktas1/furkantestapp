using UnityEditor;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace GybeGames.Systems.AddressableSystem.AssetReferenceTypes
{
    /// <summary>
    /// Generic class to use Monobehavior inherited classes as AssetReferance
    /// </summary>
    /// <typeparam name="TComponent"></typeparam>
    public class ComponentReference<TComponent> : AssetReference
    {
        private AsyncOperationHandle<TComponent> _asyncOperationHandle;

        public ComponentReference(string guid) : base(guid)
        {
        }

        /// <summary>
        /// Creater chain referance to load component referance as GameObject
        /// </summary>
        /// <returns></returns>
        public AsyncOperationHandle LoadAssetAsync()
        {
            _asyncOperationHandle =
                Addressables.ResourceManager.CreateChainOperation<TComponent, GameObject>(
                    base.LoadAssetAsync<GameObject>(), GameObjectReady);
            return _asyncOperationHandle;
        }

        /// <summary>
        /// Validate asset as GameObject
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool ValidateAsset(Object obj)
        {
            var go = obj as GameObject;
            return go != null && go.GetComponent<TComponent>() != null;
        }

        /// <summary>
        /// Validate asset as GameObject
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public override bool ValidateAsset(string path)
        {
#if UNITY_EDITOR
            //this load can be expensive...
            var go = AssetDatabase.LoadAssetAtPath<GameObject>(path);
            return go != null && go.GetComponent<TComponent>() != null;
#else
            return false;
#endif
        }

        /// <summary>
        /// Override release asset because it creates chain operation
        /// </summary>
        public override void ReleaseAsset()
        {
            Addressables.Release(_asyncOperationHandle);
            base.ReleaseAsset();
        }

        /// <summary>
        /// Release instantiated instance & GameObject referance
        /// </summary>
        /// <param name="op"></param>
        public void ReleaseInstance(AsyncOperationHandle<TComponent> op)
        {
            var component = op.Result as Component;
            if (component != null)
            {
                Addressables.ReleaseInstance(component.gameObject);
            }

            Addressables.Release(op);
        }
        
        /// <summary>
        /// GameObject load callback
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        private AsyncOperationHandle<TComponent> GameObjectReady(AsyncOperationHandle<GameObject> arg)
        {
            var comp = arg.Result.GetComponent<TComponent>();
            return Addressables.ResourceManager.CreateCompletedOperation<TComponent>(comp, string.Empty);
        }
    }
}