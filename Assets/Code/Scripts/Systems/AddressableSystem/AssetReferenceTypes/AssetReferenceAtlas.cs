using System;
using UnityEngine.AddressableAssets;
using UnityEngine.U2D;

namespace GybeGames.Systems.AddressableSystem.AssetReferenceTypes
{
    /// <summary>
    /// Asset referance for sprite atlas
    /// </summary>
    [Serializable]
    public class AssetReferenceAtlas : AssetReferenceT<SpriteAtlas>
    {
        public AssetReferenceAtlas(string guid) : base(guid) { }
    }
}
