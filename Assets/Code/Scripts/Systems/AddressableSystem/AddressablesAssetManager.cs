using System;
using System.Collections;
using System.Collections.Generic;
using GybeGames.Systems.AddressableSystem.AssetReferenceTypes;
using GybeGames.Systems.App;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.ResourceManagement.ResourceLocations;
using MEC;

namespace GybeGames.Systems.AddressableSystem
{
    public partial class AddressablesSystem : BaseSystem<AddressablesSystem>
    {
        /// <summary>
        /// Manage assets with adressables
        /// </summary>
        private static class AddressablesAssetManager
        {
            private static List<IReleaseableAsyncOperationHandleHolder<AssetReference>>
                OperationHolderDictionaryListAssetReferance =
                    new List<IReleaseableAsyncOperationHandleHolder<AssetReference>>();

            private static List<IReleaseableAsyncOperationHandleHolder<IEnumerable>>
                OperationHolderDictionaryListIEnumarable =
                    new List<IReleaseableAsyncOperationHandleHolder<IEnumerable>>();

            private static List<AsyncOperationHandle> CurrentOperations = new List<AsyncOperationHandle>();

            /// <summary>
            /// Load assets with Asset referance
            /// </summary>
            /// <param name="assetReference">AssetRefarence of the object</param>
            /// <param name="percentageAction">Percentage callback</param>
            /// <typeparam name="T">AssetReferance</typeparam>
            /// <typeparam name="U">Loaded Asset</typeparam>
            /// <returns>Loaded Asset</returns>
            public static IEnumerator<float> LoadAssetAsync<T, U>(T assetReference, Action<U> completionCallback, Action<float> percentageAction = null)
                where T : AssetReference where U : UnityEngine.Object
            {
                AsyncOperationHandle operationHandle = LoadAssetAndGetOperationHandle<U>(assetReference);
                CurrentOperations.Add(operationHandle);
                while (!operationHandle.IsDone)
                {
                    percentageAction?.Invoke(operationHandle.PercentComplete);
                    yield return Timing.WaitForOneFrame;
                }

                if (!AssetReferanceAsyncOperationHandleDictionaryHolder<T>.HasInstance)
                {
                    //A dding New Holder Instance To Load System Dictionary For Releasing everything in future
                    OperationHolderDictionaryListAssetReferance.Add(
                        AssetReferanceAsyncOperationHandleDictionaryHolder<T>.Instance as
                            IReleaseableAsyncOperationHandleHolder<AssetReference>);
                }

                U result = operationHandle.Result as U;

                AssetReferanceAsyncOperationHandleDictionaryHolder<T>.Instance.AddNewOperationHandle(
                    assetReference as T, operationHandle);
                CurrentOperations.Remove(operationHandle);
                completionCallback?.Invoke(result);
            }

            /// <summary>
            /// Loads assets with given keys, names, labels
            /// </summary>
            /// <param name="keys">Key, name label</param>
            /// <param name="mergeMode">Merge method to load assets with intersection, merge or first</param>
            /// <param name="percentageAction">Percentage callback</param>
            /// <typeparam name="T">Keys IEnumarable</typeparam>
            /// <typeparam name="U">Loaded Asset</typeparam>
            /// <returns>Loaded Assets</returns>
            public static IEnumerator<float> LoadAssetAsync<T, U>(T keys, Addressables.MergeMode mergeMode, Action<IList<U>> completionCallback,
                Action<float> percentageAction = null) where T : IEnumerable where U : UnityEngine.Object
            {
                AsyncOperationHandle<IList<U>> operationHandle = Addressables.LoadAssetsAsync<U>(keys, null, mergeMode);
                CurrentOperations.Add(operationHandle);
                while (!operationHandle.IsDone)
                {
                    percentageAction?.Invoke(operationHandle.PercentComplete);
                    yield return Timing.WaitForOneFrame;
                }

                if (!ListAssetReferanceAsyncOperationHandleDictionaryHolder<T>.HasInstance)
                {
                    //Adding New Holder Instance To Load System Dictionary For Releasing everything in future
                    OperationHolderDictionaryListIEnumarable.Add(
                        ListAssetReferanceAsyncOperationHandleDictionaryHolder<T>.Instance as
                            IReleaseableAsyncOperationHandleHolder<IEnumerable>);
                }

                IList<U> result = operationHandle.Result as IList<U>;

                ListAssetReferanceAsyncOperationHandleDictionaryHolder<T>.Instance.AddNewOperationHandle(keys,
                    operationHandle);
                CurrentOperations.Remove(operationHandle);
                completionCallback?.Invoke(result);
            }

            /// <summary>
            /// Load assets from resource locations
            /// </summary>
            /// <param name="locations">Resource locations</param>
            /// <param name="percentageAction">Percentage callback</param>
            /// <typeparam name="T">Resource Locations IEnumarable</typeparam>
            /// <typeparam name="U">Loaded Asset</typeparam>
            /// <returns>Loaded Assets</returns>
            public static IEnumerator<float> LoadAssetsFromLocationsAsync<T, U>(T locations, Action<IList<U>> completionCallback,
                Action<float> percentageAction = null) where T : IList<IResourceLocation> where U : UnityEngine.Object
            {
                AsyncOperationHandle<IList<U>> operationHandle = Addressables.LoadAssetsAsync<U>(locations, null, true);

                CurrentOperations.Add(operationHandle);
                while (!operationHandle.IsDone)
                {
                    percentageAction?.Invoke(operationHandle.PercentComplete);
                    yield return Timing.WaitForOneFrame;
                }

                if (!ListAssetReferanceAsyncOperationHandleDictionaryHolder<T>.HasInstance)
                {
                    //Adding New Holder Instance To Load System Dictionary For Releasing everything in future
                    OperationHolderDictionaryListIEnumarable.Add(
                        ListAssetReferanceAsyncOperationHandleDictionaryHolder<T>.Instance as
                            IReleaseableAsyncOperationHandleHolder<IEnumerable>);
                }

                IList<U> result = operationHandle.Result as IList<U>;

                ListAssetReferanceAsyncOperationHandleDictionaryHolder<T>.Instance.AddNewOperationHandle(locations,
                    operationHandle);
                CurrentOperations.Remove(operationHandle);
                completionCallback?.Invoke(result);
            }

            /// <summary>
            /// Load resource locations of the assets with keys, names, labels
            /// </summary>
            /// <param name="keys">Key, name label</param>
            /// <param name="mergeMode">Merge method to load assets with intersection, merge or first</param>
            /// <param name="type">Types of the assets wanted to bring ResourceLocation</param>
            /// <param name="percentageAction">Percentage callback</param>
            /// <typeparam name="T">Keys IEnumarable</typeparam>
            /// <typeparam name="U">Resource Locations IEnumarable</typeparam>
            /// <returns>Resource Locations</returns>
            public static IEnumerator<float> LoadResourceLocationsAsync<T, U>(T keys,
                Addressables.MergeMode mergeMode, Action<IList<U>> completionCallback, Type type = null, Action<float> percentageAction = null)
                where T : IEnumerable where U : IResourceLocation
            {
                AsyncOperationHandle<IList<IResourceLocation>> operationHandle =
                    Addressables.LoadResourceLocationsAsync(keys, mergeMode, type);
                CurrentOperations.Add(operationHandle);
                while (!operationHandle.IsDone)
                {
                    percentageAction?.Invoke(operationHandle.PercentComplete);
                    yield return Timing.WaitForOneFrame;
                }

                if (!ListAssetReferanceAsyncOperationHandleDictionaryHolder<T>.HasInstance)
                {
                    //Adding New Holder Instance To Load System Dictionary For Releasing everything in future
                    OperationHolderDictionaryListIEnumarable.Add(
                        ListAssetReferanceAsyncOperationHandleDictionaryHolder<T>.Instance as
                            IReleaseableAsyncOperationHandleHolder<IEnumerable>);
                }

                IList<U> result = operationHandle.Result as IList<U>;

                ListAssetReferanceAsyncOperationHandleDictionaryHolder<T>.Instance.AddNewOperationHandle(keys,
                    operationHandle);
                CurrentOperations.Remove(operationHandle);
                completionCallback?.Invoke(result);
            }

            /// <summary>
            /// Load resource locations of the asset referance
            /// </summary>
            /// <param name="assetReference">AssetRefarence of the objects</param>
            /// <param name="type">Types of the assets wanted to bring ResourceLocation</param>
            /// <param name="percentageAction">Percentage callback</param>
            /// <typeparam name="T">Keys IEnumarable</typeparam>
            /// <typeparam name="U">Resource Locations IEnumarable</typeparam>
            /// <returns>Resource Locations</returns>
            public static IEnumerator<float> LoadResourceLocationsAsync<T, U>(T assetReference, Type type, Action<IList<U>> completionCallback,
                Action<float> percentageAction) where T : AssetReference where U : IResourceLocation
            {
                AsyncOperationHandle<IList<IResourceLocation>> operationHandle =
                    Addressables.LoadResourceLocationsAsync(assetReference, type);
                CurrentOperations.Add(operationHandle);
                while (!operationHandle.IsDone)
                {
                    percentageAction?.Invoke(operationHandle.PercentComplete);
                    yield return Timing.WaitForOneFrame;
                }

                if (!AssetReferanceAsyncOperationHandleDictionaryHolder<T>.HasInstance)
                {
                    // Adding New Holder Instance To Load System Dictionary For Releasing everything in future
                    OperationHolderDictionaryListAssetReferance.Add(
                        AssetReferanceAsyncOperationHandleDictionaryHolder<T>.Instance as
                            IReleaseableAsyncOperationHandleHolder<AssetReference>);
                }

                IList<U> result = operationHandle.Result as IList<U>;

                AssetReferanceAsyncOperationHandleDictionaryHolder<T>.Instance.AddNewOperationHandle(
                    assetReference as T, operationHandle);
                CurrentOperations.Remove(operationHandle);
                completionCallback?.Invoke(result);
            }

            /// <summary>
            /// Load asset with component referance of asset referance
            /// </summary>
            /// <param name="assetReference">Asset referance of the object</param>
            /// <typeparam name="U">Operation handle</typeparam>
            /// <returns>Operation Handle</returns>
            private static AsyncOperationHandle LoadAssetAndGetOperationHandle<U>(AssetReference assetReference)
                where U : UnityEngine.Object
            {
                var componentAssetReference = assetReference as ComponentReference<U>;

                // The reason for not using Addressables.LoadAssetAsync asset is that
                // we want that asset referance can be able to hold loaded assest in its .Asset property
                if (componentAssetReference == null)
                {
                    return assetReference.LoadAssetAsync<U>();
                }

                return componentAssetReference.LoadAssetAsync();
            }

            /// <summary>
            /// Release all loaded assets with Addressables
            /// </summary>
            public static void ReleaseAllLoadedAssets() 
            {
                foreach (var operationHandle in OperationHolderDictionaryListAssetReferance)
                {
                    operationHandle.ReleaseAll();
                }

                foreach (var operationHandle in OperationHolderDictionaryListIEnumarable)
                {
                    operationHandle.ReleaseAll();
                }
                Timing.RunCoroutine(UnloadUnusedAssetsWithDelayAsync());
            }

            /// <summary>
            /// Release target asset referance
            /// </summary>
            /// <param name="asset">Asset referance</param>
            /// <typeparam name="T">Asset referance</typeparam>
            public static void ReleaseLoadedTargetAsset<T>(T asset) where T : AssetReference
            {
                if (AssetReferanceAsyncOperationHandleDictionaryHolder<T>.HasInstance)
                {
                    AssetReferanceAsyncOperationHandleDictionaryHolder<T>.Instance.ReleaseTargetAsset(asset);
                }
            }

            /// <summary>
            /// Release target asset referance with keys, names,labels
            /// </summary>
            /// <param name="asset"></param>
            /// <typeparam name="T">Keys IEnumarable</typeparam>
            public static void ReleaseLoadedTargetAssets<T>(T asset) where T : IEnumerable
            {
                if (ListAssetReferanceAsyncOperationHandleDictionaryHolder<T>.HasInstance)
                {
                    ListAssetReferanceAsyncOperationHandleDictionaryHolder<T>.Instance.ReleaseTargetAsset(asset);
                }
            }

            /// <summary>
            /// Unload all unused assets (Be careful its a heavy operation)
            /// </summary>
            private static IEnumerator<float> UnloadUnusedAssetsWithDelayAsync()
            {
                yield return Timing.WaitForOneFrame;
                Gybe.GetSystem<AppSystem>().UnloadUnusedAssets(); // TODO: yüküne bak
            }

            /// <summary>
            /// Wait till the all current operations finish
            /// </summary>
            public static IEnumerator<float> WaitTillAllCurrentOperationsFinish(Action compleionCallback)
            {
                yield return Timing.WaitUntilTrue(() => CurrentOperations.Count == 0);
                compleionCallback?.Invoke();
            }
        }
    }
}