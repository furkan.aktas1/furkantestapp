using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace GybeGames.Systems.AddressableSystem
{
    public partial class AddressablesSystem : BaseSystem<AddressablesSystem>
    {
        /// <summary>
        /// Asset handle operation holder. We need it because we need to release lots of Addressables with its load handle
        /// </summary>
        /// <typeparam name="T">Asset Referance</typeparam>
        internal class ListAssetReferanceAsyncOperationHandleDictionaryHolder<T> : IReleaseableAsyncOperationHandleHolder<T>
                where T : IEnumerable
        {

            private static ListAssetReferanceAsyncOperationHandleDictionaryHolder<T> _instance;
            protected Dictionary<T, AsyncOperationHandle> _allAsyncOperationDictionary;
            
            public static ListAssetReferanceAsyncOperationHandleDictionaryHolder<T> Instance
            {
                get
                {
                    if (_instance == null)
                    {
                        _instance = new ListAssetReferanceAsyncOperationHandleDictionaryHolder<T>();
                    }

                    return _instance;
                }
            }

            public static bool HasInstance { get { return _instance != null; } }

            public ListAssetReferanceAsyncOperationHandleDictionaryHolder()
            {
                _allAsyncOperationDictionary = new Dictionary<T, AsyncOperationHandle>();
            }
            
            public void AddNewOperationHandle(T assetReferance, AsyncOperationHandle operationHandle)
            {
                _allAsyncOperationDictionary.Add(assetReferance, operationHandle);
            }

            /// <summary>
            /// Releasing All OperationHandles
            /// </summary>
            public virtual void ReleaseAll()
            {
                foreach (var dictionaryElement in _allAsyncOperationDictionary)
                {
                    if (dictionaryElement.Value.IsValid())
                    {
                        Addressables.Release(dictionaryElement.Value);
                    }
                }
                _allAsyncOperationDictionary.Clear();
            }

            /// <summary>
            /// Release target handle
            /// </summary>
            /// <param name="assetReference">IEnumarable asset referance</param>
            public virtual void ReleaseTargetAsset(T assetReference)
            {
                if (_allAsyncOperationDictionary.ContainsKey(assetReference))
                {
                    Addressables.Release(_allAsyncOperationDictionary[assetReference]);
                    _allAsyncOperationDictionary.Remove(assetReference);
                }
            }
        }
    }
}
