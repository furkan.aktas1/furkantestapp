namespace GybeGames.Systems.AddressableSystem
{
    public interface IReleaseableAsyncOperationHandleHolder<T>
    {
        void ReleaseAll();

        void ReleaseTargetAsset(T asset);
    }
}