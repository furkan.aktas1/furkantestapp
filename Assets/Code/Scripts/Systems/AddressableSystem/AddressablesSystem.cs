using Cysharp.Threading.Tasks;
using MEC;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.ResourceLocations;
using UnityEngine.ResourceManagement.ResourceProviders;
using UnityEngine.SceneManagement;

namespace GybeGames.Systems.AddressableSystem
{
    public partial class AddressablesSystem : BaseSystem<AddressablesSystem>
    {
        #region Assets
        
        /// <summary>
        /// Load assets with Asset referance
        /// </summary>
        /// <param name="assetReference">AssetRefarence of the object</param>
        /// <param name="percentageAction">Percentage callback</param>
        /// <typeparam name="T">AssetReferance</typeparam>
        /// <typeparam name="U">Loaded Asset</typeparam>
        /// <returns>Loaded Asset</returns>
        public void LoadAssetAsync<T, U>(AssetReference assetReference, Action<U> completionCallback, Action<float> percentageAction = null)
            where T : AssetReference
            where U : UnityEngine.Object
        {
            Timing.RunCoroutine(AddressablesAssetManager.LoadAssetAsync<T, U>((T)assetReference, completionCallback, percentageAction));
        }
        
        /// <summary>
        /// Loads assets with given keys, names, labels
        /// </summary>
        /// <param name="keys">Key, name label</param>
        /// <param name="mergeMode">Merge method to load assets with intersection, merge or first</param>
        /// <param name="percentageAction">Percentage callback<</param>
        /// <typeparam name="T">Keys IEnumarable</typeparam>
        /// <typeparam name="U">Loaded Asset</typeparam>
        /// <returns>Loaded Assets</returns>
        public void LoadAssetAsync<T, U>(IEnumerable keys, Action<IList<U>> completionCallback, Addressables.MergeMode mergeMode = Addressables.MergeMode.Union, Action<float> percentageAction = null)
            where T : IEnumerable
            where U : UnityEngine.Object
        {
            Timing.RunCoroutine(AddressablesAssetManager.LoadAssetAsync<T, U>((T)keys, mergeMode, completionCallback, percentageAction));
        }

        /// <summary>
        /// Load assets from resource locations
        /// </summary>
        /// <param name="locations">Resource locations</param>
        /// <param name="percentageAction">Percentage callback<</param>
        /// <typeparam name="T">Resource Locations IEnumarable</typeparam>
        /// <typeparam name="U">Loaded Asset</typeparam>
        /// <returns>Loaded Assets</returns>
        public void LoadAssetsFromLocationsAsync<T, U>(T locations, Action<IList<U>> completionCallback, Action<float> percentageAction = null)
            where T : IList<IResourceLocation>
            where U : UnityEngine.Object
        {
            Timing.RunCoroutine(AddressablesAssetManager.LoadAssetsFromLocationsAsync<T, U>(locations, completionCallback, percentageAction));
        }
        
        /// <summary>
        /// Load resource locations of the assets with keys, names, labels
        /// </summary>
        /// <param name="keys">Key, name label</param>
        /// <param name="mergeMode">Merge method to load assets with intersection, merge or first</param>
        /// <param name="type">Types of the assets wanted to bring ResourceLocation</param>
        /// <param name="percentageAction">Percentage callback<</param>
        /// <typeparam name="T">Keys IEnumarable</typeparam>
        /// <typeparam name="U">Resource Locations IEnumarable</typeparam>
        /// <returns>Resource Locations</returns>
        public void LoadResourceLocationsAsync<T, U>(T keys, Action<IList<U>> completionCallback, Addressables.MergeMode mergeMode = Addressables.MergeMode.Union, Type type = null, Action<float> percentageAction = null)
            where T : IEnumerable
            where U : IResourceLocation
        {
            Timing.RunCoroutine(AddressablesAssetManager.LoadResourceLocationsAsync<T, U>(keys,mergeMode, completionCallback, type , percentageAction));
        }
        
        /// <summary>
        /// Load resource locations of the asset referance
        /// </summary>
        /// <param name="assetReference">AssetRefarence of the objects</param>
        /// <param name="type">Types of the assets wanted to bring ResourceLocation</param>
        /// <param name="percentageAction">Percentage callback<</param>
        /// <typeparam name="T">Keys IEnumarable</typeparam>
        /// <typeparam name="U">Resource Locations IEnumarable</typeparam>
        /// <returns>Resource Locations</returns>
        public void LoadResourceLocationsAsync<T, U>(T assetReference, Action<IList<U>> completionCallback, Type type = null, Action<float> percentageAction = null)
            where T : AssetReference
            where U : IResourceLocation
        {
            Timing.RunCoroutine(AddressablesAssetManager.LoadResourceLocationsAsync<T, U>(assetReference, type, completionCallback, percentageAction));
        }
        
        /// <summary>
        /// Release target asset referance with keys, names,labels
        /// </summary>
        /// <param name="asset"></param>
        /// <typeparam name="T">Keys IEnumarable</typeparam>
        public void ReleaseLoadedTargetAssets<T>(T asset) where T : IEnumerable
        {
            AddressablesAssetManager.ReleaseLoadedTargetAssets<T>(asset);
        }
        
        /// <summary>
        /// Release target asset referance
        /// </summary>
        /// <param name="asset">Asset referance</param>
        /// <typeparam name="T">Asset referance</typeparam>
        public void ReleaseLoadedTargetAsset<T>(T asset) where T : AssetReference
        {
            AddressablesAssetManager.ReleaseLoadedTargetAsset<T>(asset);
        }
        
        /// <summary>
        /// Release all loaded assets with Addressables
        /// </summary>
        public void ReleaseAllLoadedAssets() // TODO: oyun reset atmadan bunu yapmalı
        {
            AddressablesAssetManager.ReleaseAllLoadedAssets();
        }
        
        #endregion
        
        #region Scenes
        
        /// <summary>
        /// Load scenes async (Be careful scene has to be addressables)
        /// </summary>
        /// <param name="addressablesKey">Addressable Key</param>
        /// <param name="sceneMode">Scene Mode to Load Additive or Single</param>
        /// <param name="percentageAction">Percentage callback<</param>
        /// <returns></returns>
        public async UniTask<SceneInstance> LoadSceneAsync(string addressablesKey, LoadSceneMode sceneMode, Action<float> percentageAction = null)
        {
            return await AddressablesSceneManager.TryLoadSceneAsync(addressablesKey, sceneMode, percentageAction);
        }
        
        /// <summary>
        /// Unload the scene with given key
        /// </summary>
        /// <param name="addressablesKey">Addressable Key</param>
        /// <param name="percentageAction">Percentage callback</param>
        public async UniTask UnloadSceneAsync(string addressablesKey, Action<float> percentageAction = null)
        {
            await AddressablesSceneManager.UnloadSceneAsync(addressablesKey, percentageAction);
        }
        /// <summary>
        /// Unload all additive scenes
        /// </summary>
        /// <param name="percentageAction">Percentage callback</param>
        public async UniTask UnloadAllAddtiveScenesAsync(Action<float> percentageAction = null)
        {
            await AddressablesSceneManager.UnloadAllAdditiveScenesAsync(percentageAction: percentageAction);
        }

        #endregion
        
        /// <summary>
        /// Wait till the all current operations finish
        /// </summary>
        public async UniTask WaitTillAllCurrentOperationsFinish(Action completionCallback)
        {
            await AddressablesSceneManager.WaitTillAllCurrentOperationsFinish();
            Timing.RunCoroutine(AddressablesAssetManager.WaitTillAllCurrentOperationsFinish(completionCallback));
        }

    }
}

