using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace GybeGames.Systems.AddressableSystem
{
    public partial class AddressablesSystem : BaseSystem<AddressablesSystem>
    {
        /// <summary>
        /// Asset handle operation holder. We need it because we need to release lots of Addressables with its load handle
        /// </summary>
        /// <typeparam name="T">Asset Referance</typeparam>
        internal class AssetReferanceAsyncOperationHandleDictionaryHolder<T> : IReleaseableAsyncOperationHandleHolder<T>
            where T : AssetReference
        {
            private static AssetReferanceAsyncOperationHandleDictionaryHolder<T> _instance;
            protected static Dictionary<T, AsyncOperationHandle> _allAsyncOperationDictionary;
            
            public static AssetReferanceAsyncOperationHandleDictionaryHolder<T> Instance
            {
                get
                {
                    if (_instance == null)
                    {
                        _instance = new AssetReferanceAsyncOperationHandleDictionaryHolder<T>();
                    }

                    return _instance;
                }
            }

            public static bool HasInstance
            {
                get { return _instance != null; }
            }

            public AssetReferanceAsyncOperationHandleDictionaryHolder()
            {
                _allAsyncOperationDictionary = new Dictionary<T, AsyncOperationHandle>();
            }

            public void AddNewOperationHandle(T assetReferance, AsyncOperationHandle operationHandle)
            {
                _allAsyncOperationDictionary.Add(assetReferance, operationHandle);
            }

            /// <summary>
            /// Releasing All OperationHandles
            /// </summary>
            public virtual void ReleaseAll()
            {
                foreach (var dictionaryElement in _allAsyncOperationDictionary)
                {
                    if (dictionaryElement.Value.IsValid())
                    {
                        dictionaryElement.Key.ReleaseAsset();
                        Addressables.Release(dictionaryElement.Value);
                    }
                }

                _allAsyncOperationDictionary.Clear();
            }

            /// <summary>
            /// Release target handle
            /// </summary>
            /// <param name="assetReference">Asset Referance</param>
            public virtual void ReleaseTargetAsset(T assetReference)
            {
                if (_allAsyncOperationDictionary.ContainsKey(assetReference))
                {
                    assetReference.ReleaseAsset();
                    _allAsyncOperationDictionary.Remove(assetReference);
                }
            }
        }
    }
}