using System;
using System.Collections.Generic;
using System.Linq;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.ResourceManagement.ResourceProviders;
using UnityEngine.SceneManagement;

namespace GybeGames.Systems.AddressableSystem
{
    public partial class AddressablesSystem : BaseSystem<AddressablesSystem>
    {
        private static class AddressablesSceneManager
        {
            private static Dictionary<string, SceneInstance> _loadedSceneDictionary = new Dictionary<string, SceneInstance>();

            private static Dictionary<string, AsyncOperationHandle<SceneInstance>> _currentOperations = new Dictionary<string, AsyncOperationHandle<SceneInstance>>();

            public static async UniTask<SceneInstance> TryLoadSceneAsync(string addressablesKey, LoadSceneMode sceneMode, Action<float> percentageAction = null)
            {
                if (_loadedSceneDictionary.ContainsKey(addressablesKey) || _currentOperations.ContainsKey(addressablesKey))
                {
                    Debug.LogWarning("Tried to load, loaded scene!");
                    return default;
                }

                AsyncOperationHandle<SceneInstance> operationHandle = Addressables.LoadSceneAsync(addressablesKey, sceneMode);
                _currentOperations.Add(addressablesKey, operationHandle);
                
                while (!operationHandle.IsDone)
                {
                    percentageAction?.Invoke(operationHandle.PercentComplete);
                    await UniTask.NextFrame();
                }

                if (operationHandle.Status == AsyncOperationStatus.Succeeded)
                {
                    // Since last loaded scene was the only active scene, its handle is released by the addressables system
                    // so we need to remove it from the dicitonary from releasing the handle
                    if (sceneMode == LoadSceneMode.Single)
                    {
                        _loadedSceneDictionary.Clear();
                    }
                    _loadedSceneDictionary.Add(addressablesKey, operationHandle.Result);
                }
                else
                {
                    Debug.LogError("Failed to load scene at with de addressables key: " + addressablesKey);
                }
                var sceneInstance = operationHandle.Result;
                _currentOperations.Remove(addressablesKey);

                return sceneInstance;
            }

            public static async UniTask UnloadSceneAsync(string addressablesKey, Action<float> percentageAction = null)
            {
                if (!_loadedSceneDictionary.ContainsKey(addressablesKey) || _loadedSceneDictionary.Count == 1)
                {
                    return;
                }
                var sceneInstance = _loadedSceneDictionary[addressablesKey];
                AsyncOperationHandle<SceneInstance> operationHandle = Addressables.UnloadSceneAsync(sceneInstance);
                _currentOperations.Add(addressablesKey, operationHandle);

                while (!operationHandle.IsDone)
                {
                    percentageAction?.Invoke(operationHandle.PercentComplete);
                    await UniTask.NextFrame();
                }

                _loadedSceneDictionary.Remove(addressablesKey);

                _currentOperations.Remove(addressablesKey);
            }

            public static async UniTask UnloadAllAdditiveScenesAsync(string exceptKey = null, Action<float> percentageAction = null)
            {
                int additiveSceneCount = _loadedSceneDictionary.Count;
                var sceneKeys = _loadedSceneDictionary.Keys.ToList();
                sceneKeys.Remove(exceptKey);
                for (int i = 0; i < sceneKeys.Count; i++)
                {
                    await UnloadSceneAsync(sceneKeys[i], (x) =>
                    {
                        percentageAction?.Invoke((i * 100 + x) / additiveSceneCount);
                    });
                }
            }

            public static async UniTask WaitTillAllCurrentOperationsFinish()
            {
                await UniTask.WaitUntil(() => _currentOperations.Count == 0);
            }
        }
    }
}