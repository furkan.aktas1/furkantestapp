﻿using GybeGames.Systems.Windows;
using GybeGames.Systems.Windows.MainUIWindows;
using Sirenix.OdinInspector;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace GybeGames.Systems.PopUp
{
    [DisallowMultipleComponent]
    public class PopUpSystem : BaseSystem<PopUpSystem>
    {

        [Title("References")]
        [SerializeField] private List<BasePopUp> popupPrefabs = new List<BasePopUp>();

        private RectTransform safeAreaRectTransform { get => Gybe.GetSystem<WindowSystem>().ForceGet<StaticMainCanvas>().SafeAreaRectTransform; }
        private List<IPopUp> popUps { get => Gybe.GetSystem<WindowSystem>().Windows.OfType<IPopUp>().ToList(); }
 
        public override void Initialize()
        {
            base.Initialize();
        }

        public T Create<T>(IPopUp prefab = null) where T : IPopUp
        {
            var popup = default(T);
            prefab = prefab ?? popupPrefabs.OfType<T>().FirstOrDefault();

            if (prefab == null)
            {
                Debug.LogError("Popup prefab can not be found on popupPrefabs List. Please add prefab to list on PopupService!");
                return popup;
            }

            if (prefab != null)
            {
                popup = (T)Instantiate((BasePopUp)prefab, safeAreaRectTransform, false).GetComponent<IPopUp>();
            }
            popup?.RectTransform.SetAsLastSibling();
            return popup;
        }

        public T Get<T>() where T : IPopUp
        {
            return popUps == null ? default(T) : (T)popUps.OfType<T>().LastOrDefault();
        }

        public T ForceGet<T>(IPopUp prefab = null) where T : IPopUp
        {
            T iPopUp = Get<T>();
            return iPopUp == null ? Create<T>(prefab) : iPopUp;
        }

        public void Show<T>(float? delay = null, float? duration = null) where T : IPopUp
        {
            ForceGet<T>()?.Appear(delay, duration);
        }

        public void Hide<T>(float? delay = null, float? duration = null) where T : IPopUp
        {
            Get<T>()?.Disappear(delay, duration);
        }

        public bool IsVisible<T>() where T : IPopUp
        {
            return Get<T>()?.IsVisible ?? false;
        }

        public void HideAll(float? delay = null, float? duration = null)
        {
            popUps.ForEach(popup => popup.Disappear(delay, duration));
        }

    }
}

