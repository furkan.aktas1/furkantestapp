﻿using GybeGames.Systems.Windows;

namespace GybeGames.Systems.PopUp
{
    public interface IPopUp : IWindow
    {
        void OnBackgroundTap();
    }
}