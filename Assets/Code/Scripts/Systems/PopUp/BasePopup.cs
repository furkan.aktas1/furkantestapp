﻿using GybeGames.Systems.Windows;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace GybeGames.Systems.PopUp
{
    public abstract class BasePopUp : BaseWindow, IPopUp
    {
        protected RectTransform contentsRectTransform { get => _contentsRectTransform ?? (_contentsRectTransform = transform.GetChild(0) as RectTransform); }
        protected Graphic backgroundGraphic { get => _backGroundGraphic ?? (_backGroundGraphic = GetComponent<Graphic>()); }
        protected Button backgroundButton { get => _backgroundButton ?? (_backgroundButton = GetComponentInChildren<Button>()); }

        private RectTransform _contentsRectTransform = null;
        private Graphic _backGroundGraphic = null;
        private Button _backgroundButton = null;
        private Coroutine destroyCoroutine = null;

        protected override void Initialize()
        {
            backgroundButton.onClick.AddListener(OnBackgroundTap);
        }

        public virtual void OnBackgroundTap()
        {
            Disappear();
        }

        protected override void OnDisappearingComplete()
        {
            base.OnDisappearingComplete();
            if(destroyCoroutine == null)
            {
                destroyCoroutine = StartCoroutine(DestroyCoroutine());
            }
        }

        private IEnumerator DestroyCoroutine()
        {
            yield return new WaitForEndOfFrame();
            if (gameObject != null)
                Destroy(gameObject);
            destroyCoroutine = null;
        }
    }
}
