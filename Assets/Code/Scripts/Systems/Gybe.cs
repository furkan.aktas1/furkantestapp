﻿using System.Collections.Generic;
using UnityEngine;
using GybeGames.Utilities;

using GybeGames.Utility.Extentions;

namespace GybeGames.Systems
{
    [DisallowMultipleComponent]
    public class Gybe : SingletonMonoBehaviour<Gybe>
    {
        private static Dictionary<object, object> systems = new Dictionary<object, object>();

        /// <summary>
        /// Add system to the systems.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="service"></param>
        public static void AddSystem<T>(object service)
        {
            systems.Add(typeof(T), service);
        }

        /// <summary>
        /// Returns the system from the systems dictionary. If there is no system with that reflection, try to find the system in the active scenes or create a new one.
        /// Then initialize it.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static T GetSystem<T>() where T : BaseSystem<T>
        {
            object value;
            var isExists = systems.TryGetValue(typeof(T), out value);

            if (isExists)
            {
                return (T)value;
            }

            var service = FindObjectOfType<BaseSystem<T>>();
            if (service == null)
            {
                GameObject gameObject = new GameObject();
                service = gameObject.AddComponent<T>();
                gameObject.name = service.GetType().ToString().GetLastType();
            }

            service.Init();
            systems.TryGetValue(typeof(T), out value);
            return (T)value;

        }

        /// <summary>
        /// Remove system from systems.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="service"></param>
        public static void RemoveService<T>(object service)
        {
            if (systems.ContainsKey(typeof(T)))
                systems.Remove(typeof(T));
        }

    }
}
