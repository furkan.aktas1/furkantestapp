﻿namespace GybeGames.Systems
{
    public interface ISystem
    {
        /// <summary>
        /// Initialize system requirements. Do not use awake in any system. Awake is handled by BaseSystem. To initialize anyting use this funciton.
        /// </summary>
        void Initialize();
        /// <summary>
        /// Deinitiliaze system requirements. Do not use OnDetroy in any system. OnDestroy is handled by BaseSystem. To deinitilize anything use this function.
        /// Do not forget when application is being quit, this function cannot be invoked.
        /// </summary>
        void DeInitialize();
    }
}