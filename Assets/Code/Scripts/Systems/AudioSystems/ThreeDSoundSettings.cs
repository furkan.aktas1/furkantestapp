﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GybeGames.Systems.AudioSystems
{
    [System.Serializable]
    public class ThreeDSoundSettings
    {
        [SerializeField] private float dopplerLevel = 1;
        [SerializeField] private float spread = 0;
        [SerializeField] private AudioRolloffMode volumeRolloff = AudioRolloffMode.Custom;
        [SerializeField] private float distanceMin = 1;
        [SerializeField] private float distanceMax = 500;
        [ShowIf("volumeRolloff", AudioRolloffMode.Custom)]
        [SerializeField] private AnimationCurve distanceCurve = new AnimationCurve(new Keyframe[] { new Keyframe(0, 1), new Keyframe(1, 0) });

        public float DopplerLevel { get => dopplerLevel; }
        public float Spread { get => spread; }
        public AudioRolloffMode VolumeRolloff { get => volumeRolloff; }
        public float DistanceMin { get => distanceMin; }
        public float DistanceMax { get => distanceMax; }
        public AnimationCurve DistanceCurve { get => distanceCurve; }
    }
}
