﻿using UnityEngine;

namespace GybeGames.Systems.AudioSystems.SoundSystem
{
    [CreateAssetMenu(menuName = "ScriptableObject/Audio/Sound")]
    public class Sound : ScriptableObject, ISoundData
    {
        [SerializeField] SoundType type = SoundType.None;
        [SerializeField] new string name = null;
        [SerializeField] AudioClip clip = null;
        [Range(0f, 1f)]
        [SerializeField] float volume = 0.75f;
        [Range(-3f, 3f)]
        [SerializeField] float pitch = 1f;

        public int Type { get => (int)type; }
        public SoundType TypeEnum { get => type; }
        public string Name { get => name; }
        public AudioClip Clip { get => clip; }
        public float Volume { get => volume; }
        public float Pitch { get => pitch; }
    }
}

