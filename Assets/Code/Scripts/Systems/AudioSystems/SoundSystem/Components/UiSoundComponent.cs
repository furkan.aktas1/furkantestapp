﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace GybeGames.Systems.AudioSystems.SoundSystem.Components
{
    public class UiSoundComponent : MonoBehaviour, IPointerClickHandler
    {
        [SerializeField] private Sound sound = null;
        public void OnPointerClick(PointerEventData eventData)
        {
            if(sound == null)
            {
                Debug.LogWarning($"Sound has not been assigned to the {gameObject.name}");
                return;
            }
            Gybe.GetSystem<SoundSystem>().PlayUI(soundData: sound);
        }
    }
}
