﻿using UnityEngine;
using System.Linq;
namespace GybeGames.Systems.AudioSystems.SoundSystem.Components
{
    public class SoundComponent : MonoBehaviour, ISoundable
    {
        [SerializeField] private AudioableData audioableData = null;
        [SerializeField] private Sound[] sounds = null;

        public Sound[] Sounds { get => sounds; }
        public AudioSource AudioSource { get => audioSource; }

        private AudioSource audioSource = null;

        private void Awake()
        {
            if (sounds == null)
            {
                Debug.Log($" Sound Component of gameobject: {gameObject.name} does not have sound, destroying component...");
                DestroyImmediate(this);
                return;
            }
            CreateSource();
        }

        public void CreateSource()
        {
            audioSource = gameObject.GetComponent<AudioSource>();
            if(audioSource == null)
            {
                audioSource = gameObject.AddComponent<AudioSource>();
            }
            audioSource.outputAudioMixerGroup = Gybe.GetSystem<SoundSystem>().AudioMixerGroup;
            audioSource.playOnAwake = false;
            audioSource.clip = sounds[0].Clip;
            if(audioableData != null)
            {
                audioSource.priority = audioableData.Priority;
                audioSource.volume = audioableData.Volume;
                audioSource.pitch = audioableData.Pitch;
                audioSource.panStereo = audioableData.StereoPan;
                audioSource.spatialBlend = audioableData.SpatialBlend;
                audioSource.reverbZoneMix = audioableData.ReverbZoneMix;
                audioSource.dopplerLevel = audioableData.ThreeDSoundSettings.DopplerLevel;
                audioSource.spread = audioableData.ThreeDSoundSettings.Spread;
                audioSource.minDistance = audioableData.ThreeDSoundSettings.DistanceMin;
                audioSource.maxDistance = audioableData.ThreeDSoundSettings.DistanceMax;
                audioSource.rolloffMode = audioableData.ThreeDSoundSettings.VolumeRolloff;
                if(audioSource.rolloffMode == AudioRolloffMode.Custom)
                {
                    audioSource.SetCustomCurve(AudioSourceCurveType.CustomRolloff, audioableData.ThreeDSoundSettings.DistanceCurve);
                }
            }
        }

        public void Play(SoundType soundType)
        {
            Sound sound = null;
            for (int i = 0; i < Sounds.Length; i++)
            {
                if(Sounds[i].TypeEnum == soundType)
                {
                    sound = Sounds[i];
                    break;
                }
            }
            if(sound == null)
            {
                return;
            }
            Gybe.GetSystem<SoundSystem>().Play(this, sound);
        }
    }
}