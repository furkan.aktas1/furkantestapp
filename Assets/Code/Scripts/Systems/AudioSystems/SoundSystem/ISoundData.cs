﻿namespace GybeGames.Systems.AudioSystems.SoundSystem
{
    public interface ISoundData : IListenableData
    {
        SoundType TypeEnum { get; }
    }
}

