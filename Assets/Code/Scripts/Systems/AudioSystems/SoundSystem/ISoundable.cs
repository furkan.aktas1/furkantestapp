﻿namespace GybeGames.Systems.AudioSystems.SoundSystem
{
    public interface ISoundable : IListenable
    {
        Sound[] Sounds { get; }
    }

}