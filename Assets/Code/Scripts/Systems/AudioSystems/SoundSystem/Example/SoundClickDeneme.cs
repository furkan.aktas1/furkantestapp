﻿using GybeGames.Systems.AudioSystems.SoundSystem.Components;
using UnityEngine;
using UnityEngine.EventSystems;

namespace GybeGames.Systems.AudioSystems.SoundSystem.Example
{
    [RequireComponent(typeof(SoundComponent))]
    public class SoundClickDeneme : MonoBehaviour, IPointerClickHandler
    {
        private SoundComponent soundComponent { get => _soundComponent ?? (_soundComponent = GetComponent<SoundComponent>()); }
        private SoundComponent _soundComponent = null;

        public void OnPointerClick(PointerEventData eventData)
        {
            soundComponent.Play(SoundType.CurrencyUpdate);
        }
    }
}

