﻿namespace GybeGames.Systems.AudioSystems.SoundSystem
{
    public enum SoundType
    {
        None,
        ButtonClick,
        ToggleButtonEnabled,
        ToggleButtonDisabled,
        CurrencyUpdate
    }
}