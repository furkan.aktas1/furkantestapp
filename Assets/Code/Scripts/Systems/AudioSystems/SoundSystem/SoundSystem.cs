﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using Sirenix.OdinInspector;
using Sirenix.Utilities;
using GybeGames.Systems.AudioSystems.SoundSystem.Events;
using GybeGames.Systems.EventSystem;
using GybeGames.Systems.Save;

namespace GybeGames.Systems.AudioSystems.SoundSystem
{
    [DisallowMultipleComponent]
    public class SoundSystem : BaseSystem<SoundSystem>, IListenable, IAudioSystem
    {
        [Title("References")]
        [SerializeField, Required] private AudioMixerGroup soundMixerGroup = null;
        [SerializeField] private List<Sound> uiSounds = new List<Sound>();

        public AudioMixerGroup AudioMixerGroup { get => soundMixerGroup; }
        public AudioSource AudioSource { get => uiStaticAudioSource.AudioSource; }
        public bool IsSoundEnabled { get => isSoundEnabled; private set => isSoundEnabled = value; }

        private StaticAudioSource<Sound> uiStaticAudioSource;
        [SavedAs("Gybe.Sound.IsSoundEnabled")] private bool isSoundEnabled = true;

        public override void Initialize()
        {
            base.Initialize();
            uiStaticAudioSource = new StaticAudioSource<Sound>(soundMixerGroup);
        }

        public void Play(ISoundable soundable, Sound sound)
        {
            if (!IsSoundEnabled || soundable == null)
            {
                return;
            }

            if (soundable.AudioSource.isPlaying || soundable.AudioSource.clip != sound.Clip)
            {
                soundable.AudioSource.PlayOneShot(sound.Clip, sound.Volume);
                return;
            }

            soundable.AudioSource.Play();
        }

        public bool ChangeSoundStatus()
        {
            IsSoundEnabled = !IsSoundEnabled;
            EventManager.Send(SoundStatusChangedEvent.Create(IsSoundEnabled));
            return IsSoundEnabled;
        }

        private ISoundData GetSoundData(string name = null, SoundType musicType = SoundType.None)
        {
            return name.IsNullOrWhitespace() ? uiSounds.Find(x => x.TypeEnum == musicType) : uiSounds.Find(x => x.Name == name);
        }

        public void PlayUI(ISoundData soundData = null, string name = null, SoundType musicType = SoundType.None)
        {
            ISoundData iSoundData = soundData ?? GetSoundData(name, musicType);
            if (!IsSoundEnabled || iSoundData == null)
                return;
            uiStaticAudioSource.Play(listenableData: iSoundData);
        }

    }
}