﻿using GybeGames.Systems.EventSystem;

namespace GybeGames.Systems.AudioSystems.SoundSystem.Events
{
    public class SoundStatusChangedEvent : CustomEvent
    {
        public bool SoundStatus { get; private set; }

        public static SoundStatusChangedEvent Create(bool soundState)
        {
            SoundStatusChangedEvent soundEvent = new SoundStatusChangedEvent();
            soundEvent.SoundStatus = soundState;
            return soundEvent;
        }
    }
}
