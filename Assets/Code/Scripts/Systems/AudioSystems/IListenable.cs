﻿using UnityEngine;

namespace GybeGames.Systems.AudioSystems
{
    public interface IListenable
    {
        AudioSource AudioSource { get; }
    }
}