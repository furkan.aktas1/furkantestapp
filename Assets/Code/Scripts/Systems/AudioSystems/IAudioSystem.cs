﻿using UnityEngine.Audio;

namespace GybeGames.Systems.AudioSystems
{
    public interface IAudioSystem
    {
        AudioMixerGroup AudioMixerGroup { get; }
    }
}
