﻿namespace GybeGames.Systems.AudioSystems.MusicSystem
{
    public interface IMusicable : IListenable
    {
        Music Music { get; }
    }
}