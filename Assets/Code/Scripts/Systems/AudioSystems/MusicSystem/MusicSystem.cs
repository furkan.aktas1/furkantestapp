﻿using GybeGames.Systems.AudioSystems.MusicSystem.Events;
using GybeGames.Systems.EventSystem;
using GybeGames.Systems.Save;
using Sirenix.OdinInspector;
using Sirenix.Utilities;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

namespace GybeGames.Systems.AudioSystems.MusicSystem
{
    [DisallowMultipleComponent]
    public class MusicSystem : BaseSystem<MusicSystem>, IAudioSystem
    {
        [Title("References")]
        [SerializeField, Required] private AudioMixerGroup musicMixerGroup = null;
        [SerializeField] private List<Music> musicList = new List<Music>();

        public AudioSource AudioSource { get => staticAudioSource.AudioSource; }
        public AudioMixerGroup AudioMixerGroup { get => musicMixerGroup; }
        public bool IsMusicEnabled { get => isMusicEnabled; private set => isMusicEnabled = value; }

        private StaticAudioSource<Music> staticAudioSource;
        [SavedAs("Gybe.Music.IsMusicEnabled")] private bool isMusicEnabled = true;

        public override void Initialize()
        {
            base.Initialize();
            staticAudioSource = new StaticAudioSource<Music>(musicMixerGroup);
        }

        public void Play(string name = null, MusicType musicType = MusicType.None)
        {
            IMusicData iMusicData = GetMusicData(name, musicType);
            if (!IsMusicEnabled || iMusicData == null || (iMusicData.Clip == staticAudioSource.AudioSource.clip && staticAudioSource.AudioSource.isPlaying))
                return;
            staticAudioSource.Play(listenableData: iMusicData, playOneShot: false);
        }

        public void Pause()
        {
            if (!staticAudioSource.AudioSource.isPlaying)
                return;
            staticAudioSource.AudioSource.Pause();
        }

        public void UnPause(string name = null, MusicType musicType = MusicType.None)
        {
            if (staticAudioSource.AudioSource.isPlaying)
                return;
            IMusicData iMusicData = GetMusicData(name, musicType);
            if (!IsMusicEnabled || iMusicData == null || iMusicData.Clip != staticAudioSource.AudioSource.clip)
                return;
            staticAudioSource.AudioSource.UnPause();
        }

        public void Stop()
        {
            staticAudioSource.AudioSource.Stop();
        }

        private IMusicData GetMusicData(string name = null, MusicType musicType = MusicType.None)
        {
            return name.IsNullOrWhitespace() ? musicList.Find(x => x.TypeEnum == musicType) : musicList.Find(x => x.Name == name);
        }

        public bool ChangeMusicStatus()
        {
            IsMusicEnabled = !IsMusicEnabled;
            EventManager.Send(MusicStatusChangedEvent.Create(IsMusicEnabled));
            if (!IsMusicEnabled)
            {
                Pause();
            }
            else if (IsMusicEnabled)
            {
                UnPause();
            }
            return IsMusicEnabled;
        }
    }
}