﻿namespace GybeGames.Systems.AudioSystems.MusicSystem
{
    public interface IMusicData : IListenableData
    {
        bool Loop { get; }
        MusicType TypeEnum { get; }
    }
}
