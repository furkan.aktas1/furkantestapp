﻿using UnityEngine;

namespace GybeGames.Systems.AudioSystems.MusicSystem
{
    [CreateAssetMenu(menuName = "ScriptableObject/Audio/Musics")]
    public class Music : ScriptableObject, IMusicData
    {
        [SerializeField] MusicType type = MusicType.None;
        [SerializeField] new string name = null;
        [SerializeField] AudioClip clip = null;
        [Range(0f, 1f)]
        [SerializeField] float volume = 0.75f;
        [Range(-3f, 3f)]
        [SerializeField] float pitch = 1f;
        [SerializeField] bool loop = false;


        public int Type { get => (int)type; }
        public MusicType TypeEnum { get => type; }
        public string Name { get => name; }
        public AudioClip Clip { get => clip; }
        public float Volume { get => volume; }
        public float Pitch { get => pitch; }
        public bool Loop { get => loop; }
    }
}