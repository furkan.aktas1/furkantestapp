﻿using GybeGames.Systems.EventSystem;

namespace GybeGames.Systems.AudioSystems.MusicSystem.Events
{
    public class MusicStatusChangedEvent : CustomEvent
    {
        public bool MusicStatus { get; private set; }

        public static MusicStatusChangedEvent Create(bool musicStatus)
        {
            MusicStatusChangedEvent soundEvent = new MusicStatusChangedEvent();
            soundEvent.MusicStatus = musicStatus;
            return soundEvent;
        }
    }
}
