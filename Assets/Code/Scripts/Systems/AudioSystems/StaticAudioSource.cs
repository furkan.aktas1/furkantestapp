﻿
using UnityEngine;
using GybeGames.Utility.Extentions;
using UnityEngine.Audio;
using GybeGames.Systems.Cameras;

namespace GybeGames.Systems.AudioSystems
{
    public class StaticAudioSource<TListenableData> where TListenableData : IListenableData
    {
        public AudioSource AudioSource { get => audioSource; }
        private AudioSource audioSource;
        
        private AudioMixerGroup audioMixerGroup;

        public StaticAudioSource(AudioMixerGroup audioMixerGroup)
        {
            this.audioMixerGroup = audioMixerGroup;
            CreateSource();
        }

        public void CreateSource()
        {
            GameObject gameObject = CreateAudioSourceGameObject();
            SetAudioSource(gameObject);
        }

        public void Play(IListenableData listenableData, bool playOneShot = true)
        {
            if (playOneShot)
            {
                audioSource.PlayOneShot(listenableData.Clip, listenableData.Volume);
                return;
            }
            SetSourceToListenableData(listenableData);
            audioSource.Play();
        }

        private void SetSourceToListenableData(IListenableData listenableData)
        {
            audioSource.clip = listenableData.Clip;
            audioSource.volume = listenableData.Volume;
            audioSource.pitch = listenableData.Pitch;
        }

        private GameObject CreateAudioSourceGameObject()
        {
            GameObject gameObject = new GameObject();
            gameObject.transform.SetParent(Gybe.GetSystem<CameraSystem>().MainCamera.transform);
            gameObject.transform.localPosition = Vector3.zero;
            gameObject.name = $"{typeof(TListenableData).ToString().GetLastType()}AudioSource";
            return gameObject;
        }

        private void SetAudioSource(GameObject gameObject)
        {
            audioSource = gameObject.AddComponent<AudioSource>();
            audioSource.outputAudioMixerGroup = audioMixerGroup;
            audioSource.playOnAwake = false;
        }
    }
}