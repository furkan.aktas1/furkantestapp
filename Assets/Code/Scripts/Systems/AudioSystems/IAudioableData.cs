﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GybeGames.Systems.AudioSystems
{
    public interface IAudioableData
    {
        int Priority { get; }
        float Volume { get; }
        float Pitch { get; }
        float StereoPan { get; }
        float SpatialBlend { get; }
        float ReverbZoneMix { get; }
        ThreeDSoundSettings ThreeDSoundSettings { get; }
    }
}

