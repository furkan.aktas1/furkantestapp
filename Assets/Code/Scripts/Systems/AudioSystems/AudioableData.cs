﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GybeGames.Systems.AudioSystems
{
    [CreateAssetMenu(menuName = "ScriptableObject/Audio/AudioableData")]
    public class AudioableData : ScriptableObject, IAudioableData
    {
        [SerializeField] private int priority = 128;
        [SerializeField] private float volume = 1;
        [SerializeField] private float pitch = 1;
        [SerializeField] private float stereoPan = 0;
        [SerializeField] private float spatialBlend = 1;
        [SerializeField] private float reverbZoneMix = 1;
        [SerializeField] private ThreeDSoundSettings threeDSoundSettings = new ThreeDSoundSettings();

        public int Priority { get => priority; }
        public float Volume { get => volume; }
        public float Pitch { get => pitch; }
        public float StereoPan { get => stereoPan; }
        public float SpatialBlend { get => spatialBlend; }
        public float ReverbZoneMix { get => reverbZoneMix; }
        public ThreeDSoundSettings ThreeDSoundSettings { get => threeDSoundSettings; }
    }
}

