﻿using UnityEngine;

namespace GybeGames.Systems.AudioSystems
{
    public interface IListenableData
    {
        int Type { get; }
        string Name { get; }
        AudioClip Clip { get; }
        float Volume { get; }
        float Pitch { get; }
    }
}
