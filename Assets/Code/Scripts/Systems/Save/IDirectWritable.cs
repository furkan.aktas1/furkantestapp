using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GybeGames.Systems.Save
{
    public interface IDirectWritable
    {
        byte[] GetByteArray();
        void SetWritable(byte[] byteArray);
    }
}