﻿namespace GybeGames.Systems.Save
{
    public interface ISavable
    {
        void PostLoad();
        void Load();
        void PreSave();
        void Save();
    }
}