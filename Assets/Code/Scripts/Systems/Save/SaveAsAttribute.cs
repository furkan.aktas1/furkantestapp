﻿using System;

namespace GybeGames.Systems.Save
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property | AttributeTargets.Class)]
    public class SavedAsAttribute : Attribute
    {
        public string Key { get; private set; }
        public SavedAsAttribute(string key)
        {
            this.Key = key;
        }
    }
}