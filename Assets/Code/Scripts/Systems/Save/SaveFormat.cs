﻿namespace GybeGames.Systems.Save
{
    public enum SaveFormat
    {
        Binary = 0,
        JSON = 1
    }
}