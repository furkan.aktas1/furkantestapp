﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Sirenix.Serialization;
using UnityEngine;
using GybeGames.Systems.App;
using Sirenix.OdinInspector;

namespace GybeGames.Systems.Save
{
    [DefaultExecutionOrder(-998)]
    [DisallowMultipleComponent]
    public class SaveSystem : BaseSystem<SaveSystem>
    {
        private const int HIBRIT_SAVE_DURATION = 90;

        private static string SavePath { get => Application.persistentDataPath; }
        private static string SaveFileNameWithExtention { get => "Save.dat"; }
        private static string SaveFullPath { get => Path.Combine(SavePath, SaveFileNameWithExtention); }
        private static string SavePrefsBackupKey { get => Application.productName + "_BACKUP"; }
        private static List<ISavable> savables { get; set; } = new List<ISavable>();

        [HideInInspector]
        [SerializeField] private SaveData saveData;

        [HideInInspector]
        [SerializeField] private SaveData directWritableSaveData;
        
        private DataFormat DataFormat { get => Gybe.GetSystem<AppSystem>().SaveDataFormat; }
        private bool isFirstRun = false;

        private bool isSessionFirstRead = true;
        private bool hibritSave = true;
        private Coroutine hibritSaveCoroutine = null;

        public override void Initialize()
        {
            base.Initialize();
            StartHibritSaveCoroutine();
        }

        private void StartHibritSaveCoroutine()
        {
            if (!hibritSave)
            {
                return;
            }
            if (hibritSaveCoroutine != null)
            {
                StopCoroutine(hibritSaveCoroutine);
            }
            hibritSaveCoroutine = StartCoroutine(HibritSaveCoroutine());
        }

        private IEnumerator HibritSaveCoroutine()
        {
            while (true)
            {
                yield return new WaitForSeconds(HIBRIT_SAVE_DURATION);
                SaveAll();
                Write();
                PlayerPrefs.Save();
            }
        }

        private void OnApplicationPause(bool pauseStatus)
        {
            if (pauseStatus)
            {
                SaveAll();
                Write();
                PlayerPrefs.Save();
            }
            else
            {
                if (isSessionFirstRead)
                {
                    isSessionFirstRead = false;
                    return;
                }
                Read();
                LoadAll();
                StartHibritSaveCoroutine();
            }
        }

#if UNITY_EDITOR
        protected void OnApplicationQuit()
        {
            SaveAll();
            Write();
            PlayerPrefs.Save();
        }
#endif

        public void Read()
        {
            byte[] byteDataArray = null;
            saveData = new SaveData();

            try
            {
                if (HasSave())
                {
                    byteDataArray = File.ReadAllBytes(SaveFullPath);
                    saveData = SerializationUtility.DeserializeValue<SaveData>(byteDataArray, DataFormat);
                    Debug.Log("Loading from local...");
                }
            }
            catch (Exception ex)
            {
                Debug.LogWarning($"Reading error: {ex.Message}");
            }
            finally
            {

                if (saveData == null || saveData.Count <= 0)
                {
                    if (HasBackup())
                    {
                        byteDataArray = Convert.FromBase64String(PlayerPrefs.GetString(SavePrefsBackupKey, null));
                        saveData = SerializationUtility.DeserializeValue<SaveData>(byteDataArray, DataFormat);
                        Debug.LogWarning("Reading save data from PlayerPrefs...");
                    }
                }

                if (saveData == null || saveData?.Count <= 0)
                {
                    isFirstRun = true;
                    saveData = new SaveData();
                    Debug.Log("First run...");
                }
            }
        }

        public void Write()
        {
            byte[] byteDataArray = SerializationUtility.SerializeValue(saveData, DataFormat);

            try
            {
                File.WriteAllBytes(SaveFullPath, byteDataArray);
                if (!Directory.Exists(GetDirectWriteablePath()))
                {
                    Directory.CreateDirectory(GetDirectWriteablePath());
                }
                foreach (var writable in directWritableSaveData)
                {
                    File.WriteAllBytes(GetDirectWriteablePath(writable.Key), (writable.Value as IDirectWritable).GetByteArray());
                }
                Debug.Log("Saving data to local...");
            }
            catch (Exception ex)
            {
                Debug.LogWarning($"Writing error: {ex.Message}");
            }
            finally
            {
                PlayerPrefs.SetString(SavePrefsBackupKey, Convert.ToBase64String(byteDataArray));
            }
        }

        /// <summary>
        /// Set value to the key of the saveData.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void Set<T>(string key, T value)
        {
            if (String.IsNullOrEmpty(key))
            {
                Debug.LogWarning("Key is null or empty");
                return;
            }

            if (value == null)
            {
                Debug.LogWarning("Value is null");
                return;
            }

            if (value is IDirectWritable)
            {
                directWritableSaveData[key] = value;
                return;
            }
            
            saveData[key] = value;
        }

        /// <summary>
        /// If saved data does not have the key, this methods creates new key at the savedata and set defaults to its value.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void SetDefault<T>(string key, T value)
        {
            if (!saveData.ContainsKey(key))
            {
                Set<T>(key, value);
            }
        }

        public bool TryGetDirectWritable<T>(string key, ref T value) where T : IDirectWritable, new()
        {
            value = new T();
            if (directWritableSaveData.ContainsKey(key))
            {
                value = (T)directWritableSaveData[key];
                return true;
            }

            if (!Directory.Exists(GetDirectWriteablePath()))
            {
                return false;
            }
            if(!File.Exists(GetDirectWriteablePath(key)))
            {
                return false;
            }
            var byteDataArray = File.ReadAllBytes(GetDirectWriteablePath(key));
            if (byteDataArray == null || byteDataArray.Length == 0)
            {
                return false;
            }
            value.SetWritable(byteDataArray);

            return true;
        }
        
        /// <summary>
        /// Get saveable. Be careful do not use that function to get DirectWritable
        /// </summary>
        /// <param name="key"></param>
        /// <param name="defaultValue"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public T Get<T>(string key, T defaultValue = default(T))
        {
            if (String.IsNullOrEmpty(key))
            {
                Debug.LogWarning("Key is null or empty. Returning default value for requested type.");
                return defaultValue;
            }

            if (saveData.ContainsKey(key))
            {
                return (T)saveData[key];
            }
            else
            {
                //Debug.LogWarning($"Can't find key: {key}. Returning default value for requested type.");
            }

            return defaultValue;
        }

        public void Load<T>(T target) where T : ISavable
        {
            if (target == null)
            {
                Debug.LogWarning($"Loading error: {typeof(T)} is null.");
                return;
            }

            if (!savables.Contains(target))
            {
                savables.Add(target);
            }

            if (isFirstRun)
            {
                return;
            }

            if (saveData == null || saveData?.Count <= 0)
            {
                if (target.GetType() != this.GetType())
                {
                    Debug.LogWarning("Loading error: Save data is null, trying to read from disk.");
                }
                Read();
            }

            if (saveData?.Count <= 0)
            {
                Debug.LogWarning("Loading error: Save data is empty, nothing to load.");
                return;
            }

            FieldInfo[] objectFields = target.GetType().GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
            for (int i = 0; i < objectFields.Length; i++)
            {
                SavedAsAttribute savedAs = Attribute.GetCustomAttribute(objectFields[i], typeof(SavedAsAttribute)) as SavedAsAttribute;
                if (savedAs != null)
                {
                    if (saveData.ContainsKey(savedAs.Key))
                    {
                        object keyValue = Get<object>(savedAs.Key);
                        objectFields[i].SetValue(target, keyValue);
                    }
                }
            }
            target.PostLoad();
        }

        public void Save<T>(T source) where T : ISavable
        {
            if (source == null)
            {
                Debug.LogWarning($"Saving error: {typeof(T).ToString().ToUpper()} is null.");
                return;
            }

            if (!savables.Contains(source))
            {
                savables.Add(source);
            }
            source.PreSave();
            FieldInfo[] objectFields = source.GetType().GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
            for (int i = 0; i < objectFields.Length; i++)
            {
                SavedAsAttribute savedAs = Attribute.GetCustomAttribute(objectFields[i], typeof(SavedAsAttribute)) as SavedAsAttribute;
                if (savedAs != null)
                {
                    Set<object>(savedAs.Key, objectFields[i].GetValue(source));
                }
            }
        }

        public void SaveDefaults<T>(T source) where T : ISavable
        {
            if (source == null)
            {
                Debug.LogWarning($"Saving defaults error: {typeof(T).ToString().ToUpper()} is null.");
                return;
            }

            if (!savables.Contains(source))
            {
                savables.Add(source);
            }

            if (saveData == null || saveData?.Count == 0)
            {
                Read();
            }

            FieldInfo[] objectFields = source.GetType().GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
            for (int i = 0; i < objectFields.Length; i++)
            {
                SavedAsAttribute savedAs = Attribute.GetCustomAttribute(objectFields[i], typeof(SavedAsAttribute)) as SavedAsAttribute;
                if (savedAs != null)
                {
                    SetDefault<object>(savedAs.Key, objectFields[i].GetValue(source));
                }
            }
        }
        
        private static string GetDirectWriteablePath(string key = "")
        {
            
            return key == "" ? Path.Combine(SavePath, "DirectPath") : Path.Combine(SavePath, "DirectPath", key + ".dat");
        }

        public static bool HasSave()
        {
            return File.Exists(SaveFullPath);
        }

        public static bool HasBackup()
        {
            return PlayerPrefs.HasKey(SavePrefsBackupKey);
        }

        public static void DeleteSave()
        {
            if (!HasSave()) return;

            try
            {
                if (Directory.Exists(GetDirectWriteablePath()))
                {
                    Directory.Delete(GetDirectWriteablePath(), true);
                }
                File.Delete(SaveFullPath);
            }
            catch (Exception ex)
            {
                Debug.LogWarning($"Deleting error: {SaveFullPath} - {ex.Message}");
            }
            finally
            {
                if (!HasSave())
                {
                    Debug.LogWarning($"Save deleted successfully: {SaveFullPath}");
                }
            }
        }

        public static void DeleteBackup()
        {
            if (!HasBackup()) return;

            PlayerPrefs.DeleteKey(SavePrefsBackupKey);
            PlayerPrefs.Save();

            if (!HasBackup())
            {
                Debug.LogWarning($"Backup deleted successfully: PlayerPrefs key =  {SavePrefsBackupKey}");
            }
        }

        public void LoadAll()
        {
            foreach (var savable in savables)
            {
                Load(savable);
            }
        }

        public void SaveAll()
        {
            foreach (var savable in savables)
            {
                Save(savable);
            }
        }

        [Serializable]
        public class SaveData : Dictionary<string, object> { }

#if UNITY_EDITOR
        [Title("Editor Functions")]
        [Sirenix.OdinInspector.Button("Clear All Saved Data")]
        public void Editor_ClearAllSavedData()
        {
            DeleteSave();
            DeleteBackup();
        }


        public static void ClearAllSavedData()
        {
            DeleteSave();
            DeleteBackup();
        }
#endif
    }
}