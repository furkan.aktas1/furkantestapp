using GybeGames.Systems.Save;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

namespace GybeGames.Systems.Scenes.Editor
{
    public class SaveEditorTool
    {
        [MenuItem("SaveTool/ClearSaveData", priority = 0)]
        public static void ClearSaveData()
        {
            SaveSystem.ClearAllSavedData();
        }

        [MenuItem("SaveTool/ClearPlayerPrefs", priority = 1)]
        public static void ClearPlayerPrefs()
        {
            PlayerPrefs.DeleteAll();
        }

        [MenuItem("SaveTool/ClearAll", priority = 2)]
        public static void ClearAll()
        {
            SaveSystem.ClearAllSavedData();
            PlayerPrefs.DeleteAll();
        }
    }
}

