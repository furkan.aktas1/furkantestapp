using System;
using UnityEngine;
using Sirenix.OdinInspector;
namespace GybeGames.Systems.Save
{
    [Serializable]
    public class Texture2DSaveable : IDirectWritable
    {
        [SerializeField] private Texture2D texture;
        
        public Texture2DSaveable()
        {
            
        }
        
        public Texture2DSaveable(Texture2D texture2D)
        {
            this.texture = texture2D;
        }

        public Texture2D GetTexture2D()
        {
            return texture;
        }
        
        public Sprite GetSprite()
        {
            return Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(texture.width / 2, texture.height / 2));
        }

        public byte[] GetByteArray()
        {
            return texture.EncodeToJPG();
        }

        public void SetWritable(byte[] byteArray)
        {
            texture = new Texture2D(1, 1);
            texture.LoadImage(byteArray);
        }
    }
}
