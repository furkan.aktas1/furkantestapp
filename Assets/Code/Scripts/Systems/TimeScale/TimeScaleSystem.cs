﻿using System.Collections;
using UnityEngine;

namespace GybeGames.Systems.TimeScale
{
    public class TimeScaleSystem : BaseSystem<TimeScaleSystem>
    {
        private Coroutine slowCoroutine = null;

        public void SetSlower(float slowScale, float delay)
        {
            if (slowCoroutine != null)
            {
                StopCoroutine(slowCoroutine);
            }
            SetTimeScale(slowScale);
            slowCoroutine = StartCoroutine(SlowCoroutine(delay));
        }

        private IEnumerator SlowCoroutine(float delay)
        {
            yield return new WaitForSecondsRealtime(delay);
            SetTimeScale(1);
        }

        public void SetTimeScale(float timeScale)
        {
            Time.timeScale = timeScale;
        }
    }
}

