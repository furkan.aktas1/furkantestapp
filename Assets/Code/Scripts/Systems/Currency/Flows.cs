﻿using DG.Tweening;
using GybeGames.Systems.Cameras;
using GybeGames.Utility.Extentions;
using MEC;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GybeGames.Systems.Currency
{
    public class CurrencyParticleFlow
    {
        private float time = 0.7f, startScale = 1.2f, randomizePosition = 0.05f;
        private int count = 1;
        private double? currencyAmount = null;

        public CurrencyParticleFlow(float time, float startScale, float randomizePosition, int count = 1, double? currencyAmount = null)
        {
            this.time = time;
            this.startScale = startScale;
            this.randomizePosition = randomizePosition;
            this.count = count;
            this.currencyAmount = currencyAmount;
        }

        public void MoveFromTo(Sprite sprite, RectTransform from, RectTransform to, Transform parent, Action<double?> onComplete = null)
        {
            from.SetAsLastSibling();
            Timing.RunCoroutine(DestroyAfterTime(sprite, from.position, to, parent, onComplete));
        }

        public void MoveFromTo(Sprite sprite, Vector3 fromWorldPosition, RectTransform to, Transform parent, Action<double?> onComplete = null)
        {
            Vector3 fromPosition = Gybe.GetSystem<CameraSystem>().FromMainWorldPointToUIWorldPoint(fromWorldPosition);
            Timing.RunCoroutine(DestroyAfterTime(sprite, fromPosition, to, parent, onComplete));
        }

        private IEnumerator<float> DestroyAfterTime(Sprite sprite, Vector3 fromWorldPosition, RectTransform to, Transform parent, Action<double?> onComplete)
        {
            List<Image> createdOnes = new List<Image>();
            Canvas canvas = parent.GetComponentInParent<Canvas>();
            for (int i = 0; i < count; i++)
            {
                GameObject gameObject = new GameObject();
                Image image = gameObject.AddComponent<Image>();
                gameObject.transform.position = canvas.transform.position;
                image.transform.SetParent(parent);
                image.transform.position = fromWorldPosition;
                image.rectTransform.anchoredPosition3D = image.rectTransform.anchoredPosition3D.ZeroZ(); 
                image.sprite = sprite;
                image.transform.localScale = startScale * Vector3.one;
                createdOnes.Add(image);
            }
            time -= 0.1f;
            yield return Timing.WaitForSeconds(0.1f);
            for (int i = 0; i < createdOnes.Count; i++)
            {
                var point = createdOnes[i].rectTransform.position +
                    (Vector3.up * UnityEngine.Random.Range(-randomizePosition, randomizePosition) * Mathf.Min(canvas.planeDistance, 50)) +
                    (Vector3.right * UnityEngine.Random.Range(-randomizePosition, randomizePosition) * Mathf.Min(canvas.planeDistance, 50));
                createdOnes[i].rectTransform.DOMove(point, 0.2f);
            }
            time -= 0.2f;
            yield return Timing.WaitForSeconds(0.2f);
            for (int i = 0; i < createdOnes.Count; i++)
            {
                var image = createdOnes[i];
                image.rectTransform.DOScale(1, time / 2);
                image.rectTransform.DOMove(to.position, time, false);
            }
            yield return Timing.WaitForSeconds(time);
            to.DOKill();
            to.DOPunchScale(Vector3.one * 1.25f, 0.3f, 4).OnComplete(() => to.DOScale(Vector3.one, 0.1f));
            for (int i = 0; i < createdOnes.Count; i++)
            {
                GameObject.Destroy(createdOnes[i].gameObject);
            }
            onComplete?.Invoke(this.currencyAmount);
        }
    }

}