﻿
using GybeGames.Systems.AudioSystems.SoundSystem;
using System;
using UnityEngine;

namespace GybeGames.Systems.Currency
{
    [CreateAssetMenu(fileName = "CurrencyData", menuName = "ScriptableObject/CurrencySystem/CurrencyData")]
    public class CurrencyData : ScriptableObject
    {
        [SerializeField] private string key = null;
        [SerializeField] private Sprite icon = null;
        [SerializeField] private Sound sound = null;

        [SerializeField] private bool willAbbrivate = true, willFormat = true;
        [SerializeField] private string specialFormat = null;

        [SerializeField] private bool willTrackFlow = true;
        [SerializeField] private double currenyPerPoint = 10;
        [SerializeField] private uint maxAmount = 50;

        public string Key { get { return key; } }
        public Sprite IconSprite { get { return icon; } }
        public Sound Sound { get { return sound; } }
        public bool WillAbbrivate { get { return willAbbrivate; } }
        public bool WillFormat { get { return willFormat; } }
        public string SpecialFormat { get { return specialFormat; } }
        public bool WillTrackFlow { get { return willTrackFlow; } }
        public double CurrencyPerPoint { get { return currenyPerPoint; } }
        public uint MaxAmount { get { return maxAmount; } }

        private string saveKey => $"Gybe.Currency.{key}";

        public Action<double, bool> OnVisibleCurrencyUpdate { get; set; }
        public Action<double, Vector2, Action> OnCurrencyUpdateFlow { get; set; }

        public double Currency
        {
            get
            {
                return Gybe.GetSystem<Save.SaveSystem>().Get<double>(saveKey);
            }
            private set
            {
                var currency = value < 0d ? 0d : value;
                Gybe.GetSystem<Save.SaveSystem>().Set(saveKey, currency);
            }
        }

        public double SetCurrency(double amount, bool isAnimated = false)
        {
            var difference = amount - Currency;
            Currency = amount;
            OnVisibleCurrencyUpdate?.Invoke(difference, isAnimated);
            return Currency;
        }

        public double SetCurrency(double amount, Vector2 senderPos, Action onComplete = null)
        {
            var difference = amount - Currency;
            Currency = amount;
            OnCurrencyUpdateFlow?.Invoke(difference, senderPos, onComplete);
            return Currency;
        }

        public double AddCurrency(double amount, bool isAnimated = false)
        {
            return SetCurrency(Currency + amount, isAnimated);
        }

        public double AddCurrency(double amount, Vector2 senderPos, Action onComplete = null)
        {
            return SetCurrency(Currency + amount, senderPos, onComplete);
        }

    }
}