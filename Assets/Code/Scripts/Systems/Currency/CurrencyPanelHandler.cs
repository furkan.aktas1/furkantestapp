﻿using GybeGames.Systems.AudioSystems.SoundSystem;
using GybeGames.Systems.Currency.Events;
using GybeGames.Systems.EventSystem;
using GybeGames.Utility.Extentions;
using System;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace GybeGames.Systems.Currency
{
    public class CurrencyPanelHandler : MonoBehaviour
    {
        [SerializeField] private CurrencyData currencyData = null;
        [SerializeField] private TextMeshProUGUI currencyText = null;
        [SerializeField] private Image currencyImage = null;
        private Sound sound = null;

        private Coroutine currentRoutine;
        private double shownCurrency;
        private double animatingCurrency = 0;

        private bool isInitialized = false;

        private void OnEnable()
        {
            if (!isInitialized)
            {
                isInitialized = true;
            }

            OnCurrencyUpdate(currencyData.Currency);
            shownCurrency = currencyData.Currency;

            currencyData.OnVisibleCurrencyUpdate += OnVisibleCurrencyUpdate;

            if (currencyData.IconSprite != null)
            {
                currencyImage.sprite = currencyData.IconSprite;
            }

            if (currencyData.WillTrackFlow)
            {
                currencyData.OnCurrencyUpdateFlow = OnCurrencyUpdateFlow;
            }

            if (currencyData.Sound)
            {
                sound = currencyData.Sound;
            }
            ListenEvents();
        }

        private void OnDisable()
        {
            currencyData.OnVisibleCurrencyUpdate -= OnVisibleCurrencyUpdate;

            if (currencyData.WillTrackFlow)
            {
                currencyData.OnCurrencyUpdateFlow = null;
            }

            if (currentRoutine != null)
            {
                StopCoroutine(currentRoutine);
                currentRoutine = null;
                shownCurrency += animatingCurrency;
            }
            RemoveEventListeners();
        }

        private void ListenEvents()
        {
            EventManager.RegisterHandler<CurrencySpentEvent>(OnCurrencySpent);
            EventManager.RegisterHandler<IsCurrencyEnoughEvent>(OnIsCurrencyEnoughEvent);
        }

        private void RemoveEventListeners()
        {
            EventManager.UnregisterHandler<CurrencySpentEvent>(OnCurrencySpent);
            EventManager.UnregisterHandler<IsCurrencyEnoughEvent>(OnIsCurrencyEnoughEvent);
        }

        private void OnIsCurrencyEnoughEvent(IsCurrencyEnoughEvent isCurrencyEnoughEvent)
        {
            if (currencyData.Currency >= isCurrencyEnoughEvent.Price)
            {
                isCurrencyEnoughEvent.HaveEnoughCallback?.Invoke();
            }
            else
            {
                isCurrencyEnoughEvent.NotHaveEnoughCallback?.Invoke();
            }
        }

        private void OnCurrencySpent(CurrencySpentEvent currencySpentEvent)
        {
            currencyData.AddCurrency(-currencySpentEvent.Amount);
        }

        void OnCurrencyUpdateFlow(double currency, Vector2 from, Action onComplete)
        {
            CurrencyParticleFlow currencyParticleFlow = new CurrencyParticleFlow(time: 0.7f, startScale: 1.2f, randomizePosition: 0.05f, count: 10, currencyAmount: currency);
            currencyParticleFlow.MoveFromTo(sprite: currencyData.IconSprite, fromWorldPosition: from, to: currencyImage.transform as RectTransform,
                parent: gameObject.transform, onComplete: (double? x) =>
            {
                onComplete?.Invoke();
                OnReach(x);
            });
        }

        void OnReach(double? amount)
        {
            if(amount != null)
                currencyData.OnVisibleCurrencyUpdate?.Invoke((double)amount, false);
        }

        void OnCurrencyUpdate(double currency)
        {
            var text = currencyData.WillAbbrivate ? currency.AbbrivateNum() : currency.ToString(currencyData.WillFormat ? "N0" : "#");
            text = string.IsNullOrWhiteSpace(text) ? "0" : text;
            if (!string.IsNullOrWhiteSpace(currencyData.SpecialFormat))
            {
                text = string.Format(currencyData.SpecialFormat, text);
            }
            currencyText.text = text;
        }

        void OnVisibleCurrencyUpdate(double currency, bool isInstant)
        {
            isInstant = !gameObject.activeInHierarchy || isInstant;
            if (currentRoutine != null)
            {
                StopCoroutine(currentRoutine);
                currentRoutine = null;
                shownCurrency += animatingCurrency;
            }

            if (sound)
                Gybe.GetSystem<SoundSystem>().PlayUI(sound);

            animatingCurrency = currency;

            if (isInstant)
            {
                OnCurrencyUpdate(shownCurrency + currency);
                shownCurrency += animatingCurrency;
            }
            else
            {
                currentRoutine = StartCoroutine(CurrencyUpdateRoutine(currency));
            }
        }

        private IEnumerator CurrencyUpdateRoutine(double targetCurrency)
        {
            double fromCurrency = shownCurrency;
            double toCurrency = shownCurrency + targetCurrency;
            float timer = 0f;
            float animTime = 1f;
            while (timer < animTime)
            {
                timer += Time.deltaTime;
                float t = timer / animTime;
                OnCurrencyUpdate(t.Lerp(fromCurrency, toCurrency));
                yield return null;
            }
            shownCurrency += animatingCurrency;
            OnCurrencyUpdate(shownCurrency);
            currentRoutine = null;
            animatingCurrency = 0;
        }

#if UNITY_EDITOR
        [Sirenix.OdinInspector.Button()]
        public void Editor_AddCoinWithFlow()
        {
            currencyData.AddCurrency(500, Vector2.zero);
        }

        [Sirenix.OdinInspector.Button()]
        public void Editor_AddCoinWithOnlyUpdate()
        {
            currencyData.AddCurrency(500);
        }
#endif

    }
}