﻿using GybeGames.Systems.EventSystem;

namespace GybeGames.Systems.Currency.Events
{
    public class CurrencySpentEvent : CustomEvent
    {
        public float Amount { get; private set; }

        public static CurrencySpentEvent Create(float amount)
        {
            CurrencySpentEvent currencySpentEvent = new CurrencySpentEvent();

            currencySpentEvent.Amount = amount;

            return currencySpentEvent;
        }
    }
}