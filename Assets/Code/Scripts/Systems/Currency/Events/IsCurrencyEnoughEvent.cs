﻿using GybeGames.Systems.EventSystem;
using System;

namespace GybeGames.Systems.Currency.Events
{
    public class IsCurrencyEnoughEvent : CustomEvent
    {
        public Action HaveEnoughCallback { get; private set; }
        public Action NotHaveEnoughCallback { get; private set; }
        public float Price { get; private set; }
        public static IsCurrencyEnoughEvent Create(float price, Action haveEnoughCallback, Action notHaveEnoughCallback)
        {
            IsCurrencyEnoughEvent isCurrencyEnoughEvent = new IsCurrencyEnoughEvent();
            isCurrencyEnoughEvent.HaveEnoughCallback = haveEnoughCallback;
            isCurrencyEnoughEvent.NotHaveEnoughCallback = notHaveEnoughCallback;
            isCurrencyEnoughEvent.Price = price;
            return isCurrencyEnoughEvent;
        }
    }
}
