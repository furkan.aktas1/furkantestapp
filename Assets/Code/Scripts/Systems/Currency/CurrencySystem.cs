﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Sirenix.OdinInspector;

namespace GybeGames.Systems.Currency
{
    public class CurrencySystem : BaseSystem<CurrencySystem>
    {
        [Title("References")]
        [SerializeField] private List<CurrencyData> currencyScriptables = new List<CurrencyData>();

        public CurrencyData GetCurrency(string key)
        {
            return currencyScriptables.FirstOrDefault(p => p.Key == key);
        }
    }
}