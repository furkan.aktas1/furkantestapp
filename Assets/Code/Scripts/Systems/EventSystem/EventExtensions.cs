using GybeGames.Systems.EventSystem;

namespace GybeGames
{
    public static class EventExtensions
    {
        public static void Send<T>(this T customEvent) where T : CustomEvent
        {
            EventManager.Send(customEvent);
        }
    }
}