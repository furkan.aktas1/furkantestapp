﻿using UnityEngine;


namespace GybeGames.Systems.EventSystem.Example
{
    public class EventTestListener : MonoBehaviour
    {
        private void OnEnable()
        {
            EventManager.RegisterHandler<StartEvent>(OnStartEvent);
        }

        private void OnDisable()
        {
            EventManager.UnregisterHandler<StartEvent>(OnStartEvent);
        }

        private void OnStartEvent(StartEvent startEvent)
        {
            Debug.Log(startEvent.StartEventMessage);
        }
    }
}
