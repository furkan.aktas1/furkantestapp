﻿
namespace GybeGames.Systems.EventSystem.Example
{
    public class StartEvent : CustomEvent
    {
        public string StartEventMessage { get; private set; }
        public static StartEvent Create(string startEventMessage)
        {
            StartEvent startEvent = new StartEvent();
            startEvent.StartEventMessage = startEventMessage;
            return startEvent;
        }
    }
}
