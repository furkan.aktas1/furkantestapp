﻿using UnityEngine;

namespace GybeGames.Systems.EventSystem.Example
{
    public class EventTestInvoker : MonoBehaviour
    {
        // Start is called before the first frame update
        void Start()
        {
            EventManager.Send(StartEvent.Create("Invoker Startladı reis"));
        }

    }
}

