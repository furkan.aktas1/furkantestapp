﻿using GybeGames.Systems.EventSystem;
using GybeGames.Systems.Save;
using GybeGames.Systems.Scores.Events;
using UnityEngine;

namespace GybeGames.Systems.Scores
{
    [DisallowMultipleComponent]
    public class ScoreSystem :  BaseSystem<ScoreSystem>
    {
        public double CurrentScore { get => currentScore; set => currentScore = value < 0 ? 0 : value; }
        public double PreviousScore { get => previousScore; private set => previousScore = value; }
        public bool HasNewBestScore { get => savedScores.FinalScore > savedScores.BestScore; }

        private double currentScore = 0;
        private double previousScore = 0;
        [SavedAs("Gybe.Score.Scores")] private SavedScores savedScores = new SavedScores();

        public void Reset()
        {
            CurrentScore = 0;
            PreviousScore = 0;
            savedScores.Reset();
            EventManager.Send(ScoreResetEvent.Create());
        }

        public void Add(int amount)
        {
            PreviousScore = CurrentScore;
            CurrentScore += amount;
            CurrentScore = CurrentScore < 0 ? 0 : CurrentScore;
            savedScores.Add(currentScore: CurrentScore);
            Scores scores = new Scores(currentScore: CurrentScore, previousScore: PreviousScore, finalScore: savedScores.FinalScore, bestScore: savedScores.BestScore);
            EventManager.Send(ScoreAddedEvent.Create(scores));

            if ( HasNewBestScore)
            {
                EventManager.Send(ScoreNewBestEvent.Create(scores));
            }
        }
    }
}