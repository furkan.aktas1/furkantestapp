﻿using GybeGames.Systems.EventSystem;

namespace GybeGames.Systems.Scores.Events
{
    public class ScoreResetEvent : CustomEvent
    {
        public static ScoreResetEvent Create()
        {
            return new ScoreResetEvent();
        }
    }
}

