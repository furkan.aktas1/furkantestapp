﻿using GybeGames.Systems.EventSystem;

namespace GybeGames.Systems.Scores.Events
{
    public class ScoreAddedEvent : CustomEvent
    {
        public Scores Scores { get; private set; }

        public static ScoreAddedEvent Create(Scores scores)
        {
            ScoreAddedEvent scoreEvent = new ScoreAddedEvent();
            scoreEvent.Scores = scores;
            return scoreEvent;
        }
    }
}

