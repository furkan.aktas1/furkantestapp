﻿using GybeGames.Systems.EventSystem;

namespace GybeGames.Systems.Scores.Events
{
    public class ScoreNewBestEvent : CustomEvent
    {
        public Scores Scores { get; private set; }

        public static ScoreNewBestEvent Create( Scores scores)
        {
            ScoreNewBestEvent newBestEvent = new ScoreNewBestEvent();
            newBestEvent.Scores = scores;
            return newBestEvent;
        }
    }
}