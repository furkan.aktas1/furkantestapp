﻿namespace GybeGames.Systems.Scores
{
    public class SavedScores
    {
        public double FinalScore;
        public double BestScore;

        public void Add(double currentScore)
        {
            FinalScore = currentScore;
            BestScore = FinalScore > BestScore ? FinalScore : BestScore;
        }

        public void Reset()
        {
            FinalScore = 0;
            BestScore = 0;
        }
    }
}
