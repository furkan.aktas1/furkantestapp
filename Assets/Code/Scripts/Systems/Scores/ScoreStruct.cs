﻿namespace GybeGames.Systems.Scores
{
    public struct Scores
    {

        public double CurrentScore { get => currentScore; }
        public double PreviousScore { get => previousScore; }
        public double FinalScore { get => finalScore; }
        public double BestScore { get => bestScore; }

        private double currentScore;
        private double previousScore;
        private double finalScore;
        private double bestScore;

        public Scores(double currentScore, double previousScore, double finalScore, double bestScore)
        {
            this.currentScore = currentScore;
            this.previousScore = previousScore;
            this.finalScore = finalScore;
            this.bestScore = bestScore;
        }

    }
}

