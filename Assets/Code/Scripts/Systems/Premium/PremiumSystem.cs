﻿using GybeGames.Systems.Save;

namespace GybeGames.Systems.Premium
{
    public class PremiumSystem : BaseSystem<PremiumSystem>
    {
        public bool HasNoAd { get => hasNoAd; }

        [SavedAs("Gybe.Premium.HasNoAd")] private bool hasNoAd = false;

        public void BuyHasNoAd()
        {
            hasNoAd = true;
            Save();
        }
    }
}
