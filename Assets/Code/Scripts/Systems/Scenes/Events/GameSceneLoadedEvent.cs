﻿using GybeGames.Systems.EventSystem;
using UnityEngine.SceneManagement;

namespace GybeGames.Systems.Scenes.Events
{
    public class GameSceneLoadedEvent : CustomEvent
    {
        public Scene Scene { get; private set; }
        public static GameSceneLoadedEvent Create(Scene scene)
        {
            GameSceneLoadedEvent sceneEvent = new GameSceneLoadedEvent();
            sceneEvent.Scene = scene;
            return sceneEvent;
        }
    }
}
