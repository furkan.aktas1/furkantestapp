﻿using GybeGames.Systems.EventSystem;
using UnityEngine.SceneManagement;

namespace GybeGames.Systems.Scenes.Events
{
    public class SceneUnloadedEvent : CustomEvent
    {
        public Scene Scene { get; private set; }
        public static SceneUnloadedEvent Create(Scene scene)
        {
            SceneUnloadedEvent sceneEvent = new SceneUnloadedEvent();
            sceneEvent.Scene = scene;
            return sceneEvent;
        }
    }
}

