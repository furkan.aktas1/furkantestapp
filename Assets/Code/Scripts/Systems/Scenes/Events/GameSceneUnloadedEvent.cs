﻿using GybeGames.Systems.EventSystem;
using UnityEngine.SceneManagement;

namespace GybeGames.Systems.Scenes.Events
{
    public class GameSceneUnloadedEvent : CustomEvent
    {
        public Scene Scene { get; private set; }
        public static GameSceneUnloadedEvent Create(Scene scene)
        {
            GameSceneUnloadedEvent sceneEvent = new GameSceneUnloadedEvent();
            sceneEvent.Scene = scene;
            return sceneEvent;
        }
    }
}
