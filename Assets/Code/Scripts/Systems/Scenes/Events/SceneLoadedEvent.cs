﻿using GybeGames.Systems.EventSystem;
using UnityEngine.SceneManagement;

namespace GybeGames.Systems.Scenes.Events
{
    public class SceneLoadedEvent : CustomEvent
    {
        public Scene Scene { get; private set; }
        public static SceneLoadedEvent Create(Scene scene)
        {
            SceneLoadedEvent sceneEvent = new SceneLoadedEvent();
            sceneEvent.Scene = scene;
            return sceneEvent;
        }
    }
}
