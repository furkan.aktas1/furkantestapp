﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Linq;
using System;
using Sirenix.OdinInspector;
using UnityEngine.Assertions;
using GybeGames.Systems.EventSystem;
using GybeGames.Systems.Levels;
using GybeGames.Systems.Scenes.Events;
using GybeGames.Systems.Windows.Loading;
using GybeGames.Systems.Windows;

namespace GybeGames.Systems.Scenes
{
    [DefaultExecutionOrder(999)]
    [DisallowMultipleComponent]
    public class SceneSystem : BaseSystem<SceneSystem>, ISceneSystem, ILoadingApperable
    {
        private bool MAKE_LAST_ADDITIVE_SCENE_ACTIVE = true;

        [Title("References")]
        [SerializeField] private List<SceneReference> permanentScenes = new List<SceneReference>();
#pragma warning disable CS0414 // Remove never used warning
        [SerializeField] private SceneReference mainScene = null;
#pragma warning restore CS0414 // Remove Never used warning
        [SerializeField] private SceneReference initialScene = null;
        [SerializeField] private SceneReference gameArtScene = null;
        [SerializeField] private List<SceneReference> gameScenes = null;

        public List<SceneReference> GameScenes { get => gameScenes; }

        private List<AsyncOperation> loadingOperations = new List<AsyncOperation>();
        private List<string> activeScenes = new List<string>();

        public override void Initialize()
        {
            SceneManager.sceneLoaded += OnAnySceneLoaded;
            SceneManager.sceneUnloaded += OnAnySceneUnLoaded;
        }

        protected override void Start()
        {
            base.Start();
            LoadPermanentScenes();
            //LoadInitialScene();
            StartCoroutine(LateInitialize());
        }

        private IEnumerator LateInitialize()
        {
            yield return new WaitForEndOfFrame();
            LevelSystem levelSystem = Gybe.GetSystem<LevelSystem>();
            levelSystem.ConfigureAndLoadLevel();
        }

        public override void DeInitialize()
        {
            SceneManager.sceneLoaded -= OnAnySceneLoaded;
            SceneManager.sceneUnloaded -= OnAnySceneUnLoaded;
            base.DeInitialize();
        }

        /// <summary>
        /// Load scene asynchronusly with or without loading window.
        /// </summary>
        /// <param name="sceneName">Scene path to load</param>
        /// <param name="sceneOrderIndex">Which order the scene will be in after permanent scenes</param>
        /// <param name="loadingType">Loading window shown type</param>
        /// <param name="OnSceneLoaded">On scene loaded callback</param>
        public void LoadScene(string sceneName, int sceneOrderIndex = 0, LoadingType loadingType = LoadingType.None, Action OnSceneLoaded = null)
        {
            if (loadingType != LoadingType.None)
            {
                StartCoroutine(LoadAdditiveSceneAsyncWithLoading(sceneName, sceneOrderIndex, loadingType, OnSceneLoaded));
            }
            else
            {
                StartCoroutine(LoadAdditiveSceneAsync(sceneName, sceneOrderIndex, OnSceneLoaded));
            }
        }

        public void LoadGameScene()
        {
            LoadScene(gameScenes[0], loadingType: LoadingType.InAndOut);
        }

        public bool IsSceneOpen(string sceneNameOrPath)
        {
            var sceneCount = SceneManager.sceneCount;
            for (int i = 0; i < sceneCount; i++)
            {
                var scene = SceneManager.GetSceneAt(i);
                if (scene.name == sceneNameOrPath || scene.path == sceneNameOrPath)
                {
                    return true;
                }
            }
            return false;
        }


        public bool IsSceneValid(string sceneNameOrPath)
        {
            var sceneCountInBuildSettings = SceneManager.sceneCountInBuildSettings;
            for (int i = 0; i < sceneCountInBuildSettings; i++)
            {
                var scenePath = SceneUtility.GetScenePathByBuildIndex(i);
                int lastSlash = scenePath.LastIndexOf("/");
                var sceneName = scenePath.Substring(lastSlash + 1, scenePath.LastIndexOf(".") - lastSlash - 1);
                if (scenePath == sceneNameOrPath || sceneName == sceneNameOrPath)
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Load scene asycnhronously. If there are scenes on or before the scene order, unload these scenes first.
        /// </summary>
        /// <param name="sceneName">Scene path to laod</param>
        /// <param name="sceneOrderIndex">Which order the scene will be in after permanent scenes</param>
        /// <param name="OnSceneLoaded">On scene loaded callback</param>
        /// <returns></returns>
        private IEnumerator LoadAdditiveSceneAsync(string sceneName, int sceneOrderIndex, Action OnSceneLoaded = null)
        {
            if (!IsSceneValid(sceneName))
            {
                Debug.LogWarning($"{sceneName} is not a valid scene!");
                yield break;
            }

            if (activeScenes.Count > sceneOrderIndex && IsSceneOpen(activeScenes[sceneOrderIndex]))
            {
                var totalCount = activeScenes.Count - 1;
                for (int i = totalCount; i >= sceneOrderIndex; i--)
                {
                    var unloadingOperation = SceneManager.UnloadSceneAsync(activeScenes[i]);
                    activeScenes.Remove(activeScenes[i]);
                    yield return new WaitUntil(() => unloadingOperation.isDone);
                }
            }

            var loadingOperation = SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);

#if UNITY_ASSERTIONS
            Assert.IsNotNull(loadingOperation);
#endif

            loadingOperations.Add(loadingOperation);

            yield return new WaitUntil(() => loadingOperation.isDone);

            if (loadingOperations.Contains(loadingOperation))
            {
                loadingOperations?.Remove(loadingOperation);
            }

            activeScenes.Add(sceneName);

            if (MAKE_LAST_ADDITIVE_SCENE_ACTIVE)
            {
                Scene loadedScene = SceneManager.GetSceneByPath(sceneName);
                SceneManager.SetActiveScene(loadedScene);
            }

            yield return new WaitUntil(() => loadingOperations.All(x => x.isDone));
            OnSceneLoaded?.Invoke();
        }

        /// <summary>
        /// Load scene asycnhronously with loading window.
        /// </summary>
        /// <param name="sceneName"></param>
        /// <param name="sceneOrderIndex"></param>
        /// <param name="loadingType"></param>
        /// <param name="OnSceneLoaded"></param>
        /// <returns></returns>
        private IEnumerator LoadAdditiveSceneAsyncWithLoading(string sceneName, int sceneOrderIndex, LoadingType loadingType, Action OnSceneLoaded = null)
        {
            yield return new WaitForEndOfFrame();

            var loadingWindow = Gybe.GetSystem<WindowSystem>().ForceGet<LoadingWindow>();

            yield return StartCoroutine(AppearLoadingWindow(loadingWindow, loadingType));

            yield return StartCoroutine(LoadAdditiveSceneAsync(sceneName, sceneOrderIndex, OnSceneLoaded));

            yield return StartCoroutine(DisappearLoadingWindow(loadingWindow, loadingType));
        }

        #region ILoadingAppearable

        public IEnumerator AppearLoadingWindow(LoadingWindow loadingWindow, LoadingType loadingType)
        {
            if (loadingWindow != null && (loadingType == LoadingType.InOnly || loadingType == LoadingType.InAndOut))
            {
                loadingWindow.Appear();
                yield return new WaitUntil(() => loadingWindow.IsVisible);
            }
        }

        public IEnumerator DisappearLoadingWindow(LoadingWindow loadingWindow, LoadingType loadingType)
        {
            if (loadingWindow != null && (loadingType == LoadingType.OutOnly || loadingType == LoadingType.InAndOut))
            {
                loadingWindow.Disappear();
                yield return new WaitUntil(() => !loadingWindow.IsVisible);
            }
        }

        #endregion

        private void LoadInitialScene()
        {
            StartCoroutine(LoadAdditiveSceneAsyncWithLoading(initialScene, 0, LoadingType.OutOnly));
        }

        /// <summary>
        /// Load permanent scenes. These scenes will be active throughout the whole game.
        /// </summary>
        private void LoadPermanentScenes()
        {
            foreach (var item in permanentScenes)
            {
                if (!IsSceneOpen(item) && IsSceneValid(item))
                {
                    SceneManager.LoadScene(item, LoadSceneMode.Additive);
                }
            }
        }

        #region Triggers & Events

        private void OnAnySceneLoaded(Scene scene, LoadSceneMode mode)
        {
            SceneReference sceneReferences = gameScenes.Find(x => x.ScenePath == scene.path);
            if (sceneReferences != null)
            {
                EventManager.Send(GameSceneLoadedEvent.Create(scene));
            }
            EventManager.Send(SceneLoadedEvent.Create(scene));
        }

        private void OnAnySceneUnLoaded(Scene scene)
        {
            SceneReference sceneReferences = gameScenes.Find(x => x.ScenePath == scene.path);
            if (sceneReferences != null)
            {
                EventManager.Send(GameSceneUnloadedEvent.Create(scene));
            }
            EventManager.Send(SceneUnloadedEvent.Create(scene));
        }

        #endregion

#if UNITY_EDITOR
        [Button]
        public void Editor_OpenArtGameScene()
        {
            LoadScene(gameArtScene, loadingType: LoadingType.InAndOut);
        }
        [Title("Examples")]
        [Button]
        public void Editor_OpenGameScene()
        {
            LoadGameScene();
        }
#endif
    }
}