using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

namespace GybeGames.Systems.Scenes.Editor
{
    [InitializeOnLoadAttribute]
    public class SceneEditorTool
    {
        private const string MAIN_SCENE = "MainScene";
        private const string INITIAL_SCENE = "InitialScene";
        private const string GAME_SCENE = "GameScene";
        private const string LEVEL_CREATION_SCENE = "LevelCreationScene";
        private const string ART_GAME_SCENE = "ArtGameScene";
        private const string SCENE_EXTENTION = ".unity";

        [MenuItem("Scene Tool/Open Main Scene %m", priority = 0)]
        public static void OpenMainScene()
        {
            OpenScene(MAIN_SCENE);
        }

        [MenuItem("Scene Tool/Open Initial Scene", priority = 1)]
        public static void OpenInitialScene()
        {
            OpenScene(INITIAL_SCENE);
        }

        [MenuItem("Scene Tool/Open Game Scene %g", priority = 2)]
        public static void OpenGameScene()
        {
            OpenScene(GAME_SCENE);
        }

        [MenuItem("Scene Tool/Open Level Creation Scene", priority = 3)]
        public static void OpenLevelCreationScene()
        {
            OpenScene(LEVEL_CREATION_SCENE);
        }

        [MenuItem("Scene Tool/Open Art Game Scene %l", priority = 4)]
        public static void OpenArtGameScene()
        {
            OpenScene(ART_GAME_SCENE);
        }

        private static void OpenScene(string sceneName)
        {
            EditorBuildSettingsScene[] scenes = EditorBuildSettings.scenes;
            for (int i = 0; i < scenes.Length; i++)
            {
                string[] authorsList = scenes[i].path.Split('/');
                string sceneNameWithExtention = authorsList[authorsList.Length - 1];
                if (sceneName + SCENE_EXTENTION == sceneNameWithExtention)
                {
                    EditorSceneManager.OpenScene(scenes[i].path);
                    return;
                }
            }
            Debug.LogError($"There is no scene with the name of {sceneName}");
        }

        [MenuItem("Scene Tool/Open Main + Game Scene", priority = 0)]
        public static void OpenMainAndGameScenes()
        {
            EditorBuildSettingsScene[] scenes = EditorBuildSettings.scenes;
            for (int i = 0; i < scenes.Length; i++)
            {
                string[] authorsList = scenes[i].path.Split('/');
                string sceneNameWithExtention = authorsList[authorsList.Length - 1];
                if (MAIN_SCENE + SCENE_EXTENTION == sceneNameWithExtention)
                {
                    EditorSceneManager.OpenScene(scenes[i].path);
                }
                if (GAME_SCENE + SCENE_EXTENTION == sceneNameWithExtention)
                {
                    EditorSceneManager.OpenScene(scenes[i].path, OpenSceneMode.Additive);
                }
            }
        }

        static SceneEditorTool()
        {
            EditorApplication.playModeStateChanged += UnloadGameScene;
        }

        private static void UnloadGameScene(PlayModeStateChange state)
        {
            if (state == PlayModeStateChange.EnteredPlayMode)
            {
                if (EditorSceneManager.GetSceneByName(GAME_SCENE).isLoaded && EditorSceneManager.loadedSceneCount > 1)
                {
                    EditorSceneManager.UnloadSceneAsync(GAME_SCENE);
                }
            }
        }

    }
}

