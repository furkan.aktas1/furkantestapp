﻿using System.Collections;
using UnityEngine;

namespace GybeGames.Systems.Scenes.Example
{
    public class GameSceneTest : MonoBehaviour
    {
        // Start is called before the first frame update
        IEnumerator Start()
        {
            yield return new WaitForSeconds(5);
            Gybe.GetSystem<SceneSystem>().LoadGameScene();
        }

    }
}

