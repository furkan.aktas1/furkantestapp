﻿using GybeGames.Systems.Windows.Loading;
using System;
using System.Collections.Generic;

namespace GybeGames.Systems.Scenes
{
    public interface ISceneSystem
    {
        List<SceneReference> GameScenes { get; }
        void LoadScene(string sceneName, int sceneOrderIndex = 0, LoadingType loadingType = LoadingType.None, Action OnSceneLoaded = null);
        bool IsSceneOpen(string sceneNameOrPath);
        bool IsSceneValid(string sceneNameOrPath);
    }
}

