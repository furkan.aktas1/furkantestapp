﻿using Lean.Pool;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GybeGames.Utility.Extentions;
using GybeGames.Systems;

namespace GybeGames.Systems.InGamePoolSystem
{
    public abstract class BaseInGamePoolController<T> : MonoBehaviour, IDespawnablePoolController where T : Component
    {
        protected List<T> spawnedObjects = new List<T>();

        private void OnEnable()
        {
            Initialize();
        }

        private void OnDisable()
        {
            DeInitialize();
        }

        public virtual void Initialize()
        {
            Gybe.GetSystem<GamePlayPoolSystem>().AddToPool(this);
        }

        public virtual void DeInitialize()
        {
            Gybe.GetSystem<GamePlayPoolSystem>().RemoveFromPool(this);
        }

        public virtual U Spawn<U>(U prefab, Transform parent, bool worldPositionStays = false) where U : T
        {
            U pooledObject = LeanPool.Spawn(prefab, parent, worldPositionStays);
            spawnedObjects.Add(pooledObject);
            return pooledObject;
        }

        public virtual U Spawn<U>(U prefab, Vector3 position, Quaternion rotation) where U : T
        {
            U pooledObject = LeanPool.Spawn(prefab, position, rotation);
            spawnedObjects.Add(pooledObject);
            return pooledObject;
        }

        public virtual void Despawn(T prefab)
        {
            for (int i = 0; i < spawnedObjects.Count; i++)
            {
                if (spawnedObjects[i] == prefab)
                {
                    LeanPool.Despawn(spawnedObjects[i]);
                    spawnedObjects.RemoveAt(i);
                    return;
                }
            }
        }

        public virtual void DespawnAll()
        {
            spawnedObjects.DespawnCachedList();
        }

    }
}