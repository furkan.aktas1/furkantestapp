﻿

namespace GybeGames.Systems.InGamePoolSystem
{
    public interface IDespawnablePoolController
    {
        void DespawnAll();
    }
}
