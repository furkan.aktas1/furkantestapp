﻿#if UNITY_EDITOR
using Lean.Pool;
using System.Collections.Generic;
using UnityEngine;

namespace GybeGames.Systems.InGamePoolSystem.Example
{
    public class PoolInvokerExample : MonoBehaviour
    {
        [SerializeField] private PoolableExample poolable = null;

        private List<PoolableExample> pool = new List<PoolableExample>();

        [Sirenix.OdinInspector.Button()]
        public void Spawn()
        {
            PoolableExample poolableExample = LeanPool.Spawn<PoolableExample>(poolable);
            if (!pool.Contains(poolableExample))
                pool.Add(poolableExample);
        }

        [Sirenix.OdinInspector.Button()]
        public void OnDespawn()
        {
            for (int i = 0; i < pool.Count; i++)
            {
                if (pool[i].gameObject.activeInHierarchy)
                {
                    LeanPool.Despawn(pool[i]);
                    break;
                }
            }
        }
    }
}
#endif