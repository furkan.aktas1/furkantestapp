﻿#if UNITY_EDITOR
using Lean.Pool;
using UnityEngine;

namespace GybeGames.Systems.InGamePoolSystem.Example
{
    public class PoolableExample : MonoBehaviour, IPoolable
    {
        private void Awake()
        {
            Debug.Log("Awake of Poolable Example");
        }

        private void OnEnable()
        {
            Debug.Log("OnEnable of Poolable Example");
        }

        private void OnDisable()
        {
            Debug.Log("OnDisable of Poolable Example");
        }

        void Start()
        {
            Debug.Log("Start of Poolable Example");
        }

        public void OnDespawn()
        {
            Debug.Log("OnDespawn of Poolable Example");
        }

        public void OnSpawn()
        {
            Debug.Log("OnSpawn of Poolable Example");
        }

    }
}
#endif
