﻿using GybeGames.Systems;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GybeGames.Systems.InGamePoolSystem
{
    public class GamePlayPoolSystem : BaseSystem<GamePlayPoolSystem>
    {
        private Dictionary<object, IDespawnablePoolController> poolSystemDictionary = new Dictionary<object, IDespawnablePoolController>();

        public void AddToPool(IDespawnablePoolController poolManagerInstance)
        {
            if (!poolSystemDictionary.ContainsKey(poolManagerInstance))
            {
                poolSystemDictionary.Add(poolManagerInstance.GetType(), poolManagerInstance);
            }
        }

        public T GetPoolManager<T>()
        {
            var typeOfObject = typeof(T);

            T poolManagerInstance = default(T);

            if (poolSystemDictionary.ContainsKey(typeOfObject))
            {
                poolManagerInstance = (T)poolSystemDictionary[typeOfObject];
            }

            return poolManagerInstance;
        }

        public void RemoveFromPool(object poolManagerInstance)
        {
            if (poolSystemDictionary.ContainsKey(poolManagerInstance.GetType()))
            {
                poolSystemDictionary.Remove(poolManagerInstance.GetType());
            }
        }

        public void DespawnAll()
        {
            foreach (KeyValuePair<object, IDespawnablePoolController> poolManager in poolSystemDictionary)
            {
                var poolManagerInstance = poolManager.Value;
                poolManagerInstance.DespawnAll();
            }
        }
    }
}
