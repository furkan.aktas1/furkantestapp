﻿using Lean.Pool;
using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GybeGames.Systems.InGamePoolSystem
{
    public class WinFXPoolController : BaseInGamePoolController<ParticleSystem>
    {
        [SerializeField, Required] private ParticleSystem winFXPrefab = null;

        public ParticleSystem SetWinFx(Vector3 position, Quaternion rotation)
        {
            return Spawn(winFXPrefab, position, rotation);
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        public override void DeInitialize()
        {
            base.DeInitialize();
        }

    }

}