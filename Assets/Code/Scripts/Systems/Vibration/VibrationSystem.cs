﻿using UnityEngine;
using Lofelt.NiceVibrations;
using GybeGames.Systems.EventSystem;
using GybeGames.Systems.Save;

namespace GybeGames.Systems.Vibration
{
    [DisallowMultipleComponent]
    public class VibrationSystem : BaseSystem<VibrationSystem>
    {

        public bool IsVibrationEnabled { get => isVibrationEnabled; private set => isVibrationEnabled = value; }

        private static double lastHapticTime = 0d;
        [SavedAs("Gybe.Vibration.IsVibrationEnabled")] private bool isVibrationEnabled = true;

        private bool isSupported = false;

        public override void Initialize()
        {
            base.Initialize();
            isSupported = IsHapticsSupported();
        }

        public override void SetDefaults()
        {
            base.SetDefaults();
#if UNITY_ANDROID
            isVibrationEnabled = false;
#endif
#if UNITY_IOS
            isVibrationEnabled = true;
#endif
        }

        public bool IsHapticsSupported()
        {
            return DeviceCapabilities.isVersionSupported;
        }

        public void TriggerHaptic(HapticPatterns.PresetType vibrationType = HapticPatterns.PresetType.MediumImpact)
        {
            if (!IsVibrationEnabled) return;
            if (Time.time - lastHapticTime > 0.05)
            {
                TriggerHapticIfEnabled(vibrationType);
                lastHapticTime = Time.time;
            }
        }

        public void TriggerSelection()
        {
            TriggerHapticIfEnabled(HapticPatterns.PresetType.Selection);
        }

        public void TriggerSuccess()
        {
            TriggerHapticIfEnabled(HapticPatterns.PresetType.Success);
        }

        public void TriggerWarning()
        {
            TriggerHapticIfEnabled(HapticPatterns.PresetType.Warning);
        }

        public void TriggerFailure()
        {
            TriggerHapticIfEnabled(HapticPatterns.PresetType.Failure);
        }

        public void TriggerLight()
        {
            TriggerHapticIfEnabled(HapticPatterns.PresetType.LightImpact);
        }

        public void TriggerMedium()
        {
            TriggerHapticIfEnabled(HapticPatterns.PresetType.MediumImpact);
        }

        public void TriggerHeavy()
        {
            TriggerHapticIfEnabled(HapticPatterns.PresetType.HeavyImpact);
        }

        public bool ChangeVibrationStatus()
        {
            IsVibrationEnabled = !IsVibrationEnabled;
            EventManager.Send(VibrationChangeEvent.Create(IsVibrationEnabled));
            TriggerMedium();
            return IsVibrationEnabled;
        }

        private void TriggerHapticIfEnabled(HapticPatterns.PresetType type)
        {
            if (!isSupported) return;
            if (!IsVibrationEnabled) return;
            HapticPatterns.PlayPreset(type);
        }
    }
}
