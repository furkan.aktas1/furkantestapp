﻿using UnityEngine;

namespace GybeGames.Systems.Vibration.Example
{
    public class VibrationDeneme : MonoBehaviour
    {
        public void _Failure()
        {
            Gybe.GetSystem<VibrationSystem>().TriggerFailure();
        }

        public void _Heavy()
        {
            Gybe.GetSystem<VibrationSystem>().TriggerHeavy();
        }

        public void _Light()
        {
            Gybe.GetSystem<VibrationSystem>().TriggerLight();
        }

        public void _Selection()
        {
            Gybe.GetSystem<VibrationSystem>().TriggerSelection();
        }

        public void _Medium()
        {
            Gybe.GetSystem<VibrationSystem>().TriggerMedium();
        }

        public void _Success()
        {
            Gybe.GetSystem<VibrationSystem>().TriggerSuccess();
        }

        public void _Warning()
        {
            Gybe.GetSystem<VibrationSystem>().TriggerWarning();
        }
    }
}
