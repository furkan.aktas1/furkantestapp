﻿using GybeGames.Systems.EventSystem;

namespace GybeGames.Systems.Vibration
{
    public class VibrationChangeEvent : CustomEvent
    {
        public bool VibrationStatus { get; private set; }

        public static VibrationChangeEvent Create(bool vibrationStatus)
        {
            VibrationChangeEvent vibrationChangeEvent = new VibrationChangeEvent();
            vibrationChangeEvent.VibrationStatus = vibrationStatus;
            return vibrationChangeEvent;
        }
    }
}

