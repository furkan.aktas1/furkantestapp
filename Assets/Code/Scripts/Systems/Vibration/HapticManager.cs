using GybeGames.Systems.EventSystem;
using GybeGames.Systems.GameStates.Events;
using UnityEngine;

namespace GybeGames.Systems.Vibration
{
    public class HapticManager : MonoBehaviour
    {
        private VibrationSystem vibrationSystem { get => _vibrationSystem ?? (_vibrationSystem = Gybe.GetSystem<VibrationSystem>()); }

        private VibrationSystem _vibrationSystem = null;

        private void OnEnable()
        {
            ListenEvents();
        }

        private void OnDisable()
        {
            RemoveEventListeners();
        }

        private void ListenEvents()
        {
            EventManager.RegisterHandler<GameWonEvent>(OnGameWon);
            EventManager.RegisterHandler<GameFailedEvent>(OnGameFailed);
        }

        private void RemoveEventListeners()
        {
            EventManager.UnregisterHandler<GameWonEvent>(OnGameWon);
            EventManager.UnregisterHandler<GameFailedEvent>(OnGameFailed);
        }

        private void OnGameWon(GameWonEvent gameWonEvent)
        {

        }

        private void OnGameFailed(GameFailedEvent gameFailedEvent)
        {

        }
    }
}

