﻿using GybeGames.Systems;
using GybeGames.Systems.Windows;
using GybeGames.Utility.Extentions;
using UnityEngine;

namespace GybeGames.Windows
{
    public class GetTestWindow : MonoBehaviour
    {
        void Start()
        {
            Gybe.GetSystem<WindowSystem>().ForceGet<WindowsTest>().Show();
        }
    }
}
