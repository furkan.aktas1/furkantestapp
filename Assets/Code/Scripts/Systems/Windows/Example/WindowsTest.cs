﻿using GybeGames.Systems.Windows;
using System.Collections;
using UnityEngine;

namespace GybeGames.Windows
{
    public class WindowsTest : BaseWindow
    {
        protected override IEnumerator AppearProgress()
        {
            Debug.Log("AppearProgress");
            yield return null;
        }

        protected override IEnumerator DisappearProgress()
        {
            Debug.Log("Disappearing");
            yield return null;
        }
    }
}

