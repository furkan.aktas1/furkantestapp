﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using GybeGames.Systems.Cameras;
using Sirenix.OdinInspector;

namespace GybeGames.Systems.Windows
{
    [DisallowMultipleComponent]
    public class WindowSystem : BaseSystem<WindowSystem>
    {
        public const int PLANE_DISTANCE = 100;

        [Title("References")]
        [SerializeField] private List<BaseWindow> windowPrefabs = new List<BaseWindow>();

        public List<BaseWindow> WindowPrefabs { get => windowPrefabs; }
        public List<IWindow> Windows { get => windows; }

        private List<IWindow> windows = new List<IWindow>();

        public override void Initialize()
        {
            base.Initialize();
        }

        public void Add(IWindow panel)
        {
            if (!windows.Contains(panel))
            {
                windows.Add(panel);
            }
        }

        public void Remove(IWindow panel)
        {
            if (windows.Contains(panel))
            {
                windows.Remove(panel);
            }
        }

        public T Create<T>(IWindow prefab = null) where T : IWindow
        {
            var panel = default(T);
            prefab = prefab ?? windowPrefabs.OfType<T>().FirstOrDefault();

            if (prefab == null)
            {
                Debug.LogError("Popup prefab can not be found on popupPrefabs List. Please add prefab to list on PopupService!");
                return panel;
            }

            panel = (T)Instantiate((BaseWindow)prefab).GetComponent<IWindow>();
            panel?.RectTransform.SetAsLastSibling();
            if(panel != null && panel.Canvas != null && panel.CameraRenderMode == RenderMode.ScreenSpaceCamera)
            {
                panel.Canvas.renderMode = RenderMode.ScreenSpaceCamera;
                panel.Canvas.worldCamera = Gybe.GetSystem<CameraSystem>().UICamera;
                panel.Canvas.planeDistance = PLANE_DISTANCE;

            }
            return panel;
        }

        public T Get<T>() where T : IWindow
        {
            return windows.OfType<T>().LastOrDefault();
        }

        public T ForceGet<T>(IWindow prefab = null) where T : IWindow
        {
            T iWindow = Get<T>();
            return iWindow == null ? Create<T>(prefab) : iWindow;
        }

        public void Show<T>(float? delay = null, float? duration = null) where T : IWindow
        {
            Get<T>()?.Appear(delay, duration);
        }

        public void Hide<T>(float? delay = null, float? duration = null) where T : IWindow
        {
            Get<T>()?.Disappear(delay, duration);
        }

        public bool IsVisible<T>() where T : IWindow
        {
            return Get<T>()?.IsVisible ?? false;
        }

    }
}
