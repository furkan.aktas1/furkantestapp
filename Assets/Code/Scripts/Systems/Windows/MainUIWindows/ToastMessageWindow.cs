﻿using DG.Tweening;
using GybeGames.Systems.Windows;
using Sirenix.OdinInspector;
using System.Collections;
using TMPro;
using UnityEngine;

namespace GybeGames.Systems.Windows.MainUIWindows
{
    public class ToastMessageWindow : BaseWindow
    {
        [SerializeField, Required] private CanvasGroup textCanvasGroup = null;
        [SerializeField, Required] private RectTransform textPanelRectTransform = null;
        [SerializeField, Required] private TextMeshProUGUI toastText = null;

        protected override IEnumerator AppearProgress()
        {
            yield return null;
        }

        protected override IEnumerator DisappearProgress()
        {
            yield return null;
        }

        private void OnDisable()
        {
            DOTween.Kill(textCanvasGroup);
            DOTween.Kill(textPanelRectTransform);
        }

        [Button]
        public void ShowPanel(string message)
        {
            DOTween.Kill(textCanvasGroup);
            textCanvasGroup.alpha = 0;
            toastText.text = message;
            var seq = DOTween.Sequence();
            seq.Append(textCanvasGroup.DOFade(1, 0.1f));
            seq.AppendInterval(1.2f);
            seq.Append(textCanvasGroup.DOFade(0, 0.1f)).SetId(textCanvasGroup);
            seq.Play();
            DOTween.Kill(textPanelRectTransform);
            textPanelRectTransform.anchoredPosition = new Vector2(0, -250);
            textPanelRectTransform.DOAnchorPos(Vector2.zero, 2f);
        }
    }
}
