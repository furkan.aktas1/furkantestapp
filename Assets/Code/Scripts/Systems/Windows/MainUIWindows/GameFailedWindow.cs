﻿using GybeGames.Systems.Levels;
using System.Collections;

namespace GybeGames.Systems.Windows.MainUIWindows
{
    public class GameFailedWindow : BaseWindow
    {
        protected override IEnumerator AppearProgress()
        {
            yield return null;
        }

        protected override IEnumerator DisappearProgress()
        {
            yield return null;
        }

        public void _RetryButton()
        {
            SetActiveBlocker(true);
            LevelSystem levelSystem = Gybe.GetSystem<LevelSystem>();
            levelSystem.ConfigureAndLoadLevel();
        }
    }
}

