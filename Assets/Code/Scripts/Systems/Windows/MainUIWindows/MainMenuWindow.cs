﻿using System.Collections;

namespace GybeGames.Systems.Windows.MainUIWindows
{
    public class MainMenuWindow : BaseWindow
    {

        protected override IEnumerator AppearProgress()
        {
            yield return null;
        }

        protected override IEnumerator DisappearProgress()
        {
            yield return null;
        }
    }
}
