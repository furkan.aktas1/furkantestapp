﻿using GybeGames.SafeAreaContents;
using UnityEngine;

namespace GybeGames.Systems.Windows.MainUIWindows
{
    public class StaticMainCanvas : BaseWindow
    {
        public RectTransform SafeAreaRectTransform { get => safeAreaTransform ?? (safeAreaTransform = GetComponentInChildren<SafeArea>().transform as RectTransform); }
        private RectTransform safeAreaTransform = null;

        protected override void Initialize()
        {
            base.Initialize();
            Canvas.planeDistance = WindowSystem.PLANE_DISTANCE;
        }

    }
}

