﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace GybeGames.Systems.Windows
{
    [RequireComponent(typeof(CanvasGroup), typeof(RectTransform))]
    public abstract class BaseWindow : MonoBehaviour, IWindow
    {
        [SerializeField] private RenderMode cameraRenderMode = RenderMode.ScreenSpaceOverlay;
        [SerializeField] private GameObject blockerGameObject = null;
        public RenderMode CameraRenderMode { get => cameraRenderMode; }
        public bool IsInitialized { get; protected set; }
        public bool IsVisible { get; protected set; }
        public bool IsAppearing { get; protected set; }
        public bool IsDisappearing { get; protected set; }
        public CanvasGroup CanvasGroup { get => canvasGroup ?? (canvasGroup = GetComponent<CanvasGroup>()); }
        public Canvas Canvas { get => canvas ?? (canvas = canvas = GetComponent<Canvas>()); }
        public RectTransform RectTransform { get => rectTransform ?? (rectTransform = transform as RectTransform); }
        public List<IWindow> Windows { get => windows ?? (windows = GetComponentsInChildren<IWindow>(true).Where(window => window.RectTransform != RectTransform).ToList()); }

        private CanvasGroup canvasGroup = null;
        private Canvas canvas = null;
        private RectTransform rectTransform = null;
        private List<IWindow> windows = null;

        private void InternalInitialize()
        {
            if (IsInitialized) return;

            Initialize();
            IsInitialized = true;
        }

        protected virtual void Initialize() { }


        protected virtual void Awake()
        {
            InternalInitialize();
        }

        protected virtual void Start()
        {
            Gybe.GetSystem<WindowSystem>().Add(this);
        }

        protected virtual void OnDestroy()
        {
            if (Gybe.HasInstance && Gybe.GetSystem<WindowSystem>())
            {
                Gybe.GetSystem<WindowSystem>().Remove(this);
            }
        }

        public void SetActive(bool isActive = true)
        {
            gameObject.SetActive(isActive);
        }

        public void SetInteractible(bool isInteractable = true)
        {
            CanvasGroup.interactable = isInteractable;
        }

        public void SetBlocksRaycasts(bool blocksRaycasts = true)
        {
            CanvasGroup.blocksRaycasts = blocksRaycasts;
        }

        public void SetActiveBlocker(bool activate)
        {
            if (blockerGameObject == null)
            {
                return;
            }
            blockerGameObject.SetActive(activate);
        }

        #region Appear

        public void Appear(float? delay = null, float? duration = null)
        {
            if (IsAppearing)
            {
                return;
            }
            gameObject.SetActive(true);
            StartCoroutine(AppearCoroutine(delay, duration));
        }

        private IEnumerator AppearCoroutine(float? delay, float? duration)
        {
            IsAppearing = true;
            OnPreAppear();
            SetBlocksRaycasts(true);
            Windows.ForEach(window => window.Appear(delay, duration));
            yield return null;
            yield return StartCoroutine(AppearProgress());
            yield return new WaitWhile(() => Windows.Any(panel => panel.IsAppearing));
            SetBlocksRaycasts(true);
            IsAppearing = false;
            IsVisible = true;
            OnAppearingComplete();
        }

        protected virtual void OnPreAppear() { }

        protected virtual IEnumerator AppearProgress()
        {
            yield return null;
        }

        protected virtual void OnAppearingComplete()
        {
            SetActiveBlocker(false);
        }

        #endregion

        #region Disappear

        public void Disappear(float? delay = null, float? duration = null, bool disActivate = false)
        {
            if (IsDisappearing)
            {
                return;
            }
            StartCoroutine(DisAppearCoroutine(delay, duration, disActivate));
        }

        private IEnumerator DisAppearCoroutine(float? delay, float? duration, bool disactivate)
        {
            IsDisappearing = true;
            OnPreDisappear();
            Windows.ForEach(window => window.Disappear(delay, duration));
            SetBlocksRaycasts(false);
            yield return null;
            yield return StartCoroutine(DisappearProgress());
            yield return new WaitWhile(() => Windows.Any(panel => panel.IsDisappearing));
            IsDisappearing = false;
            IsVisible = false;
            OnDisappearingComplete();
            SetBlocksRaycasts(false);
            if (disactivate)
            {
                gameObject.SetActive(false);
            }
        }

        protected virtual void OnPreDisappear() { }

        protected virtual IEnumerator DisappearProgress()
        {
            yield return null;
        }

        protected virtual void OnDisappearingComplete() { }

        #endregion
    }
}
