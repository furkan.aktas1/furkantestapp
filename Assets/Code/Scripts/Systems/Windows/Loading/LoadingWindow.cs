﻿using DG.Tweening;
using System.Collections;
using UnityEngine;

namespace GybeGames.Systems.Windows.Loading
{
    public class LoadingWindow : BaseWindow
    {
        protected override void Initialize()
        {
            base.Initialize();
            SetCanvasGroupAlpha(1); // Ensures that loading window starts with open.
        }

        protected override void OnPreAppear()
        {
            SetCanvasGroupAlpha(0);
        }

        protected override IEnumerator AppearProgress()
        {
            bool fading = false;
            CanvasGroup.DOFade(1, 0.3f).SetEase(Ease.Linear).OnComplete(() => fading = true); ;
            yield return new WaitUntil(() => fading);
        }

        protected override void OnPreDisappear()
        {
            SetCanvasGroupAlpha(1);
        }

        protected override IEnumerator DisappearProgress()
        {
            SetBlocksRaycasts(true);
            bool fading = false;
            CanvasGroup.DOFade(0, 0.3f).SetEase(Ease.Linear).OnComplete(() => fading = true);
            yield return new WaitUntil(() => fading);
        }

        private void SetCanvasGroupAlpha(float alpha)
        {
            CanvasGroup.alpha = alpha;
        }
    }
}

