﻿using System.Collections;

namespace GybeGames.Systems.Windows.Loading
{
    public interface ILoadingApperable
    {
        IEnumerator AppearLoadingWindow(LoadingWindow loadingWindow, LoadingType loadingType);
        IEnumerator DisappearLoadingWindow(LoadingWindow loadingWindow, LoadingType loadingType);
    }
}
