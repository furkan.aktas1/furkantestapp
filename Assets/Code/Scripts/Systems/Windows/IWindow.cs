﻿using System.Collections.Generic;
using UnityEngine;

namespace GybeGames.Systems.Windows
{
    public interface IWindow
    {
        RenderMode CameraRenderMode { get; }
        bool IsInitialized { get; }
        bool IsVisible { get; }
        bool IsAppearing { get; }
        bool IsDisappearing { get; }
        CanvasGroup CanvasGroup { get; }
        Canvas Canvas { get; }
        RectTransform RectTransform { get; }
        List<IWindow> Windows { get; }

        void SetActive(bool isActive = true);
        void SetInteractible(bool isInteractible = true);
        void Appear(float? delay = null, float? duration = null);
        void Disappear(float? delay = null, float? duration = null, bool disactivate = false);
    }
}
