﻿using GybeGames.Systems.Save;
using UnityEngine;

namespace GybeGames.Systems
{
    /// <summary>
    /// Base class of all the systems.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    [DisallowMultipleComponent]
    public class BaseSystem<T> : MonoBehaviour, ISystem, ISavable where T : BaseSystem<T>
    {
        private bool isInitialized = false;

        protected void Awake()
        {
            Init();
        }

        protected virtual void Start()
        {

        }

        public void Init()
        {
            if (isInitialized)
                return;
            isInitialized = true;
            Gybe.AddSystem<T>(this);
            SetDefaults();
            Gybe.GetSystem<SaveSystem>().SaveDefaults(this);
            Load();
            Initialize();
        }

        public virtual void Initialize() { }

        /// <summary>
        /// Called on destroy of the system.
        /// </summary>
        private void OnDestroy()
        {
            if (!Gybe.HasInstance)
                return;
            Gybe.RemoveService<T>(this);
            DeInitialize();
            Save();
        }

        /// <summary>
        /// Deinitilization of the system has to be done there. Do not forget when application is being quit, this function cannot be invoked.
        /// </summary>
        public virtual void DeInitialize() { }

        public void Load()
        {
            Gybe.GetSystem<SaveSystem>().Load(this);
        }

        /// <summary>
        /// Post loading logic.
        /// </summary>
        public virtual void PostLoad() { }

        /// <summary>
        /// Pre saving logic. You can clear or zero some values before saving them.
        /// </summary>
        public virtual void PreSave() { }

        public void Save()
        {
            Gybe.GetSystem<SaveSystem>().Save(this);
        }

        /// <summary>
        /// Set default values. If save data does not have the key, creates new key and set defaults to its value.
        /// </summary>
        public virtual void SetDefaults() { }

    }

}

