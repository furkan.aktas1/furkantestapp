﻿using GybeGames.Systems.EventSystem;
using GybeGames.Systems.Levels.Events;
using GybeGames.Systems.Tutorial;
using UnityEngine;
using UnityEngine.Events;

namespace GybeGames.Systems.Input
{
    [DisallowMultipleComponent]
    public class InputSystem : BaseSystem<InputSystem>
    {
        public event UnityAction<Vector2> OnPanBegan;
        public event UnityAction<Vector2> OnPanExecuting;
        public event UnityAction<Vector2> OnPanEnded;
        public event UnityAction<Vector2> OnTap;
        public event UnityAction<Vector2> OnDown;
        public event UnityAction<Vector2> OnUp;

        public bool IsEnabled { get => canGetInput; }

        private InputCanvas inputCanvas { get => _inputCanvas ?? (_inputCanvas = FindObjectOfType<InputCanvas>()); }

        private InputCanvas _inputCanvas = null;

        private bool isFirstInputGiven = false;
        private bool canGetInput = false;

        public override void Initialize()
        {
            base.Initialize();
            EventManager.RegisterHandler<LevelLoadedEvent>(OnLevelLoaded);
            EventManager.RegisterHandler<LevelUnloadedEvent>(OnLevelUnloaded);
            EventManager.RegisterHandler<TutorialAppearingCompletedEvent>(OnTutorialAppearingCompleted);
        }

        protected override void Start()
        {
            base.Start();
            inputCanvas.SetCanvasProperties(this);
        }

        public override void DeInitialize()
        {
            base.DeInitialize();
            EventManager.UnregisterHandler<LevelLoadedEvent>(OnLevelLoaded);
            EventManager.UnregisterHandler<LevelUnloadedEvent>(OnLevelUnloaded);
            EventManager.UnregisterHandler<TutorialAppearingCompletedEvent>(OnTutorialAppearingCompleted);
        }

        private void OnLevelLoaded(LevelLoadedEvent levelLoadedEvent)
        {
            isFirstInputGiven = false;
        }

        private void OnLevelUnloaded(LevelUnloadedEvent levelUnloadedEvent)
        {
            canGetInput = false;
        }

        private void OnTutorialAppearingCompleted(TutorialAppearingCompletedEvent tutorialAppearingCompletedEvent)
        {
            canGetInput = true;
        }

        public void PointerDown(Vector2 point)
        {
            if (!canGetInput)
            {
                return;
            }
            FirstInputCheck();
            OnDown?.Invoke(point);
        }

        public void PointerUp(Vector2 point)
        {
            if (!canGetInput)
            {
                return;
            }
            OnUp?.Invoke(point);
        }

        public void PointerClick(Vector2 point)
        {
            if (!canGetInput)
            {
                return;
            }
            OnTap?.Invoke(point);
        }
        
        public void DragStart(Vector2 delta)
        {
            if (!canGetInput)
            {
                return;
            }
            OnPanBegan?.Invoke(delta);
        }

        public void DragExecuting(Vector2 delta)
        {
            if (!canGetInput)
            {
                return;
            }
            OnPanExecuting?.Invoke(delta);
        }

        public void DragEnd(Vector2 delta)
        {
            if (!canGetInput)
            {
                return;
            }
            OnPanEnded?.Invoke(delta);
        }

        private void FirstInputCheck()
        {
            if (!isFirstInputGiven)
            {
                EventManager.Send(FirstInputEvent.Create());
                isFirstInputGiven = true;
            }
        }

    }
}