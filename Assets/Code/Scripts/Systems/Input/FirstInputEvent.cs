using GybeGames.Systems.EventSystem;

namespace GybeGames.Systems.Input
{
    public class FirstInputEvent : CustomEvent
    {
        public static FirstInputEvent Create()
        {
            return new FirstInputEvent();
        }
    }
}
