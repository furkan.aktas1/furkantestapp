using GybeGames.Systems.Cameras;
using UnityEngine;
using UnityEngine.EventSystems;
using GybeGames.Systems.Windows;

namespace GybeGames.Systems.Input
{
    public class InputCanvas : MonoBehaviour, IPointerClickHandler, IBeginDragHandler, IDragHandler, IEndDragHandler, IPointerDownHandler, IPointerUpHandler
    {
        private Canvas canvas { get => _canvas ?? (_canvas = GetComponent<Canvas>()); }

        private Canvas _canvas = null;

        private InputSystem inputSystem = null;

        public void SetCanvasProperties(InputSystem inputSystem)
        {
            this.inputSystem = inputSystem;
            canvas.renderMode = RenderMode.ScreenSpaceCamera;
            canvas.worldCamera = Gybe.GetSystem<CameraSystem>().UICamera;
            canvas.planeDistance = WindowSystem.PLANE_DISTANCE;
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            inputSystem.PointerDown(eventData.position);
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            inputSystem.PointerUp(eventData.position);
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            inputSystem.PointerClick(eventData.position);
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            inputSystem.DragStart(eventData.delta);
        }

        public void OnDrag(PointerEventData eventData)
        {
            inputSystem.DragExecuting(eventData.delta);
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            inputSystem.DragEnd(eventData.delta);
        }
    }
}

