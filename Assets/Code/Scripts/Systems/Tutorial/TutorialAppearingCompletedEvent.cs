using GybeGames.Systems.EventSystem;

namespace GybeGames.Systems.Tutorial
{
    public class TutorialAppearingCompletedEvent : CustomEvent
    {
        public static TutorialAppearingCompletedEvent Create()
        {
            TutorialAppearingCompletedEvent tutorialAppearingCompletedEvent = new TutorialAppearingCompletedEvent();
            return tutorialAppearingCompletedEvent;
        }
    }
}