﻿using DG.Tweening;
using GybeGames.Utility.Extentions;
using UnityEngine;

namespace GybeGames.Systems.Tutorial
{
    public class EightSlideHandTutorial : BaseHandTutorial
    {
        private Animator animator => _animator ?? (_animator = GetComponentInChildren<Animator>());
        private Animator _animator;

        protected override void SetDefaults()
        {
            base.SetDefaults();
            animator.WriteDefaultValues();
        }

        protected override void PlayTutorialAnimation(bool isFirst = true)
        {
            DOTween.Kill(handRectTransform);

            handImage.color = handImage.color.SetAlpha(1);

            animator.SetTrigger("Hand");
            animator.speed = 1f / Duration;
        }
    }
}
