﻿using DG.Tweening;
using UnityEngine;

namespace GybeGames.Systems.Tutorial
{
    public class SwipeHandTutorial : BaseHandTutorial
    {
        [SerializeField] private Vector2 rightTo = new Vector2(200, 0);
        [SerializeField] private Vector2 leftTo = new Vector2(-160, 0);

        protected override void SetDefaults()
        {
            base.SetDefaults();
            //handRectTransform.anchoredPosition = Vector2.zero + leftTo;
            handRectTransform.anchoredPosition = Vector3.zero;
        }

        protected override void OnPreAppear()
        {
            base.OnPreAppear();
        }

        protected override void PlayTutorialAnimation(bool isFirst = true)
        {
            DOTween.Kill(handRectTransform);

            var sequence = DOTween.Sequence().SetId(handRectTransform);

            if (isFirst)
            {
                //handRectTransform.anchoredPosition = Vector2.zero + leftTo;
                handRectTransform.anchoredPosition = Vector3.zero;
                sequence.Append(handImage.DOFade(1f, .15f));
                sequence.Join(handRectTransform.DOAnchorPos(rightTo, Duration / 2).SetEase(Ease));
            }
            else
            {
                sequence.Join(handRectTransform.DOAnchorPos(rightTo, Duration).SetEase(Ease));
            }
            sequence.Append(handRectTransform.DOAnchorPos(leftTo, Duration).SetEase(Ease));
            sequence.OnComplete(() => PlayTutorialAnimation(false));

            //sequence.Append(handImage.DOFade(1f, .15f));
            //sequence.Join(handRectTransform.DOAnchorPos(rightTo, Duration).SetEase(Ease).SetDelay(DelayBetweenLoops));
            //sequence.Append(handRectTransform.DOAnchorPos(leftTo, Duration).SetEase(Ease));
            //sequence.OnComplete(() => PlayTutorialAnimation(false));


            sequence.Play();
        }
    }
}
