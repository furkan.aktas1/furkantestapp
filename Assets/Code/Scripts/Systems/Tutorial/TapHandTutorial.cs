﻿using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;

namespace GybeGames.Systems.Tutorial
{
    public class TapHandTutorial : BaseHandTutorial
    {
        [Title("References")]
        [SerializeField] private RectTransform tapRectTransform = null;
        [SerializeField] private Image tapImage = null;

        protected override void SetDefaults()
        {
            base.SetDefaults();
            tapRectTransform.localScale = Vector3.zero;
        }

        protected override void PlayTutorialAnimation(bool isFirst = true)
        {
            DOTween.Kill(handRectTransform);
            tapRectTransform.localScale = Vector3.zero;
            var newPos = Vector3.zero;
            var sequence = DOTween.Sequence().SetId(handRectTransform);

            if (!isFirst)
                sequence.Append(RectTransform.DOAnchorPos(newPos, DelayBetweenLoops));

            sequence.Append(handImage.DOFade(1f, .15f));
            sequence.Append(handRectTransform.DOScale(Vector3.one * 0.8f, Duration).SetEase(Ease).OnComplete(() => tapImage.DOFade(1f, 0.15f)));
            sequence.Append(handRectTransform.DOScale(Vector3.one, Duration).SetEase(Ease));
            sequence.Join(tapRectTransform.DOScale(Vector3.one, Duration * 0.8f).SetEase(Ease).OnComplete(() => tapImage.DOFade(0f, 0.25f)));
            sequence.Append(handImage.DOFade(0f, 0.25f));



            //sequence.Append(handContainer.DOScale(Vector3.one * Scale * 0.8f, Duration).SetEase(Ease));
            //sequence.Join(shadowImage.rectTransform.DOAnchorPos(-Vector3.one * 5f, .25f));
            //sequence.Append(handContainer.DOScale(Vector3.one * Scale, Duration).SetEase(Ease));
            //sequence.Join(shadowImage.rectTransform.DOAnchorPos(Vector3.one * -10f, .25f));

            sequence.OnComplete(() => PlayTutorialAnimation(false));

            sequence.Play();
        }
    }

}
