﻿using DG.Tweening;
using GybeGames.Systems.EventSystem;
using GybeGames.Systems.GameStates;
using GybeGames.Systems.Input;
using GybeGames.Systems.Levels.Events;
using GybeGames.Systems.Windows;
using Lean.Pool;
using Sirenix.OdinInspector;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace GybeGames.Systems.Tutorial
{
    public abstract class BaseHandTutorial : BaseWindow, IHandTutorial, IPoolable, IPointerDownHandler
    {
        [Title("References")]
        [SerializeField] protected Image handImage;
        [SerializeField] protected RectTransform handRectTransform;
        [SerializeField] protected RectTransform textRectTransform;
        [SerializeField] protected TextMeshProUGUI text;

        [Title("General Values")]
        [SerializeField] protected float duration = 1;
        [SerializeField] protected float delayBetweenLoops = 0.5f;
        [SerializeField] protected Ease ease = Ease.InOutSine;

        public float Duration { get => duration; }
        public float DelayBetweenLoops { get => delayBetweenLoops; }
        public Ease Ease { get => ease; }

        protected override void Initialize()
        {
            base.Initialize();
            SetDefaults();
            EventManager.RegisterHandler<LevelLoadedEvent>(OnLevelLoaded);
            EventManager.RegisterHandler<LevelUnloadedEvent>(OnLevelUnloaded);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();

            EventManager.UnregisterHandler<LevelLoadedEvent>(OnLevelLoaded);
            EventManager.UnregisterHandler<LevelUnloadedEvent>(OnLevelUnloaded);
        }

        private void OnLevelUnloaded(LevelUnloadedEvent obj)
        {
            EventManager.UnregisterHandler<FirstInputEvent>(OnFirstInput);
        }

        private void OnLevelLoaded(LevelLoadedEvent obj)
        {
            EventManager.RegisterHandler<FirstInputEvent>(OnFirstInput);
        }

        protected virtual void SetDefaults()
        {
            CanvasGroup.alpha = 0;
        }
        protected override void OnPreAppear()
        {
            base.OnPreAppear();
            EventManager.RegisterHandler<FirstInputEvent>(OnFirstInput);
        }

        private void OnFirstInput(FirstInputEvent obj)
        {
            if (IsVisible)
            {
                StopAllCoroutines();
                DOTween.Kill(handRectTransform);
                Disappear();
                Gybe.GetSystem<GameStateSystem>().StartGame();
                EventManager.UnregisterHandler<FirstInputEvent>(OnFirstInput);
            }
        }

        protected override void OnAppearingComplete()
        {
            EventManager.Send(TutorialAppearingCompletedEvent.Create());
            CanvasGroup.DOFade(1f, 0.15f).SetId(handRectTransform).OnComplete(() => PlayAnimation(isFirst: true, delay: false));
        }

        protected override void OnDisappearingComplete()
        {
            DOTween.Kill(handRectTransform);
            CanvasGroup.DOFade(0f, 0.15f).SetId(handRectTransform).OnComplete(() =>
            {
                SetDefaults();
                LeanPool.Despawn(this);
            });
        }

        public void OnSpawn()
        {
            DOTween.Kill(handRectTransform);
            SetDefaults();
        }

        public void OnDespawn()
        {
            DOTween.Kill(handRectTransform);
            SetDefaults();
        }

        public virtual void PlayAnimation(bool isFirst = true, bool delay = true)
        {
            float delayTime = (delay ? DelayBetweenLoops : 0);
            StopAllCoroutines();
            StartCoroutine(PlayCoroutine(isFirst, delayTime));
        }

        private IEnumerator PlayCoroutine(bool isFirst = true, float delayTime = 0)
        {
            yield return new WaitForSeconds(delayTime);
            PlayTutorialAnimation(isFirst);
        }

        protected abstract void PlayTutorialAnimation(bool isFirst = true);

        public void OnPointerDown(PointerEventData eventData)
        {
            if (IsVisible)
            {
                //StopAllCoroutines();
                //DOTween.Kill(handRectTransform);
                //Disappear();
                //Gybe.GetSystem<GameStateSystem>().StartGame();
            }
        }
    }

}