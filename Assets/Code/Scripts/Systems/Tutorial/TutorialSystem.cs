﻿using GybeGames.Systems.Windows;
using GybeGames.Systems.Windows.MainUIWindows;
using GybeGames.Utility.Extentions;
using Lean.Pool;
using Sirenix.OdinInspector;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace GybeGames.Systems.Tutorial
{
    public class TutorialSystem : BaseSystem<TutorialSystem>
    {

        [Title("References")]
        [SerializeField] List<BaseHandTutorial> handTutorialPrefabs = new List<BaseHandTutorial>();

        private RectTransform safeAreaRectTransform { get => Gybe.GetSystem<WindowSystem>().ForceGet<StaticMainCanvas>().SafeAreaRectTransform; }
        public List<BaseHandTutorial> HandTutorialPrefabs { get => handTutorialPrefabs; }

        private List<IHandTutorial> handTutorials { get => Gybe.GetSystem<WindowSystem>().Windows.OfType<IHandTutorial>().ToList(); }

        public override void Initialize()
        {
            base.Initialize();
        }

        public T ForceGet<T>(IHandTutorial prefab = null) where T : IHandTutorial
        {
            var handTutorial = handTutorials.OfType<T>().LastOrDefault();
            if (handTutorial != null && handTutorial.IsVisible)
            {
                return handTutorial;
            }
            prefab = prefab ?? handTutorialPrefabs.OfType<T>().FirstOrDefault();

            if (prefab == null)
            {
                Debug.LogError("Hand tutorial prefab can not be found on handTutorialPrefabs List. Please add prefab to list on TutorialService!");
                return handTutorial;
            }

            handTutorial = (T)LeanPool.Spawn((BaseHandTutorial)prefab, safeAreaRectTransform, false).GetComponent<IHandTutorial>();
            handTutorial?.RectTransform.SetAsLastSibling();
            if (!handTutorials.Contains(handTutorial))
            {
                handTutorials.Add(handTutorial);
            }
            return handTutorial;
        }

        public void Hide<T>(float? delay = null, float? duration = null) where T : IHandTutorial
        {
            ForceGet<T>().Disappear(delay, duration);
        }


#if UNITY_EDITOR
        [FoldoutGroup("Editor Buttons")]
        [Button]
        public void Editor_StartEightHandTutorial()
        {
            ForceGet<EightSlideHandTutorial>().Show();
        }
        [FoldoutGroup("Editor Buttons")]
        [Button]
        public void Editor_StartSlideHandTutorial()
        {
            ForceGet<SwipeHandTutorial>().Show();
        }
        [FoldoutGroup("Editor Buttons")]
        [Button]
        public void Editor_StartTapHandTutorial()
        {
            ForceGet<TapHandTutorial>().Show();
        }
#endif
    }
}
