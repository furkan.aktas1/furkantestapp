﻿using DG.Tweening;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace GybeGames.Systems.App
{
    [DefaultExecutionOrder(-999)]
    [DisallowMultipleComponent]
    public class AppSystem : BaseSystem<AppSystem>
    {
        [Title("References")]
        [SerializeField, Required] private AppSettings appSettings = null;
        public AppQuality QualityLevel { get => appSettings.QualityLevel; }
        public DataFormat SaveDataFormat { get => appSettings.SaveFormat; }

        public override void Initialize()
        {
            base.Initialize();
            SetFrameRate();
            SetQuality();
            SetLogType();
            DOTween.Init();
            DOTween.SetTweensCapacity(250, 100);
            Screen.sleepTimeout = SleepTimeout.NeverSleep;
            UnityEngine.Input.simulateMouseWithTouches = false;
#if DEV_MODE
            SRDebug.Init();
#endif
        }

        private void SetLogType()
        {
            Debug.unityLogger.filterLogType = appSettings.UnityLogType;
        }

        public void SetFrameRate(int? targetRate = null)
        {
            if(targetRate != null)
            {
                Application.targetFrameRate = (int)targetRate;
                return;
            }
            if (SystemInfo.processorCount == 1 && Screen.width <= 600)
            {
                Application.targetFrameRate = 30;
            }
            else
            {
                Application.targetFrameRate = 60;
            }
        }

        public void SetQuality()
        {
            int qualityIndex = QualitySettings.names.ToList()
                .IndexOf(QualitySettings.names.ToList()
                .SingleOrDefault(_qualityLevel => _qualityLevel == QualityLevel.ToString()));

            QualitySettings.SetQualityLevel(qualityIndex);
        }

        public IEnumerator CleanMemory()
        {
            GC.Collect();
            yield return Resources.UnloadUnusedAssets();
            GC.Collect();
        }

        public void UnloadUnusedAssets()
        {
            Resources.UnloadUnusedAssets();
        }
    }
}

