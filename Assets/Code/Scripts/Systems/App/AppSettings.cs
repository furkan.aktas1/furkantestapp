﻿using GybeGames.Systems.App;
using Sirenix.OdinInspector;
using UnityEngine;
using GybeGames.Utilities;
using Sirenix.Serialization;

namespace GybeGames.Systems.App
{
    [CreateAssetMenu(fileName = "AppSettings", menuName = "ScriptableObject/AppSettings")]
    public class AppSettings : ScriptableObject
    {
        [Title("App")]
        [SerializeField] private AppQuality qualityLevel = AppQuality.High;
        [Title("Save")]
        [SerializeField] private DataFormat saveFormat = DataFormat.JSON;

        public AppQuality QualityLevel { get => qualityLevel; }
        public DataFormat SaveFormat { get => saveFormat; }

        #region Development

        [Title("Development")]
        [SerializeField] private LogType unityLogType = LogType.Exception;

        public LogType UnityLogType
        {
            get
            {
                return unityLogType;
            }
            set
            {
                unityLogType = value;
                Debug.unityLogger.filterLogType = unityLogType;
            }
        }

        [OnValueChanged("unityLogType")]
        private void SetLogType()
        {
            Debug.unityLogger.filterLogType = unityLogType;
        }

#pragma warning disable CS0414 // Remove never used warning
        private string devModeButtonText = "Enable Dev Mode";
#pragma warning restore CS0414 // Remove Never used warning

        private void OnValidate()
        {
            Editor_SetDevModeButtonText();
        }

#if UNITY_EDITOR
        [Sirenix.OdinInspector.Button(Name = "$devModeButtonText")]
        private void Editor_ToggleDevMode()
        {
            devModeButtonText = "Updating! Please wait...";
#if DEV_MODE
            DefineManager.RemoveDefine("DEV_MODE");
#else
            DefineManager.AddDefine("DEV_MODE");
#endif
        }
#endif

        private void Editor_SetDevModeButtonText()
        {
#if DEV_MODE
            devModeButtonText = "Disable DEV MODE";
#else
            devModeButtonText = "Enable DEV MODE";
#endif
        }

        #endregion
    }
}
