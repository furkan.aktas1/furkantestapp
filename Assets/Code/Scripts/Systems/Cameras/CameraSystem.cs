﻿using DG.Tweening;
using UnityEngine;
using Sirenix.OdinInspector;

namespace GybeGames.Systems.Cameras
{
    [DisallowMultipleComponent]
    public class CameraSystem : BaseSystem<CameraSystem>
    {
        public Camera UICamera { get => uiCamera != null ? uiCamera : uiCamera = GameObject.FindGameObjectWithTag("UICamera").GetComponent<Camera>(); }
        public Camera MainCamera { get => mainCamera != null ? mainCamera : mainCamera = GameObject.FindGameObjectWithTag("MAINCamera").GetComponent<Camera>(); }

        private Camera uiCamera;
        private Camera mainCamera;


        public void Shake(Camera camera = null, float duration = 1f, float strength = 0.25f, int vibrato = 25, int randomness = 90, bool fadeOut = true)
        {
            camera = camera ?? MainCamera;
            if (!camera) return;

            camera.DOKill(true);
            Vector3 startPos = camera.transform.position;
            Quaternion startRot = camera.transform.rotation;

            camera.DOShakePosition(duration, strength, vibrato, randomness, fadeOut).OnComplete(() =>
            {
                camera.transform.SetPositionAndRotation(startPos, startRot);
            });
        }

        /// <summary>
        /// Get world position from Main Camera and projects it to the UI camera world position.
        /// </summary>
        /// <param name="position"></param>
        /// <returns></returns>
        public Vector3 FromMainWorldPointToUIWorldPoint(Vector3 position)
        {
            return UICamera.ScreenToWorldPoint(MainCamera.WorldToScreenPoint(position));
        }

#if UNITY_EDITOR
        [Title("Examples")]
        [Button()]
        public void Editor_ShakeCamera()
        {
            Shake();
        }
#endif
    }
}