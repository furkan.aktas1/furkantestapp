﻿using GybeGames.Systems.EventSystem;
using GybeGames.Systems.GameStates.Events;
using GybeGames.Systems.Levels.Data;
using GybeGames.Systems.Levels.Events;
using GybeGames.Systems.Save;
using Sirenix.OdinInspector;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace GybeGames.Systems.Levels
{
    [DefaultExecutionOrder(-10)]
    [DisallowMultipleComponent]
    public class LevelSystem : BaseSystem<LevelSystem>, ILevelSystem
    {
        private const int LOOP_LEVEL_INDEX = 1;

        [Title("References")]
        [SerializeField, Required] private AllLevels allLeves = null;

        public AllLevels AllLevels { get => allLeves; }
        public int CurrentLevelNumber { get => currentLevelNumber; set => currentLevelNumber = value; }
        public int HighestLevelNumber { get => highestLevelNumber; }
        public ILevel CurrentLevel { get => currentLevel; }
        public ILevelController CurrentLevelController { get => currentLevelController; }
        public bool IsLevelReady { get; private set; }


        [SavedAs("Gybe.Level.CurrentLevelNumber")] private int currentLevelNumber;
        [SavedAs("Gybe.Level.HighestLevelNumber")] private int highestLevelNumber;
        private List<ILevelController> levelControllers { get => _levelControllers ?? (_levelControllers = GetComponentsInChildren<ILevelController>().ToList()); }
        private List<ILevelController> _levelControllers = null;
        private ILevelController currentLevelController = null;
        private ILevelController previousLevelController = null;
        private ILevel currentLevel = null;

        public override void Initialize()
        {
            base.Initialize();
            EventManager.RegisterHandler<GameWonEvent>(OnGameWon);
        }

        public override void DeInitialize()
        {
            StopAllCoroutines();
            base.DeInitialize();
        }

        public override void SetDefaults()
        {
            base.SetDefaults();
            currentLevelNumber = 1;
            highestLevelNumber = 1;
        }

        private void OnGameWon(GameWonEvent gameWonEvent)
        {
            IncreaseLevel();
        }

        public void IncreaseLevel()
        {
            if (currentLevelNumber < allLeves.Levels.Count - 1)
                currentLevelNumber++;
            else
            {
                currentLevelNumber = LOOP_LEVEL_INDEX;
            }
            highestLevelNumber++;
        }

        public void SetLevelIndex(int levelIndex)
        {
            CurrentLevelNumber = levelIndex;
        }

        public void SetCurrentLevelNumberViaID(string levelId)
        {
            CurrentLevelNumber = allLeves.Levels.FindIndex(p => p.ID == levelId);
        }

        public void ConfigureAndLoadLevel()
        {
            SetLevel();
            SetLevelContoller();
            LoadLevel();
        }

        protected virtual void SetLevel()
        {
            if (allLeves == null || CurrentLevelNumber >= allLeves.Levels.Count || CurrentLevelNumber < 0)
            {
                Debug.LogError("CurrentLevel number is out of index of AllLevels.");
                currentLevelNumber = LOOP_LEVEL_INDEX;
            }
            currentLevel = allLeves?.Levels.ElementAt(CurrentLevelNumber);
        }

        private void SetLevelContoller()
        {
            previousLevelController = currentLevelController;
            currentLevelController = levelControllers?.Find(x => x.GetType() == currentLevel.LevelControllerType);
        }

        private void LoadLevel()
        {
            CurrentLevelController?.LoadLevel(CurrentLevel, previousLevelController);
        }

        public void LevelLoaded<T>(T level, object levelRef = null) where T : ILevel
        {
            EventManager.Send(LevelLoadedEvent.Create(level, levelRef));
        }

        public void LevelUnLoaded()
        {
            EventManager.Send(LevelUnloadedEvent.Create());
        }

#if UNITY_EDITOR

        [Title("Examples")]
        [SerializeField] private int levelIndexEDITOR = 0;
        [Button()]
        public void Editor_SetLevelIndexAndLoadScene()
        {
            SetLevelIndex(levelIndexEDITOR);
            ConfigureAndLoadLevel();
        }
#endif
    }
}
