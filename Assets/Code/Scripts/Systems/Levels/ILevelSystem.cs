﻿using GybeGames.Systems.Levels.Data;

namespace GybeGames.Systems.Levels
{
    public interface ILevelSystem
    {
        AllLevels AllLevels { get; }
        int CurrentLevelNumber { get; set; }
        int HighestLevelNumber { get; }
        ILevel CurrentLevel { get; }
        ILevelController CurrentLevelController { get; }
        bool IsLevelReady { get; }

        void SetLevelIndex(int levelIndex);
        void SetCurrentLevelNumberViaID(string levelId);
        void ConfigureAndLoadLevel();
        void LevelLoaded<T>(T level, object levelRef = null) where T : ILevel;
        void LevelUnLoaded();
    }
}
