﻿using GybeGames.Systems.EventSystem;

namespace GybeGames.Systems.Levels.Events
{
    public class LevelUnloadedEvent : CustomEvent
    {
        public static LevelUnloadedEvent Create()
        {
            LevelUnloadedEvent levelEvent = new LevelUnloadedEvent();
            return levelEvent;
        }
    }
}