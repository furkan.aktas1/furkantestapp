﻿using GybeGames.Systems.EventSystem;

namespace GybeGames.Systems.Levels.Events
{
    public class LevelLoadedEvent : CustomEvent
    {
        public ILevel LevelData { get; private set; }
        public object Reference { get; private set; }

        public static LevelLoadedEvent Create(ILevel levelData, object reference)
        {
            LevelLoadedEvent levelEvent = new LevelLoadedEvent();
            levelEvent.LevelData = levelData;
            levelEvent.Reference = reference;
            return levelEvent;
        }
    }
}
