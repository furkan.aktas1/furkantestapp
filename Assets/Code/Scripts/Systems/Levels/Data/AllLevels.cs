﻿using Sirenix.OdinInspector;
using System.Collections.Generic;
using UnityEngine;

namespace GybeGames.Systems.Levels.Data
{
    [CreateAssetMenu(fileName = "AllLevels", menuName = "ScriptableObject/Levels/AllLevels")]
    public class AllLevels : ScriptableObject
    {
        [InfoBox("Index 0 has to be null. First level needs to start from first index.", InfoMessageType.Warning)]
        public List<BaseLevel> Levels = new List<BaseLevel>();
    }
}
