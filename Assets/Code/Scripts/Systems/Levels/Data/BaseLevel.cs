﻿using GybeGames.Systems.Levels.LevelController;
using GybeGames.Utility.TypeReferences;
using System;
using UnityEngine;

namespace GybeGames.Systems.Levels.Data
{
    public abstract class BaseLevel : ScriptableObject, ILevel
    {
        [ClassImplements(typeof(ILevelController))]
        public ClassTypeReference levelControllerType = typeof(DefaultLevelController);
        [SerializeField] protected string id;

        public Type LevelControllerType { get => levelControllerType; }
        public string ID { get => id; set => id = value; }
    }
}
