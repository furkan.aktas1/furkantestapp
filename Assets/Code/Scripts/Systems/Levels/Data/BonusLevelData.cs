﻿using UnityEngine;

namespace GybeGames.Systems.Levels.Data
{
    /// <summary>
    /// Different Level Datas will be created according to level types like, mini game level etc.
    /// </summary>
    [CreateAssetMenu(fileName = "BonusLevelData", menuName = "ScriptableObject/Levels/BonusLevelData")]
    public class BonusLevelData : BaseLevel
    {

    }
}
