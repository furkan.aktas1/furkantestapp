﻿using UnityEngine;

namespace GybeGames.Systems.Levels.Data
{
    /// <summary>
    /// Different Level Datas will be created according to level types like, mini game level etc.
    /// </summary>
    [CreateAssetMenu(fileName = "LevelData", menuName = "ScriptableObject/Levels/LevelData")]
    public class LevelData : BaseLevel
    {

    }
}
