﻿using System;

namespace GybeGames.Systems.Levels
{
    public interface ILevel
    {
        Type LevelControllerType { get; }
        string ID { get; set; }
    }
}
