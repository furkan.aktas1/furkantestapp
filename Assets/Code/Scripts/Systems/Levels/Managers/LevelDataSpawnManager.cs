﻿using GybeGames.Systems.Levels.Data;
using UnityEngine;

namespace GybeGames.Systems.Levels.Managers
{
    public abstract class LevelDataSpawnController : MonoBehaviour, ISpawnManager<LevelData>
    {
        public abstract void Spawn(LevelData data);
        public abstract void Desapwn();
    }
}