﻿namespace GybeGames.Systems.Levels.Managers
{
    public interface ISpawnManager<in T>
    {
        void Spawn(T data);
        void Desapwn();
    }
}