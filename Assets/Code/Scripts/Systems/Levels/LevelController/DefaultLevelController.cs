﻿using GybeGames.Systems.InGamePoolSystem;
using GybeGames.Systems.Levels.Data;
using GybeGames.Systems.Levels.Managers;
using GybeGames.Systems.Scenes;
using System.Collections;

namespace GybeGames.Systems.Levels.LevelController
{
    public class DefaultLevelController : BaseLevelController<LevelData>
    {
        private ISpawnManager<LevelData>[] spawnControllers { get => _spawnControllers ?? (_spawnControllers = FindObjectsOfType<LevelDataSpawnController>()); }

        private ISpawnManager<LevelData>[] _spawnControllers = null;


        protected override SceneReference sceneReference { get => sceneSystem.GameScenes[0]; }

        protected override void Initialize()
        {
            base.Initialize();
        }

        /// <summary>
        /// The things has to be instantiated, or initialize for level
        /// </summary>
        public override IEnumerator LoadLevelAssetsAndConfiguration()
        {
            for (int i = 0; i < spawnControllers.Length; i++)
            {
                spawnControllers[i].Spawn(currentLevel);
            }
            yield return null;
        }

        /// <summary>
        /// The things has to be pooled if they havent yet
        /// </summary>
        public override IEnumerator UnLoadLevelAssetsAndConfiguration()
        {
            for (int i = 0; i < spawnControllers.Length; i++)
            {
                spawnControllers[i].Desapwn();
            }

            Gybe.GetSystem<GamePlayPoolSystem>().DespawnAll();

            yield return null;
        }
    }
}