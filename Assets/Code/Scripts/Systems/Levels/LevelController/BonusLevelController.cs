﻿using GybeGames.Systems.Levels.Data;
using GybeGames.Systems.Scenes;
using System.Collections;

namespace GybeGames.Systems.Levels.LevelController
{
    public class BonusLevelController : BaseLevelController<BonusLevelData>
    {
        protected override SceneReference sceneReference { get => sceneSystem.GameScenes[0]; }

        protected override void Initialize()
        {
            base.Initialize();
        }

        /// <summary>
        /// The things has to be instantiated, or initialize for level
        /// </summary>
        public override IEnumerator LoadLevelAssetsAndConfiguration()
        {
            //Make everyyting here
            yield return null;
        }

        /// <summary>
        /// The things has to be pooled if they havent yet
        /// </summary>
        public override IEnumerator UnLoadLevelAssetsAndConfiguration()
        {
            // Make everyting here
            yield return null;
        }
    }
}

