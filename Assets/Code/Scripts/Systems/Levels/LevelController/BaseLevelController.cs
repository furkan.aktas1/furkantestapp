﻿using GybeGames.Systems.App;
using System.Collections;
using UnityEngine;
using GybeGames.Systems.Scenes;
using GybeGames.Systems.Windows.Loading;
using GybeGames.Systems.Windows;

namespace GybeGames.Systems.Levels.LevelController
{
    public class BaseLevelController<T> : MonoBehaviour, ILoadingApperable, ILevelController where T : ILevel
    {
        private const bool USE_FIRSTOPEN = true;
        public SceneReference SceneReference { get => sceneReference; }

        protected static bool sessionFirstOpen = true;


        protected ISceneSystem sceneSystem { get => _sceneSystem ?? (_sceneSystem = Gybe.GetSystem<SceneSystem>()); }
        private ILevelSystem levelSystem { get => _levelSystem ?? (_levelSystem = Gybe.GetSystem<LevelSystem>()); }

        protected T currentLevel;
        protected virtual SceneReference sceneReference { get; }

        private ISceneSystem _sceneSystem = null;
        private ILevelSystem _levelSystem = null;

        private void Awake()
        {
            Initialize();
        }

        protected virtual void Initialize() { }

        public void LoadLevel(ILevel level, ILevelController previousLevelController)
        {
            currentLevel = (T)level;

            if (sessionFirstOpen && USE_FIRSTOPEN)
            {
                StartCoroutine(LoadLevelCoroutine(LoadingType.None, previousLevelController));
                sessionFirstOpen = false;
                return;
            }

            StartCoroutine(LoadLevelCoroutine(LoadingType.InOnly, previousLevelController));
        }

        private IEnumerator LoadLevelCoroutine(LoadingType loadingType, ILevelController previousLevelController)
        {
            var loadingWindow = Gybe.GetSystem<WindowSystem>().ForceGet<LoadingWindow>();

            yield return StartCoroutine(AppearLoadingWindow(loadingWindow, loadingType));

            if (previousLevelController != null)
            {
                yield return StartCoroutine(previousLevelController.UnLoadLevelAssetsAndConfiguration());
            }

            yield return Gybe.GetSystem<AppSystem>().CleanMemory();

            levelSystem.LevelUnLoaded();

            //if (sceneSystem.IsSceneOpen(sceneReference))
            //{
            //    OnSceneLevelLoaded();
            //    yield break;
            //}
            sceneSystem.LoadScene(sceneName: SceneReference.ScenePath, sceneOrderIndex: 0, loadingType: LoadingType.None, OnSceneLoaded: OnSceneLevelLoaded);
        }

        public virtual IEnumerator LoadLevelAssetsAndConfiguration() { yield return null; }

        public virtual IEnumerator UnLoadLevelAssetsAndConfiguration() { yield return null; }

        private void OnSceneLevelLoaded()
        {
            StartCoroutine(OnSceneLoadedCoroutine());
        }

        private IEnumerator OnSceneLoadedCoroutine()
        {
            yield return StartCoroutine(LoadLevelAssetsAndConfiguration());
            levelSystem.LevelLoaded(currentLevel);
            var loadingWindow = Gybe.GetSystem<WindowSystem>().ForceGet<LoadingWindow>();
            StartCoroutine(DisappearLoadingWindow(loadingWindow, LoadingType.OutOnly));
        }

        #region ILoadingAppearable

        public IEnumerator AppearLoadingWindow(LoadingWindow loadingWindow, LoadingType loadingType)
        {
            if (loadingWindow != null && (loadingType == LoadingType.InOnly || loadingType == LoadingType.InAndOut))
            {
                loadingWindow.Appear();
                yield return new WaitUntil(() => loadingWindow.IsVisible);
            }
        }

        public IEnumerator DisappearLoadingWindow(LoadingWindow loadingWindow, LoadingType loadingType)
        {
            if (loadingWindow != null && (loadingType == LoadingType.OutOnly || loadingType == LoadingType.InAndOut))
            {
                loadingWindow.Disappear();
                yield return new WaitUntil(() => !loadingWindow.IsVisible);
            }
        }

        #endregion
    }
}
