﻿using System.Collections;

namespace GybeGames.Systems.Levels
{
    public interface ILevelController
    {
        void LoadLevel(ILevel iLevel, ILevelController iPreviousLevelController);
        IEnumerator LoadLevelAssetsAndConfiguration();
        IEnumerator UnLoadLevelAssetsAndConfiguration();
    }
}
