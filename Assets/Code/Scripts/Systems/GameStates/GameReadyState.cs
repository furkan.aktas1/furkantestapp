﻿using UnityEngine;
using MEC;
using System.Collections.Generic;
using GybeGames.Systems.EventSystem;
using GybeGames.Systems.GameStates.Events;
using GybeGames.Systems.Tutorial;
using GybeGames.Utility.Extentions;

namespace GybeGames.Systems.GameStates
{
    [CreateAssetMenu(fileName = "GameReadyState", menuName = "ScriptableObject/GameState/GameReadyState", order = 0)]
    public class GameReadyState : BaseGameState
    {
        protected override void InvokeStateStartEvent()
        {
            EventManager.Send(GameReadyEvent.Create(this));
        }

        protected override void OnStateStart()
        {
            base.OnStateStart();
            Timing.RunCoroutine(DelayedStart());
        }

        private IEnumerator<float> DelayedStart()
        {
            yield return Timing.WaitForOneFrame;
            Gybe.GetSystem<TutorialSystem>().ForceGet<EightSlideHandTutorial>().Show();
        } 

        protected override void OnStateStop()
        {
            base.OnStateStop();
            Timing.KillCoroutines();
        }
    }
}
