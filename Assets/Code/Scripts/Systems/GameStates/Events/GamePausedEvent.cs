﻿using GybeGames.Systems.EventSystem;

namespace GybeGames.Systems.GameStates.Events
{
    public class GamePausedEvent : CustomEvent
    {
        public GamePausedState GameState { get; private set; }
        public static GamePausedEvent Create(GamePausedState gameState)
        {
            GamePausedEvent readyEvent = new GamePausedEvent();
            readyEvent.GameState = gameState;
            return readyEvent;
        }
    }
}