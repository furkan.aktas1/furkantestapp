﻿using GybeGames.Systems.EventSystem;

namespace GybeGames.Systems.GameStates.Events
{
    public class GameInitialEvent : CustomEvent
    {
        public GameInitialState GameInitialState { get; private set; }
        public static GameInitialEvent Create(GameInitialState gameInitialState)
        {
            GameInitialEvent gameInitialEvent = new GameInitialEvent();
            gameInitialEvent.GameInitialState = gameInitialState;
            return gameInitialEvent;
        }
    }
}

