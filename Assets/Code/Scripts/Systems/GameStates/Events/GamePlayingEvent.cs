﻿using GybeGames.Systems.EventSystem;

namespace GybeGames.Systems.GameStates.Events
{
    public class GamePlayingEvent : CustomEvent
    {
        public GamePlayingState GamePlayingState { get; private set; }
        public static GamePlayingEvent Create(GamePlayingState gamePlayingState)
        {
            GamePlayingEvent playingEvent = new GamePlayingEvent();
            playingEvent.GamePlayingState = gamePlayingState;
            return playingEvent;
        }
    }
}