﻿using GybeGames.Systems.EventSystem;

namespace GybeGames.Systems.GameStates.Events
{
    public class GameReadyEvent : CustomEvent
    {
        public GameReadyState GameReadyState { get; private set; }
        public static GameReadyEvent Create(GameReadyState gameReadyState)
        {
            GameReadyEvent readyEvent = new GameReadyEvent();
            readyEvent.GameReadyState = gameReadyState;
            return readyEvent;
        }
    }
}