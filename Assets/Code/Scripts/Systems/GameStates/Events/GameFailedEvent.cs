﻿using GybeGames.Systems.EventSystem;

namespace GybeGames.Systems.GameStates.Events
{
    public class GameFailedEvent : CustomEvent
    {
        public GameFailedState GameState { get; private set; }
        public static GameFailedEvent Create(GameFailedState gameState)
        {
            GameFailedEvent readyEvent = new GameFailedEvent();
            readyEvent.GameState = gameState;
            return readyEvent;
        }
    }
}