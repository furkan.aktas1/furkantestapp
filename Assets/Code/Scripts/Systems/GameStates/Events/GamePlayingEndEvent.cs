﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GybeGames.Systems.EventSystem;

namespace GybeGames.Systems.GameStates.Events
{
    public class GamePlayingEndEvent : CustomEvent
    {
        public static GamePlayingEndEvent Create()
        {
            GamePlayingEndEvent gamePlayingEndEvent = new GamePlayingEndEvent();
            return gamePlayingEndEvent;
        }
    }
}

