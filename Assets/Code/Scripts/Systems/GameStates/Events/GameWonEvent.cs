﻿using GybeGames.Systems.EventSystem;

namespace GybeGames.Systems.GameStates.Events
{
    public class GameWonEvent : CustomEvent
    {
        public GameWonState GameState { get; private set; }
        public static GameWonEvent Create(GameWonState gameState)
        {
            GameWonEvent readyEvent = new GameWonEvent();
            readyEvent.GameState = gameState;
            return readyEvent;
        }
    }
}