﻿using UnityEngine;
using GybeGames.Systems.EventSystem;
using GybeGames.Systems.GameStates.Events;
using GybeGames.Systems.Windows.MainUIWindows;
using GybeGames.Systems.Windows;
using GybeGames.Utility.Extentions;

namespace GybeGames.Systems.GameStates
{
    [CreateAssetMenu(fileName = "GameWonState", menuName = "ScriptableObject/GameState/GameWonState")]
    public class GameWonState : BaseGameState
    {
        protected override void InvokeStateStartEvent()
        {
            EventManager.Send(GameWonEvent.Create(this));
        }

        protected override void OnStateStart()
        {
            base.OnStateStart();
            Gybe.GetSystem<WindowSystem>().ForceGet<GameWonWindow>().Show();
        }

        protected override void OnStateStop()
        {
            base.OnStateStop();

            Gybe.GetSystem<WindowSystem>().Get<GameWonWindow>()?.Disappear(disActivate: true);
        }
    }
}
