﻿
using UnityEngine;


namespace GybeGames.Systems.GameStates
{
    /// <summary>
    /// For each state from initilaization to game end. Dont forget to clear your variables since they are not creating new scriptable objects only using 
    /// </summary>
    public abstract class BaseGameState : ScriptableObject
    {
        [SerializeField] protected float delayForNextState = 0;
        [System.NonSerialized] protected bool isStateActive;

        public float DelayForNextState { get => delayForNextState; }
        public bool IsStateActive { get => isStateActive; }

        public void StartState()
        {
            isStateActive = true;
            InvokeStateStartEvent();
            OnStateStart();
        }

        public void StopState()
        {
            isStateActive = false;
            OnStateStop();
        }
        protected abstract void InvokeStateStartEvent();
        protected virtual void OnStateStart() { }
        protected virtual void OnStateStop() { }
    }
}