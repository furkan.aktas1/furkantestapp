﻿using GybeGames.Systems.EventSystem;
using GybeGames.Systems.GameStates.Events;
using GybeGames.Systems.Windows;
using GybeGames.Systems.Windows.MainUIWindows;
using GybeGames.Utility.Extentions;
using UnityEngine;

namespace GybeGames.Systems.GameStates
{
    [CreateAssetMenu(fileName = "GameFailedState", menuName = "ScriptableObject/GameState/GameFailedState")]
    public class GameFailedState : BaseGameState
    {
        protected override void InvokeStateStartEvent()
        {
            EventManager.Send(GameFailedEvent.Create(this));
        }

        protected override void OnStateStart()
        {
            base.OnStateStart();
            Gybe.GetSystem<WindowSystem>().ForceGet<GameFailedWindow>().Show();
        }

        protected override void OnStateStop()
        {
            base.OnStateStop();
            Gybe.GetSystem<WindowSystem>().Get<GameFailedWindow>()?.Disappear(disActivate: true);
        }
    }
}
