﻿using GybeGames.Systems.EventSystem;
using GybeGames.Systems.GameStates.Events;
using GybeGames.Systems.Windows;
using GybeGames.Systems.Windows.MainUIWindows;
using GybeGames.Utility.Extentions;
using UnityEngine;

namespace GybeGames.Systems.GameStates
{
    [CreateAssetMenu(fileName = "GamePlayingState", menuName = "ScriptableObject/GameState/GamePlayingState", order = 1)]
    public class GamePlayingState : BaseGameState
    {
        protected override void InvokeStateStartEvent()
        {
            EventManager.Send(GamePlayingEvent.Create(this));
        }

        protected override void OnStateStart()
        {
            base.OnStateStart();
            Gybe.GetSystem<WindowSystem>().ForceGet<GamePlayWindow>().Show();
        }

        protected override void OnStateStop()
        {
            EventManager.Send(GamePlayingEndEvent.Create());
            base.OnStateStop();

            Gybe.GetSystem<WindowSystem>().Get<GamePlayWindow>()?.Disappear(disActivate: true);
        }
    }
}
