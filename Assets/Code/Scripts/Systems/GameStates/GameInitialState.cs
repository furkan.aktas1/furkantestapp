﻿
using GybeGames.Systems.EventSystem;
using GybeGames.Systems.GameStates.Events;
using GybeGames.Systems.Windows;
using GybeGames.Systems.Windows.MainUIWindows;
using GybeGames.Utility.Extentions;

using UnityEngine;

namespace GybeGames.Systems.GameStates
{
    [CreateAssetMenu(fileName = "GameInitialState", menuName = "ScriptableObject/GameState/GameInitialState")]
    public class GameInitialState : BaseGameState
    {
        protected override void InvokeStateStartEvent()
        {
            EventManager.Send(GameInitialEvent.Create(this));
        }

        protected override void OnStateStart()
        {
            base.OnStateStart();
            Gybe.GetSystem<WindowSystem>().ForceGet<MainMenuWindow>().Show();
        }

        protected override void OnStateStop()
        {
            base.OnStateStop();
            Gybe.GetSystem<WindowSystem>().Get<MainMenuWindow>()?.Disappear(disActivate: true);
        }
    }
}