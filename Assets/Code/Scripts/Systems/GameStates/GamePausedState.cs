﻿using GybeGames.Systems.EventSystem;
using GybeGames.Systems.GameStates.Events;
using UnityEngine;

namespace GybeGames.Systems.GameStates
{
    [CreateAssetMenu(fileName = "GamePausedState", menuName = "ScriptableObject/GameState/GamePausedState")]
    public class GamePausedState : BaseGameState
    {
        protected override void InvokeStateStartEvent()
        {
            EventManager.Send(GamePausedEvent.Create(this));
        }
    }
}
