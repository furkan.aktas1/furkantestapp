﻿using GybeGames.Systems.EventSystem;
using GybeGames.Systems.Levels.Events;
using GybeGames.Systems.Save;
using GybeGames.Utility.Extentions;
using MEC;
using Sirenix.OdinInspector;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace GybeGames.Systems.GameStates
{
    [DefaultExecutionOrder(-997)]
    [DisallowMultipleComponent]
    public class GameStateSystem : BaseSystem<GameStateSystem>
    {
        [Title("References")]
        [SerializeField] private List<BaseGameState> gameStates = new List<BaseGameState>();

        [Title("Values")]
        [SerializeField] private bool useHistoryForInitialState = false;

        public BaseGameState CurrentGameState { get => currentGameState; set => currentGameState = value; }
        public int GamesStarted { get => gamesStarted; private set => gamesStarted = value; }
        public int GamesWon { get => gamesWon; private set => gamesWon = value; }
        public int GamesFailed { get => gamesFailed; private set => gamesFailed = value; }
        public bool IsNewUser { get => isNewUser; set => isNewUser = value; }
        public bool IsGameInitialState { get => CurrentGameState is GameInitialState; }
        public bool IsGamePlaying { get => CurrentGameState is GamePlayingState; }
        public bool IsGamePaused { get => CurrentGameState is GamePausedState; }
        public bool IsGameWon { get => CurrentGameState is GameWonState; }
        public bool IsGameFailed { get => CurrentGameState is GameFailedState; }
        public Stack<Type> GameStateHistory { get => gameStateHistory; set => gameStateHistory = value; }

        private BaseGameState currentGameState
        {
            get
            {
                if (_currentGameState == null)
                {
                    _currentGameState = TryGetGameState(typeof(GameInitialState));
                    gameStateHistory.Push(typeof(GameInitialState));
                    Debug.Log($"Newly initialized current state added to history: {gameStateHistory.Peek().ToString().GetLastType()}");
                }
                return _currentGameState;
            }
            set
            {
                _currentGameState = value;
                gameStateHistory.Push(_currentGameState.GetType());
            }
        }

        private BaseGameState _currentGameState = null;

        [SavedAs("Gybe.Game.GamesStarted")] private int gamesStarted = 0;
        [SavedAs("Gybe.Game.GamesWon")] private int gamesWon = 0;
        [SavedAs("Gybe.Game.GamesFailed")] private int gamesFailed = 0;
        [SavedAs("Gybe.Game.IsNewUser")] private bool isNewUser = true;
        [SavedAs("Gybe.Game.History")] private Stack<Type> gameStateHistory = new Stack<Type>();

        public override void Initialize()
        {
            base.Initialize();
            InitializeState();
            RegisterToEvents();
        }

        public override void DeInitialize()
        {
            base.DeInitialize();
            UnregisterFromEvents();
        }

        private void RegisterToEvents()
        {
            EventManager.RegisterHandler<LevelLoadedEvent>(OnLevelLoaded);
        }

        private void UnregisterFromEvents()
        {
            EventManager.UnregisterHandler<LevelLoadedEvent>(OnLevelLoaded);
        }

        /// <summary>
        /// Initialize first state. If game history is not empty and useHistoryForInitialState is true, use previous state. Otherwise start with GameInitialState.
        /// </summary>
        private void InitializeState()
        {
            if (gameStateHistory.Count != 0 && useHistoryForInitialState)
            {
                currentGameState = TryGetGameState(gameStateHistory.Pop());
                Debug.Log("Loaded current state from history.");
            }
            Debug.Log($"Current state: {currentGameState}");
            currentGameState.StartState();
        }

        public override void PreSave()
        {
            base.PreSave();
            ClearHistoryExceptLastOne();
        }

        private void OnLevelLoaded(LevelLoadedEvent levelLoadedEvent)
        {
            MoveToState(typeof(GameReadyState), true);
        }

        private void ClearHistoryExceptLastOne()
        {
            Type type = gameStateHistory.Peek();
            gameStateHistory.Clear();
            gameStateHistory.Push(type);
        }

        public void StartGame()
        {
            GamesStarted++;
            MoveToState(typeof(GamePlayingState));
        }

        public void PauseGame()
        {
            MoveToState(typeof(GamePausedState));
        }

        public void ResumeGame()
        {
            MoveToState(typeof(GamePlayingState));
        }

        public void WinGame()
        {
            GamesWon++;
            // Increase current level at the level system
            MoveToState(typeof(GameWonState));
        }

        public void FailGame()
        {
            GamesFailed++;
            MoveToState(typeof(GameFailedState));
        }

        public void MoveToState(Type nextState, bool force = false)
        {
            if (!currentGameState.IsStateActive)
                return;
            if (currentGameState.DelayForNextState > 0)
                Timing.RunCoroutine(MoveToStateCoroutine(currentGameState.DelayForNextState, nextState, force));
            else
                MoveToStateInstantly(nextState, force);
        }

        private IEnumerator<float> MoveToStateCoroutine(float delay, Type nextState, bool force)
        {
            yield return Timing.WaitForSeconds(delay);
            MoveToStateInstantly(nextState, force);
        }

        private void MoveToStateInstantly(Type nextState, bool force)
        {
            if (!force)
            {
                if (currentGameState.GetType() == nextState)
                    return;
            }
            currentGameState.StopState();
            currentGameState = TryGetGameState(nextState);
            currentGameState.StartState();
        }

        private BaseGameState TryGetGameState(Type gameStateType)
        {
            BaseGameState gameState = GetGameState(gameStateType);
            if (!gameState)
                throw new Exception($"There is no state for {gameStateType} in the Game State List!");
            return gameState;
        }

        [Obsolete("Use GetGameState(Type) instead.")]
        private int GetGameStateIDX(Type gameStateType)
        {
            return gameStates.FindIndex(x => x.GetType() == gameStateType);
        }

        private BaseGameState GetGameState(Type gameStateType)
        {
            return gameStates.Find(x => x.GetType() == gameStateType);
        }

        private void ResetHistory()
        {
            gameStateHistory.Clear();
        }

        // DEBUG METHOD
        private void LogHistory()
        {
            var history = "";
            foreach (var entry in gameStateHistory)
                history += entry + " - ";
            Debug.Log($"Game State History: {history}");
        }

#if UNITY_EDITOR
        [FoldoutGroup("Editor Buttons")]
        [Button]
        public void Editor_ChangeGameStateToInitial()
        {
            MoveToState(typeof(GameInitialState));
        }
        [FoldoutGroup("Editor Buttons")]
        [Button]
        public void Editor_ChangeGameStateToReady()
        {
            MoveToState(typeof(GameReadyState));
        }
        [FoldoutGroup("Editor Buttons")]
        [Button]
        public void Editor_ChangeGameStateToPlaying()
        {
            MoveToState(typeof(GamePlayingState));
        }
        [FoldoutGroup("Editor Buttons")]
        [Button]
        public void Editor_ChangeGameStateToWon()
        {
            MoveToState(typeof(GameWonState));
        }
        [FoldoutGroup("Editor Buttons")]
        [Button]
        public void Editor_ChangeGameStateToFailed()
        {
            MoveToState(typeof(GameFailedState));
        }
        [FoldoutGroup("Editor Buttons")]
        [Button]
        public void Editor_ExceptionTest()
        {
            MoveToState(typeof(BaseGameState));
        }
        [FoldoutGroup("Editor Buttons")]
        [Button]
        public void Editor_LogHistoryTest()
        {
            LogHistory();
        }
        [FoldoutGroup("Editor Buttons")]
        [Button]
        public void Editor_ResetHistoryTest()
        {
            ResetHistory();
        }
#endif
    }
}