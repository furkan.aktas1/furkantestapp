﻿using UnityEngine;

namespace GybeGames.Utilities
{
    public class SingletonMonoBehaviour<T> : MonoBehaviour where T : SingletonMonoBehaviour<T>
    {
        public static bool IsInitialized { get => isInitialized; }
        public static bool HasInstance { get => instance != null && !isShuttingDown; }

        public static T Instance
        {
            get
            {
                if (isShuttingDown)
                {
                    Debug.LogWarning($"Singleton Instance '{typeof(T)}' already destroyed. Returning null.");
                    return null;
                }

                lock (lockObject)
                {
                    if (instance == null)
                    {
                        CreateInstance();
                    }
                    else if (!isInitialized)
                    {
                        isInitialized = true;
                        instance.Initialize();
                    }
                    return instance;
                }
            }

        }

        private static bool isInitialized = false;
        private static bool isShuttingDown = false;
        private static object lockObject = new object();
        private static T instance;

        protected void Awake()
        {
            if (instance == null)
            {
                instance = (T)this;
            }
            else if (instance != this)
            {
                Debug.LogError($"Another instance of '{typeof(T)}' already exists! Destroying self...");
                DestroyImmediate(this);
                return;
            }

            if (!isInitialized)
            {
                isInitialized = true;
                instance.Initialize();
            }
        }

        protected virtual void OnDestroy()
        {
            isShuttingDown = true;
            isInitialized = false;
            instance.DeInitialize();
        }
        protected virtual void OnApplicationQuit()
        {
            isShuttingDown = true;
            isInitialized = false;
        }

        protected virtual void Initialize() { }
        protected virtual void DeInitialize() { }

        private static void CreateInstance()
        {
            instance = FindObjectOfType<T>();
            if (instance == null)
            {
                GameObject newGo = new GameObject(typeof(T).Name);
                instance = newGo.AddComponent<T>();
                DontDestroyOnLoad(instance);
            }
            instance.Initialize();
        }
    }
}