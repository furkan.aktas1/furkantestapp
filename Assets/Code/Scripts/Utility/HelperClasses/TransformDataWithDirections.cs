﻿using UnityEngine;

namespace GybeGames.Utility.HelperClasses
{
    [System.Serializable]
    public struct TransformDataWithDirections
    {
        [SerializeField] private TransformData transformData;
        [SerializeField] private Vector3 up;
        [SerializeField] private Vector3 right;
        [SerializeField] private Vector3 forward;

        public TransformData TransformData { get => transformData; }
        public Vector3 Up { get => up; }
        public Vector3 Right { get => right; }
        public Vector3 Forward { get => forward; }

        public TransformDataWithDirections(TransformData transformData, Vector3 up, Vector3 right, Vector3 forward)
        {
            this.transformData = transformData;
            this.up = up;
            this.right = right;
            this.forward = forward;
        }

        public TransformDataWithDirections(Vector3 position, Quaternion rotation, Vector3 localScale, Vector3 up, Vector3 right, Vector3 forward)
        {
            this.transformData = new TransformData(position, rotation, localScale);
            this.up = up;
            this.right = right;
            this.forward = forward;
        }
    }
}