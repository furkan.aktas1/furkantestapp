﻿using UnityEngine;

namespace GybeGames.Utility.HelperClasses
{
    [System.Serializable]
    public struct TransformData
    {
        [SerializeField] private Vector3 position;
        [SerializeField] private Quaternion rotation;
        [SerializeField] private Vector3 scale;

        public Vector3 Position { get => position; }
        public Quaternion Rotation { get => rotation; }
        public Vector3 Scale { get => scale; }

        public TransformData(Vector3 position, Quaternion rotation, Vector3 scale)
        {
            this.position = position;
            this.rotation = rotation;
            this.scale = scale;
        }
    }
}
