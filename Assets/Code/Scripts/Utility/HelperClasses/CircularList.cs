﻿using System.Collections.Generic;

namespace GybeGames.Utilities.HelperClasses
{
    public class CircularList<T> : List<T>
    {
        public T GetBack(int i)
        {
            if (i == 0)
            {
                return this[this.Count - 1];
            }
            return this[i - 1];
        }

        public T GetFront(int i)
        {
            if (i == this.Count - 1)
            {
                return this[0];
            }
            return this[i + 1];
        }
    }
}