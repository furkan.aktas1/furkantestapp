﻿using GybeGames.Utility.Extentions;
using UnityEngine;

namespace GybeGames.Utility.HelperClasses
{
    [System.Serializable]
    public struct TransformDataWithDirection
    {
        [SerializeField] private TransformData transformData;
        [SerializeField] private Ray ray;

        public TransformData TransformData { get => transformData; }
        public Ray Ray { get => ray; }

        public TransformDataWithDirection(TransformData transformData, Ray ray)
        {
            this.transformData = transformData;
            this.ray = ray;
        }
        public TransformDataWithDirection(TransformData transformData, Vector3 forwardVector)
        {
            this.transformData = transformData;
            this.ray = new Ray(transformData.Position, forwardVector);
        }

        public TransformDataWithDirection(Vector3 position, Quaternion rotation, Vector3 localScale, Ray ray)
        {
            this.transformData = new TransformData(position, rotation, localScale);
            this.ray = ray;
        }

        public TransformDataWithDirection(Vector3 position, Quaternion rotation, Vector3 localScale, Vector3 forwardVector)
        {
            this.transformData = new TransformData(position, rotation, localScale);
            this.ray = new Ray(position, forwardVector);
        }
    }
}