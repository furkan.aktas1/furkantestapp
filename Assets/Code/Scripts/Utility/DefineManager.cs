﻿#if UNITY_EDITOR
using UnityEditor;
using System.Collections.Generic;
using System.Text;
using System;

namespace GybeGames.Utilities
{
    public static class DefineManager
    {
        private static BuildTargetGroup[] sWorkingBuildTargetGroups = null;

        #region Common API

        /// <summary>
        /// Defines the symbol globally for all build target groups if they don't exist.
        /// </summary>
        /// <param name="symbol">Symbol.</param>
        public static void AddDefine(string symbol)
        {
            SDS_AddDefineOnAllPlatforms(symbol);
        }

        /// <summary>
        /// Defines the symbols globally for all build target groups if they don't exist.
        /// </summary>
        /// <param name="symbols">Symbols.</param>
        public static void AddDefines(string[] symbols)
        {
            SDS_AddDefinesOnAllPlatforms(symbols);
        }

        /// <summary>
        /// Undefines the symbol on all build target groups where they exist.
        /// </summary>
        /// <param name="symbol">Symbol.</param>
        public static void RemoveDefine(string symbol)
        {
            SDS_RemoveDefineOnAllPlatforms(symbol);
        }

        /// <summary>
        /// Undefines the symbols on all build target groups where they exist.
        /// </summary>
        /// <param name="symbols">Symbols.</param>
        public static void RemoveDefines(string[] symbols)
        {
            SDS_RemoveDefinesOnAllPlatforms(symbols);
        }

        #endregion

        #region Global define using Scripting Define Symbols (SDS) in Player Settings

        public static bool SDS_IsDefined(string symbol, BuildTargetGroup platform)
        {
            string symbolStr = PlayerSettings.GetScriptingDefineSymbolsForGroup(platform);
            List<string> symbols = new List<string>(symbolStr.Split(';'));

            return symbols.Contains(symbol);
        }

        /// <summary>
        /// Adds the scripting define symbol to all platforms where it doesn't exist.
        /// </summary>
        /// <param name="symbol">Symbol.</param>
        public static void SDS_AddDefineOnAllPlatforms(string symbol)
        {
            foreach (BuildTargetGroup target in GetWorkingBuildTargetGroups())
            {
                SDS_AddDefine(symbol, target);
            }
        }

        /// <summary>
        /// Adds the scripting define symbols in the given array to all platforms where they don't exist. 
        /// </summary>
        /// <param name="symbols">Symbols.</param>
        public static void SDS_AddDefinesOnAllPlatforms(string[] symbols)
        {
            foreach (BuildTargetGroup target in GetWorkingBuildTargetGroups())
            {
                SDS_AddDefines(symbols, target);
            }
        }

        /// <summary>
        /// Adds the scripting define symbols in given array to the target platforms if they don't exist.
        /// </summary>
        /// <param name="symbols">Symbols.</param>
        /// <param name="platform">Platform.</param>
        public static void SDS_AddDefines(string[] symbols, BuildTargetGroup platform)
        {
            string symbolStr = PlayerSettings.GetScriptingDefineSymbolsForGroup(platform);
            List<string> currentSymbols = new List<string>(symbolStr.Split(';'));
            int added = 0;

            foreach (string symbol in symbols)
            {
                if (!currentSymbols.Contains(symbol))
                {
                    currentSymbols.Add(symbol);
                    added++;
                }
            }

            if (added > 0)
            {
                StringBuilder sb = new StringBuilder();

                for (int i = 0; i < currentSymbols.Count; i++)
                {
                    sb.Append(currentSymbols[i]);
                    if (i < currentSymbols.Count - 1)
                        sb.Append(";");
                }

                PlayerSettings.SetScriptingDefineSymbolsForGroup(platform, sb.ToString());
            }
        }

        /// <summary>
        /// Adds the scripting define symbols on the platform if it doesn't exist.
        /// </summary>
        /// <param name="symbol">Symbol.</param>
        /// <param name="Platform">Platform.</param>
        public static void SDS_AddDefine(string symbol, BuildTargetGroup platform)
        {
            string symbolStr = PlayerSettings.GetScriptingDefineSymbolsForGroup(platform);
            List<string> symbols = new List<string>(symbolStr.Split(';'));

            if (!symbols.Contains(symbol))
            {
                symbols.Add(symbol);

                StringBuilder sb = new StringBuilder();

                for (int i = 0; i < symbols.Count; i++)
                {
                    sb.Append(symbols[i]);
                    if (i < symbols.Count - 1)
                        sb.Append(";");
                }

                PlayerSettings.SetScriptingDefineSymbolsForGroup(platform, sb.ToString());
            }
        }

        /// <summary>
        /// Removes the scripting define symbols in the given array on all platforms where they exist.
        /// </summary>
        /// <param name="symbols">Symbols.</param>
        public static void SDS_RemoveDefinesOnAllPlatforms(string[] symbols)
        {
            foreach (BuildTargetGroup target in GetWorkingBuildTargetGroups())
            {
                SDS_RemoveDefines(symbols, target);
            }
        }

        /// <summary>
        /// Removes the scripting define symbol on all platforms where it exists.
        /// </summary>
        /// <param name="symbol">Symbol.</param>
        public static void SDS_RemoveDefineOnAllPlatforms(string symbol)
        {
            foreach (BuildTargetGroup target in GetWorkingBuildTargetGroups())
            {
                SDS_RemoveDefine(symbol, target);
            }
        }

        /// <summary>
        /// Removes the scripting define symbols in the given array on the target platform if they exists.
        /// </summary>
        /// <param name="symbols">Symbols.</param>
        /// <param name="platform">Platform.</param>
        public static void SDS_RemoveDefines(string[] symbols, BuildTargetGroup platform)
        {
            string symbolStr = PlayerSettings.GetScriptingDefineSymbolsForGroup(platform);
            List<string> currentSymbols = new List<string>(symbolStr.Split(';'));
            int removed = 0;

            foreach (string symbol in symbols)
            {
                if (currentSymbols.Contains(symbol))
                {
                    currentSymbols.Remove(symbol);
                    removed++;
                }
            }

            if (removed > 0)
            {
                StringBuilder sb = new StringBuilder();

                for (int i = 0; i < currentSymbols.Count; i++)
                {
                    sb.Append(currentSymbols[i]);
                    if (i < currentSymbols.Count - 1)
                        sb.Append(";");
                }

                PlayerSettings.SetScriptingDefineSymbolsForGroup(platform, sb.ToString());
            }
        }

        /// <summary>
        /// Removes the scripting define symbol on the platform if it exists.
        /// </summary>
        /// <param name="symbol">Symbol.</param>
        /// <param name="Platform">Platform.</param>
        public static void SDS_RemoveDefine(string symbol, BuildTargetGroup platform)
        {
            string symbolStr = PlayerSettings.GetScriptingDefineSymbolsForGroup(platform);
            List<string> symbols = new List<string>(symbolStr.Split(';'));

            if (symbols.Contains(symbol))
            {
                symbols.Remove(symbol);

                StringBuilder settings = new StringBuilder();

                for (int i = 0; i < symbols.Count; i++)
                {
                    settings.Append(symbols[i]);
                    if (i < symbols.Count - 1)
                        settings.Append(";");
                }

                PlayerSettings.SetScriptingDefineSymbolsForGroup(platform, settings.ToString());
            }
        }

        #endregion


        #region  Helpers
        /// <summary>
        /// Gets all supported build target groups, excluding the <see cref="BuildTargetGroup.Unknown"/>
        /// and the obsolete ones.
        /// </summary>
        /// <returns>The working build target groups.</returns>
        public static BuildTargetGroup[] GetWorkingBuildTargetGroups()
        {
            if (sWorkingBuildTargetGroups != null)
                return sWorkingBuildTargetGroups;

            var groups = new List<BuildTargetGroup>();
            Type btgType = typeof(BuildTargetGroup);

            foreach (string name in System.Enum.GetNames(btgType))
            {
                // First check obsolete.
                var memberInfo = btgType.GetMember(name)[0];
                if (System.Attribute.IsDefined(memberInfo, typeof(System.ObsoleteAttribute)))
                    continue;

                // Name -> enum value and exclude the 'Unknown'.
                BuildTargetGroup g = (BuildTargetGroup)Enum.Parse(btgType, name);
                if (g != BuildTargetGroup.Unknown)
                    groups.Add(g);
            }

            sWorkingBuildTargetGroups = groups.ToArray();
            return sWorkingBuildTargetGroups;
        }
        #endregion
    }
}
#endif