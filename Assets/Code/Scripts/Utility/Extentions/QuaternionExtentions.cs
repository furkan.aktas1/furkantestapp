using UnityEngine;

namespace GybeGames.Utility.Extentions
{
    public static class QuaternionExtentions
    {
        public static Quaternion ClampRotation(this Quaternion quaternion, Vector3 boundsMin, Vector3 boundsMax)
        {
            Quaternion q = quaternion;
            q.x /= q.w;
            q.y /= q.w;
            q.z /= q.w;
            q.w = 1.0f;

            float angleX = 2.0f * Mathf.Rad2Deg * Mathf.Atan(q.x);
            angleX = Mathf.Clamp(angleX, boundsMin.x, boundsMax.x);
            q.x = Mathf.Tan(0.5f * Mathf.Deg2Rad * angleX);

            float angleY = 2.0f * Mathf.Rad2Deg * Mathf.Atan(q.y);
            angleY = Mathf.Clamp(angleY, boundsMin.y, boundsMax.y);
            q.y = Mathf.Tan(0.5f * Mathf.Deg2Rad * angleY);

            float angleZ = 2.0f * Mathf.Rad2Deg * Mathf.Atan(q.z);
            angleZ = Mathf.Clamp(angleZ, boundsMin.z, boundsMax.z);
            q.z = Mathf.Tan(0.5f * Mathf.Deg2Rad * angleZ);

            return q;
        }

    }
}