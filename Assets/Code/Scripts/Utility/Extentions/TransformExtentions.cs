﻿using GybeGames.Utility.HelperClasses;
using UnityEngine;

namespace GybeGames.Utility.Extentions
{
    public static class TransformExtentions
    {
        public static TransformData GetTransformData(this Transform transform)
        {
            return new TransformData(transform.position, transform.rotation, transform.localScale);
        }

        public static TransformData GetLocalTransformData(this Transform transform)
        {
            return new TransformData(transform.localPosition, transform.localRotation, transform.localScale);
        }
        public static TransformData GetTransformDataWithLossyScale(this Transform transform)
        {
            return new TransformData(transform.position, transform.rotation, transform.lossyScale);
        }

        public static TransformDataWithDirection GetTransformDataWithDirection(this Transform transform)
        {
            return new TransformDataWithDirection(transform.position, transform.rotation, transform.localScale, transform.forward);
        }

        public static TransformDataWithDirection GetLocalTransformDataWithDirection(this Transform transform)
        {
            return new TransformDataWithDirection(transform.localPosition, transform.localRotation, transform.localScale, transform.forward);
        }
        public static TransformDataWithDirection GetTransformDataWithDirectionAndLossyScale(this Transform transform)
        {
            return new TransformDataWithDirection(transform.position, transform.rotation, transform.lossyScale, transform.forward);
        }

        public static TransformDataWithDirections GetTransformDataWithDirections(this Transform transform)
        {
            return new TransformDataWithDirections(transform.position, transform.rotation, transform.localScale, transform.up, transform.right, transform.forward);
        }

        public static TransformDataWithDirections GetLocalTransformDataWithDirections(this Transform transform)
        {
            return new TransformDataWithDirections(transform.localPosition, transform.localRotation, transform.localScale, transform.up, transform.right, transform.forward);
        }
        public static TransformDataWithDirections GetTransformDataWithDirectionsAndLossyScale(this Transform transform)
        {
            return new TransformDataWithDirections(transform.position, transform.rotation, transform.lossyScale, transform.up, transform.right, transform.forward);
        }
    }
}