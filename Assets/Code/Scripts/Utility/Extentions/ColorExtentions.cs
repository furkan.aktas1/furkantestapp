﻿using UnityEngine;

namespace GybeGames.Utility.Extentions
{
    public static class ColorExtentions
    {
        public static Color SetAlpha(this Color color, float alphaValue)
        {
            return new Color(color.r, color.g, color.b, alphaValue);
        }
    }
}
