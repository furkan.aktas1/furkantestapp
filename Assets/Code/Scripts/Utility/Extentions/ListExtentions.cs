﻿using Lean.Pool;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace GybeGames.Utility.Extentions
{
    public static class ListExtentions
    {
        public static T GetRandom<T>(this List<T> list)
        {
            int randomIndex = UnityEngine.Random.Range(0, list.Count);
            return list[randomIndex];
        }

        public static T GetRandom<T>(this List<T> list, out int? index)
        {
            int randomIndex = UnityEngine.Random.Range(0, list.Count);
            index = randomIndex;
            return list[randomIndex];
        }

        public static T GetRandom<T>(this List<T> list, List<int> notIncludedIndexes)
        {
            List<T> newList = new List<T>();
            for (int i = 0; i < list.Count; i++)
            {
                if (notIncludedIndexes.Contains(i))
                    continue;
                newList.Add(list[i]);
            }
            int randomIndex = UnityEngine.Random.Range(0, newList.Count);
            return newList[randomIndex];
        }

        public static T GetRandom<T>(this List<T> list, List<int> notIncludedIndexes, out int? index)
        {
            List<T> newList = new List<T>();
            for (int i = 0; i < list.Count; i++)
            {
                if (notIncludedIndexes.Contains(i))
                    continue;
                newList.Add(list[i]);
            }
            int randomIndex = UnityEngine.Random.Range(0, newList.Count);
            index = randomIndex;
            return newList[randomIndex];
        }

        public static void DespawnCachedList<T>(this List<T> list) where T : Component
        {
            for (int i = 0; i < list.Count; i++)
            {
                LeanPool.Despawn(list[i]);
            }
            list.Clear();
        }

        public static List<T> Clone<T>(this List<T> list)
        {
            List<T> cloneList = new List<T>(list.Count);
            for (int i = 0; i < list.Count; i++)
            {
                cloneList.Add(list[i]);
            }
            return cloneList;
        }

        public static List<T> DeepClone<T>(this List<T> list) where T : ICloneable
        {
            List<T> cloneList = new List<T>(list.Count);
            for (int i = 0; i < list.Count; i++)
            {
                cloneList.Add((T)list[i].Clone());
            }
            return cloneList;
        }
    }
}

