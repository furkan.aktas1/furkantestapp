﻿using UnityEngine;

namespace GybeGames.Utility.Extentions
{
    public static class Vector3Extentions
    {
        public static Vector2 GetVector2AsXY(this Vector3 vector3)
        {
            return new Vector2(vector3.x, vector3.y);
        }

        public static Vector2 GetVector2AsXZ(this Vector3 vector3)
        {
            return new Vector2(vector3.x, vector3.z);
        }

        public static Vector3 ZeroX(this Vector3 vector3)
        {
            return new Vector3(0, vector3.y, vector3.z);
        }
        public static Vector3 ZeroY(this Vector3 vector3)
        {
            return new Vector3(vector3.x, 0, vector3.z);
        }
        public static Vector3 ZeroZ(this Vector3 vector3)
        {
            return new Vector3(vector3.x, vector3.y, 0);
        }
    }
}

