﻿using System;
using System.Linq;

namespace GybeGames.Utility.Extentions
{
    public static class DoubleExtentions
    {
        /// <summary>
        /// Abbrivates this number with the 1000th indices. <br/>
        /// Example:<br/>
        /// 1,000 returns 1k <br/>
        /// 1,000,000 returns 1m <br/>
        /// 1,000,000,000 returns 1b <br/>
        /// and so on...<br/>
        /// </summary>
        /// <param name="num"></param>
        /// <returns></returns>
        public static string AbbrivateNum(this double num)
        {
            if (num < 1000) return num.ToString("N0");
            int exp = (int)(Math.Log(num) / Math.Log(1000));
            string numStr = string.Format("{0:0.00}", num / Math.Pow(1000, exp));
            numStr = numStr.Substring(0, 4).Trim('.');
            return string.Format("{0}{1}", numStr, ExtentionHelpers.CURRENCY_ABBREVATIONS.ElementAt(exp - 1));
        }
    }
}

