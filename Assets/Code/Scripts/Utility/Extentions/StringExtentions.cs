﻿namespace GybeGames.Utility.Extentions
{
    public static class StringExtentions
    {
        public static string GetLastType(this string fullTypeName)
        {
            return fullTypeName.Substring(fullTypeName.LastIndexOf('.') + 1);
        }
    }
}
