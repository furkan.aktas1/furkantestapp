﻿using GybeGames.Systems.Cameras;
using GybeGames.Systems;
using UnityEngine;
using GybeGames.Systems.Feedback;

namespace GybeGames.Utility.Extentions
{
    public static class IFeedbackExtentions
    {
        public static IFeedback SetAppearPosition(this IFeedback feedback, RectTransform appearRectTransform)
        {
            feedback.AppearPosition = appearRectTransform.position;
            return feedback;
        }

        public static IFeedback SetAppearPosition(this IFeedback feedback, Vector3 appearWorldPosition)
        {
            Vector3 fromPosition = Gybe.GetSystem<CameraSystem>().FromMainWorldPointToUIWorldPoint(appearWorldPosition);
            feedback.AppearPosition = fromPosition;
            return feedback;
        }

        public static IFeedback SetDisappearAncOffset(this IFeedback feedback, Vector2 disappearAncOffset)
        {
            feedback.DisappearAncOffset = disappearAncOffset;
            return feedback;
        }

        public static IFeedback SetScale(this IFeedback feedback, float scale)
        {
            feedback.Scale = scale;
            return feedback;
        }

        public static IFeedback SetDuration(this IFeedback feedback, float duration)
        {
            feedback.Duration = duration;
            return feedback;
        }

        public static IFeedback SetRotationAngle(this IFeedback feedback, float rotationAngle)
        {
            feedback.RotationAngle = rotationAngle;
            return feedback;
        }

        public static IFeedback SetRotationRepeat(this IFeedback feedback, int loopTime)
        {
            feedback.RotationRepeat = loopTime;
            return feedback;
        }

        public static IFeedback SetFadeOffOnDisappear(this IFeedback feedback, bool hasFadeOffDisappear)
        {
            feedback.HasFadeOffDisappear = hasFadeOffDisappear;
            return feedback;
        }
    }
}

