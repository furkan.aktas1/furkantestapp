﻿using GybeGames.Systems.Windows;

namespace GybeGames.Utility.Extentions
{
    public static class IWindowExtentions
    {
        public static void Show(this IWindow iWindow, float? delay = null, float? duration = null)
        {
            iWindow?.Appear(delay, duration);
        }

        public static void Hide(this IWindow iWindow, float? delay = null, float? duration = null)
        {
            iWindow?.Disappear(delay, duration);
        }

        public static bool IsVisible(this IWindow iWindow)
        {
            return iWindow?.IsVisible() ?? false;
        }
    }
}
