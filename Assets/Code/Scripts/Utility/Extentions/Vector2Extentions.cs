﻿using UnityEngine;

namespace GybeGames.Utility.Extentions
{
    public static class Vector2Extentions
    {
        public static Vector3 GetVector3AsXY(this Vector2 vector2)
        {
            return new Vector3(vector2.x, vector2.y, 0);
        }

        public static Vector3 GetVector3AsXZ(this Vector2 vector2)
        {
            return new Vector3(vector2.x, 0, vector2.y);
        }

        public static float GetMin(this Vector2 vector2)
        {
            return Mathf.Min(vector2.x, vector2.y);
        }

        public static float GetMax(this Vector2 vector2)
        {
            return Mathf.Max(vector2.x, vector2.y);
        }

        public static bool IsInBetween(this Vector2 vector2, float value)
        {
            float min = vector2.GetMin();
            float max = vector2.GetMax();
            if (value <= max && value >= min)
            {
                return true;
            }
            return false;
        }
    }
}

