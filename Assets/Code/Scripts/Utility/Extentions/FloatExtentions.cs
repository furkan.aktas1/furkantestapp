﻿using UnityEngine;

namespace GybeGames.Utility.Extentions
{
    public static class FloatExtentions
    {
        /// <summary>
        /// Linearly interpolates the given start and end values with this percentage value. <br/>
        /// </summary>
        /// <param name="value"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        public static double Lerp(this float value, double start, double end)
        {
            value = Mathf.Clamp01(value);
            return start + ((end - start) * value);
        }
    }
}

