using GybeGames.Systems.EventSystem;

namespace GybeGames.UI.GameOpenLoadings
{
    public class SetGameOpenLoadingPercentageEvent : CustomEvent
    {
        public int Percentage { get; private set; }
        public static SetGameOpenLoadingPercentageEvent Create(int percenatge)
        {
            SetGameOpenLoadingPercentageEvent setGameOpenLoadingPercentageEvent = new SetGameOpenLoadingPercentageEvent();
            setGameOpenLoadingPercentageEvent.Percentage = percenatge;
            return setGameOpenLoadingPercentageEvent;
        }
    }
}

