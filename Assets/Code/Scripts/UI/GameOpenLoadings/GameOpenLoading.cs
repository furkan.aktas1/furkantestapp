using GybeGames.Systems.EventSystem;
using GybeGames.Systems.Scenes.Events;
using Sirenix.OdinInspector;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace GybeGames.UI.GameOpenLoadings
{
    public class GameOpenLoading : MonoBehaviour
    {
        [SerializeField, Required] private Slider slider = null;
        [SerializeField, Required] private TextMeshProUGUI percentageText = null;

        private int currentPercentageLimit = 20;
        private int currentPercentage = 0;

        private void OnEnable()
        {
            ChangeValue(0);
            StartCoroutine(EventLoading());
            //StartCoroutine(FakeLoading());
            EventManager.RegisterHandler<GameSceneLoadedEvent>(OnGameSceneLoadedEvent);
            EventManager.RegisterHandler<SetGameOpenLoadingPercentageEvent>(OnPercentageSet);
        }

        private void OnDisable()
        {
            EventManager.UnregisterHandler<GameSceneLoadedEvent>(OnGameSceneLoadedEvent);
            EventManager.UnregisterHandler<SetGameOpenLoadingPercentageEvent>(OnPercentageSet);
        }

        private void OnGameSceneLoadedEvent(GameSceneLoadedEvent gameSceneLoadedEvent)
        {
            StopAllCoroutines();
            ChangeValue(1);
            StartCoroutine(DisactivateCoroutine());
        }

        private void OnPercentageSet(SetGameOpenLoadingPercentageEvent setGameOpenLoadingPercentageEvent)
        {
            currentPercentageLimit = setGameOpenLoadingPercentageEvent.Percentage;
        }

        private IEnumerator EventLoading()
        {
            float time = 0;
            float duration = 4;
            while (true)
            {
                if (currentPercentage < currentPercentageLimit)
                {
                    ChangeValue(time / duration);
                    time += 0.2f;
                }
                yield return new WaitForSecondsRealtime(0.2f);
            }
        }

        private IEnumerator FakeLoading()
        {
            float time = 0;
            float duration = 4;
            while (time < duration)
            {
                ChangeValue(time / duration);
                time += 0.2f;
                yield return new WaitForSecondsRealtime(0.2f);
            }
        }

        private IEnumerator DisactivateCoroutine()
        {
            yield return new WaitForSeconds(0.4f);
            Destroy(gameObject);
        }

        private void ChangeValue(float value)
        {
            slider.value = value;
            currentPercentage = (int)Mathf.Round(value * 100);
            percentageText.text = currentPercentage.ToString() + "%";
        }

#if UNITY_EDITOR

        [Button]
        private void SetPercentageLimit(int percentage)
        {
            currentPercentageLimit = percentage;
        }

#endif
    }
}

