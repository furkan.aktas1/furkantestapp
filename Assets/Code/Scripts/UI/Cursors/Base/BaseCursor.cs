﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace GybeGames.UI.Stars.Cursors
{
    public abstract class BaseCursor : MonoBehaviour
    {
        [Title("References")]
        [SerializeField]
        protected Sprite normalState;

        [SerializeField]
        protected Sprite clickState;

        public abstract void Initialize();
        public abstract void DeInitialize();
        
        public abstract void SetPosition(Vector3 position);
        public abstract void SetState(bool isTouchDown);

        public abstract void SetActivation(bool activate);
        
    }
}