﻿using UnityEngine;

namespace GybeGames.UI.Stars.Cursors
{
    public abstract class BaseCursorController : MonoBehaviour
    {
        public abstract void Initialize();
        public abstract void DeInitialize();

        public abstract void SetActive(bool activate);
    }
}