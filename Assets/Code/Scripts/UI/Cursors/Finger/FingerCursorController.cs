﻿using System;
using GybeGames.Systems;
using GybeGames.Systems.Cameras;
using GybeGames.Systems.Input;
using Sirenix.OdinInspector;
using UnityEngine;

namespace GybeGames.UI.Stars.Cursors
{
    public class FingerCursorController : BaseCursorController
    {
        private BaseCursor cursor = null;
        private Vector2 size;

        private InputSystem inputSystem { get => _inputSystem ?? (_inputSystem = Gybe.GetSystem<InputSystem>()); }
        private InputSystem _inputSystem = null;
        
        private void Awake()
        {
            cursor = GetComponentInChildren<BaseCursor>();
        }

        private void Start()
        {
            Initialize();
        }

        private void OnEnable()
        {
            inputSystem.OnDown += OnDown;
            inputSystem.OnUp += OnUp;
        }

        private void OnDisable()
        {
            inputSystem.OnDown -= OnDown;
            inputSystem.OnUp -= OnUp;
        }

        private void OnDown(Vector2 arg0)
        {
            cursor.SetState(true);
        }

        private void OnUp(Vector2 arg0)
        {
            cursor.SetState(false);
        }

        public override void Initialize()
        {
            cursor.Initialize();
            size = cursor.transform.parent.GetComponent<RectTransform>().rect.size;
        }

        public override void DeInitialize()
        {
            cursor.DeInitialize();
        }

        public override void SetActive(bool activate)
        {
            cursor.SetActivation(activate);
        }

        private void Update()
        {
            var viewportPoint = Gybe.GetSystem<CameraSystem>().UICamera.ScreenToViewportPoint(Input.mousePosition);
            
            Vector3 pos = new Vector3(size.x * viewportPoint.x, size.y * viewportPoint.y, 0);
            cursor.SetPosition(pos);
        }
        
        
        [Button("Open/Close Finger Cursor")]
        private void Editor_OpenOrCloseFingerCursor()
        {
            cursor.gameObject.SetActive(!cursor.gameObject.activeSelf);
        }
    }
}