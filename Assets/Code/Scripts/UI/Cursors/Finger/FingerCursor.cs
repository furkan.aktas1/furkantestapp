﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace GybeGames.UI.Stars.Cursors
{
    public class FingerCursor : BaseCursor
    {
        private RectTransform rectTransform = null;
        private Image image = null;
        private Vector3 defaultScale;

        private void Awake()
        {
            rectTransform = GetComponent<RectTransform>();
            image = GetComponentInChildren<Image>();
        }

        public override void Initialize()
        {
            defaultScale = image.rectTransform.localScale;
            //SetActivation(false);
        }

        public override void DeInitialize()
        {
            SetActivation(false);
        }

        public override void SetPosition(Vector3 position)
        {
            image.rectTransform.anchoredPosition3D = position;
        }

        public override void SetState(bool isTouchDown)
        {
            if (isTouchDown)
            {
                //image.sprite = clickState;

                image.rectTransform.DOKill(false);
                image.rectTransform.localScale = defaultScale;
                Sequence sequence = DOTween.Sequence().Pause();
                Vector3 rectTransformLocalScale = image.rectTransform.localScale;
                sequence.Append(image.rectTransform.DOScale(rectTransformLocalScale * 0.7f, 0.1f));
                //sequence.Append(image.rectTransform.DOScale(rectTransformLocalScale, 0.1f));
                sequence.Play();

            }
            else
            {
                image.rectTransform.DOKill(false);
                image.rectTransform.DOScale(defaultScale, 0.1f);
                //image.sprite = normalState;
            }
        }

        public override void SetActivation(bool activate)
        {
            image.enabled = activate;
        }
    }
}