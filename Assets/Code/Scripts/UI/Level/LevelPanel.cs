using GybeGames.Systems;
using GybeGames.Systems.EventSystem;
using GybeGames.Systems.Levels;
using GybeGames.Systems.Levels.Events;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;

namespace GybeGames.UI.Level
{
    public class LevelPanel : MonoBehaviour
    {
        [SerializeField, Required] private TextMeshProUGUI levelText = null;

        private LevelSystem levelSystem { get => _levelSystem ?? (_levelSystem = Gybe.GetSystem<LevelSystem>()); }

        private LevelSystem _levelSystem = null;


        private void OnEnable()
        {
            EventManager.RegisterHandler<LevelLoadedEvent>(OnLevelLoaded);
        }

        private void OnDisable()
        {
            EventManager.UnregisterHandler<LevelLoadedEvent>(OnLevelLoaded);
        }

        private void OnLevelLoaded(LevelLoadedEvent levelLoadedEvent)
        {
            levelText.text = "Level " + levelSystem.HighestLevelNumber.ToString();
        }
    }
}

