﻿using GybeGames.Systems;
using GybeGames.Systems.Levels;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GybeGames.UI.Level
{
    public class PlayButton : MonoBehaviour
    {
        private bool canEnter = true;

        private void OnEnable()
        {
            canEnter = true;
        }

        public void _StartFirstLevel()
        {
            if (!canEnter)
            {
                return;
            }
            canEnter = false;
            LevelSystem levelSystem = Gybe.GetSystem<LevelSystem>();
            levelSystem.ConfigureAndLoadLevel();
        }
    }
}

