﻿using GybeGames.Systems;
using GybeGames.Systems.Levels;
using UnityEngine;

namespace GybeGames.UI.Level
{
    public class RestartButton : MonoBehaviour
    {
        private LevelSystem LevelSystem => _levelSystem ?? (_levelSystem = Gybe.GetSystem<LevelSystem>());
        private LevelSystem _levelSystem;

        private bool canRestart = true;

        private void OnEnable()
        {
            canRestart = true;
        }

        private void OnDisable()
        {
            canRestart = false;
        }

        public void _RestartLevel()
        {
            if (!canRestart) return;

            canRestart = false;

            LevelSystem.ConfigureAndLoadLevel();
        }

    }
}

