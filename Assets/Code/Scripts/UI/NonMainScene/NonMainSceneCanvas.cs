﻿using GybeGames.Systems.Cameras;
using GybeGames.Systems;
using UnityEngine;
using GybeGames.Systems.Windows;

namespace GybeGames.UI.NonMainScene
{
    public class NonMainSceneCanvas : MonoBehaviour
    {
        private Canvas canvas { get => _canvas ?? (_canvas = GetComponent<Canvas>()); }

        private Canvas _canvas = null;

        private void Awake()
        {
            canvas.renderMode = RenderMode.ScreenSpaceCamera;
            canvas.worldCamera = Gybe.GetSystem<CameraSystem>().UICamera;
            canvas.planeDistance = WindowSystem.PLANE_DISTANCE;
        }
    }
}

