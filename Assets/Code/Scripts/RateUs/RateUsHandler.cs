﻿
using System.Collections;
using UnityEngine;
using GybeGames.Systems;
using GybeGames.Systems.EventSystem;
using GybeGames.Systems.Levels;
using GybeGames.Systems.Publishing.RemoteConfig;
#if UNITY_IOS
using UnityEngine.iOS;
#endif
#if UNITY_ANDROID
using Google.Play.Review;
#endif

namespace GybeGames.RateUs
{
    public class RateUsHandler : MonoBehaviour
    {
        private RemoteConfigurationSystem remoteConfigurationSystem { get => _remoteConfigurationSystem ?? (_remoteConfigurationSystem = Gybe.GetSystem<RemoteConfigurationSystem>()); }

        private RemoteConfigurationSystem _remoteConfigurationSystem;

        private LevelSystem levelSystem { get => _levelSystem ?? (_levelSystem = Gybe.GetSystem<LevelSystem>()); }

        private LevelSystem _levelSystem = null;

        private void OnEnable()
        {
            EventManager.RegisterHandler<RequestRateUsEvent>(OnRequestRateUs);
        }

        private void OnDisable()
        {
            EventManager.UnregisterHandler<RequestRateUsEvent>(OnRequestRateUs);
        }

        private void OnRequestRateUs(RequestRateUsEvent obj)
        {
            RequestRateUsPanel();
        }

        private void RequestRateUsPanel()
        {
            int realRateUsLevelForRemote = remoteConfigurationSystem.RateUsLevel; //2

            int highestLevelNumber = levelSystem.HighestLevelNumber - 1; //2

            if (realRateUsLevelForRemote == highestLevelNumber)
            {
                TryToShow();
            }
        }

        private void TryToShow()
        {
#if UNITY_IOS
        Device.RequestStoreReview();
#elif UNITY_ANDROID
            StartCoroutine(RequestAppReviewAndroid());
#endif

        }

#if UNITY_ANDROID
        private IEnumerator RequestAppReviewAndroid()
        {
            ReviewManager reviewManager = new ReviewManager();
            var requestFlowOperation = reviewManager.RequestReviewFlow();
            yield return requestFlowOperation;
            if (requestFlowOperation.Error != ReviewErrorCode.NoError)
            {
                Debug.LogError("Error while getting review flow: " + requestFlowOperation.Error.ToString());
                yield break;
            }
            PlayReviewInfo playReviewInfo = requestFlowOperation.GetResult();

            var launchFlowOperation = reviewManager.LaunchReviewFlow(playReviewInfo);
            yield return launchFlowOperation;
            if (launchFlowOperation.Error != ReviewErrorCode.NoError)
            {
                Debug.LogError("Error while getting launch flow: " + launchFlowOperation.Error.ToString());
                yield break;
            }
        }
#endif

    }
}

