using System.Diagnostics;

namespace GybeGames.Devops.Git
{
    public class GitHelper
    {
        public static string GetCurrentBranchName()
        {
            ProcessStartInfo startInfo = new ProcessStartInfo("git");

            startInfo.UseShellExecute = false;
            startInfo.RedirectStandardInput = true;
            startInfo.RedirectStandardOutput = true;
            startInfo.Arguments = "rev-parse --abbrev-ref HEAD";

            Process process = new Process
            {
                StartInfo = startInfo
            };
            process.Start();
            process.WaitForExit();

            string branchname = process.StandardOutput.ReadLine();
            return branchname;
        }
    }
}