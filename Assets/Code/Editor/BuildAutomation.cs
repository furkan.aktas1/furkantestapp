﻿using System;
using System.IO;
using System.Linq;
using System.Text;

using UnityEditor;
using UnityEngine;
using UnityEditor.Callbacks;

using GybeGames.Utilities;

using GybeGames.Devops.XCode;
using GybeGames.Devops.Android;
using GybeGames.Systems.App;

using UnityEditor.AddressableAssets.Build;
using UnityEditor.AddressableAssets.Settings;

namespace GybeGames.Devops
{
    /** 
     * Version: 2.3.0
     * Author: Cevat Aykan Sevinc, Tumay Varel
     */
    public class BuildAutomation
    {
        private static readonly string PATH_TO_PROJECT_SETTINGS = $"../DiscordAutomationBot/DiscordAutomationBot/ProjectCaches/{GetProjectName()}.txt";
        private static readonly string PATH_TO_PROJECT_SETTINGS_NEW = $"../ProjectCaches/{GetProjectName()}.txt";
        private static bool isLaunch;
        private static bool isRelease;

        public static void PerformIOSBuild()
        {
            ReadCommandLineArgs(true);

            BuildPlayerOptions buildPlayerOptions = new BuildPlayerOptions();
            SetCommanBuildPipelineSettings(ref buildPlayerOptions, true);
            buildPlayerOptions.targetGroup = BuildTargetGroup.iOS;
            buildPlayerOptions.target = BuildTarget.iOS;
            buildPlayerOptions.options = BuildOptions.None;

            OnBeforeBuildEvent();
            BuildProject(buildPlayerOptions);
            OnAfterBuildEvent();
        }

        public static void PerformAndroidBuild()
        {
            ReadCommandLineArgs(false);

            BuildPlayerOptions buildPlayerOptions = new BuildPlayerOptions();
            SetCommanBuildPipelineSettings(ref buildPlayerOptions, false);
            buildPlayerOptions.targetGroup = BuildTargetGroup.Android;
            buildPlayerOptions.target = BuildTarget.Android;
            buildPlayerOptions.options = BuildOptions.None;

            SetAndroidPlayerSettings();

            OnBeforeBuildEvent();
            BuildProject(buildPlayerOptions);
            OnAfterBuildEvent();
        }

        [MenuItem("Build/Test Android Build")]
        private static void TestAndroidBuild()
        {
            BuildPlayerOptions buildPlayerOptions = new BuildPlayerOptions();
            SetTestBuildPipelineSettings(ref buildPlayerOptions, false);
            buildPlayerOptions.targetGroup = BuildTargetGroup.Android;
            buildPlayerOptions.target = BuildTarget.Android;
            buildPlayerOptions.options = BuildOptions.None;

            SetAndroidPlayerSettings();

            OnBeforeBuildEvent();
            BuildProject(buildPlayerOptions);
            OnAfterBuildEvent();
        }

        [MenuItem("Build/Test iOS Build")]
        public static void TestIOSBuild()
        {
            BuildPlayerOptions buildPlayerOptions = new BuildPlayerOptions();
            SetTestBuildPipelineSettings(ref buildPlayerOptions, true);
            buildPlayerOptions.targetGroup = BuildTargetGroup.iOS;
            buildPlayerOptions.target = BuildTarget.iOS;
            buildPlayerOptions.options = BuildOptions.None;

            OnBeforeBuildEvent();
            BuildProject(buildPlayerOptions);
            OnAfterBuildEvent();
        }

        public static void InitAndroidProject()
        {
            AndroidSettingsHandler.SetKeystoreAndAliasName();

            BuildPlayerOptions buildPlayerOptions = new BuildPlayerOptions();
            SetCommanBuildPipelineSettings(ref buildPlayerOptions, false);
            buildPlayerOptions.targetGroup = BuildTargetGroup.Android;
            buildPlayerOptions.target = BuildTarget.Android;
            buildPlayerOptions.options = BuildOptions.None;

            PlayerSettings.Android.bundleVersionCode = 1;
            PlayerSettings.bundleVersion = "0.0.1";
            PlayerSettings.SplashScreen.showUnityLogo = false;

            SetAndroidPlayerSettings();

            OnBeforeBuildEvent();
            BuildProject(buildPlayerOptions);
            OnAfterBuildEvent();
        }

        [PostProcessBuildAttribute(1)]
        public static void OnPostprocessBuild(BuildTarget target, string pathToBuiltProject)
        {
            if (target == BuildTarget.iOS)
            {
                XCodeManipulator.ManipulateXCode(pathToBuiltProject, isLaunch);
            }
        }

        private static void ReadCommandLineArgs(bool isIOS)
        {
            string buildNo; // must be at first Line
            string version; // must be at sec Line

            string[] settingsProperties;

            string cachePath = Directory.Exists("../ProjectCaches") ? PATH_TO_PROJECT_SETTINGS_NEW : PATH_TO_PROJECT_SETTINGS;
            using (FileStream fileStream = new FileStream(cachePath, FileMode.Open, FileAccess.Read))
            {
                using (StreamReader streamReader = new StreamReader(fileStream, Encoding.UTF8))
                {
                    string contents = streamReader.ReadToEnd();
                    settingsProperties = contents.Split('\n');
                }
            }

            isLaunch = settingsProperties[8] == "1";
            isRelease = settingsProperties[9] == "1";
            if (isIOS)
            {
                buildNo = settingsProperties[0];
                version = settingsProperties[1];
                settingsProperties[4] = PlayerSettings.GetApplicationIdentifier(BuildTargetGroup.iOS);
                PlayerSettings.iOS.buildNumber = buildNo;
            }
            else
            {
                buildNo = settingsProperties[2];
                version = settingsProperties[3];
                settingsProperties[4] = PlayerSettings.GetApplicationIdentifier(BuildTargetGroup.Android);
                PlayerSettings.Android.bundleVersionCode = int.Parse(buildNo);
            }
            PlayerSettings.bundleVersion = version;
            PlayerSettings.SplashScreen.showUnityLogo = false;

            settingsProperties[7] = PlayerSettings.iOS.iOSManualProvisioningProfileID;
            using (StreamWriter streamWriter = new StreamWriter(cachePath, false))
            {
                for (int i = 0; i < settingsProperties.Length - 1; ++i)
                {
                    streamWriter.WriteLine(settingsProperties[i]);
                }
                streamWriter.Write(settingsProperties[settingsProperties.Length - 1]);
            }

            if (buildNo == string.Empty && version == string.Empty)
                throw new Exception("Build no and/or version is empty!");
        }

        private static void SetCommanBuildPipelineSettings(ref BuildPlayerOptions buildPlayerOptions, bool isIOS)
        {
            string projectName = GetProjectName();
            string endPath = isIOS ? "ios/" : $"android/{projectName}";

            buildPlayerOptions.scenes = File.Exists("Assets/Elephant/elephant_scene.unity") ?
                new[] { "Assets/Elephant/elephant_scene.unity", "Assets/Level/Scenes/MainScene.unity", "Assets/Level/Scenes/GameScene.unity" }
                : new[] { "Assets/Level/Scenes/MainScene.unity", "Assets/Level/Scenes/GameScene.unity" };
            buildPlayerOptions.locationPathName = $"../builds/{projectName}-{endPath}";
        }

        private static void SetTestBuildPipelineSettings(ref BuildPlayerOptions buildPlayerOptions, bool isIOS)
        {
            string projectName = GetProjectName();
            string endPath = isIOS ? "ios/" : $"android/{projectName}";

            buildPlayerOptions.scenes = File.Exists("Assets/Elephant/elephant_scene.unity") ?
                new[] { "Assets/Elephant/elephant_scene.unity", "Assets/Level/Scenes/MainScene.unity", "Assets/Level/Scenes/GameScene.unity" }
                : new[] { "Assets/Level/Scenes/MainScene.unity", "Assets/Level/Scenes/GameScene.unity" };
            buildPlayerOptions.locationPathName = $"./{projectName}-{endPath}";
        }

        private static string GetProjectName()
        {
            string fullPath = Path.GetFullPath(Directory.GetCurrentDirectory()).TrimEnd(Path.DirectorySeparatorChar);
            string projectName = fullPath.Split(Path.DirectorySeparatorChar).Last();
            return projectName;
        }

        private static void BuildProject(in BuildPlayerOptions buildPlayerOptions)
        {
            UnityEngine.Debug.Log("Build started in Unity");
            try
            {
                BuildPipeline.BuildPlayer(buildPlayerOptions);
            }
            catch (Exception e)
            {
                UnityEngine.Debug.Log($"Build failed: {e}");
            }
            UnityEngine.Debug.Log("Build process end in Unity");
        }

        private static void SetAndroidPlayerSettings()
        {
            EditorUserBuildSettings.buildAppBundle = true;
            AndroidSettingsHandler.SetAndroidKeystorePasswords();
        }

        private static void OnBeforeBuildEvent()
        {
            SetAppSettingsConfigurations();

            BuildAddressables();

            CheckPlatformArguments();
        }

        private static void OnAfterBuildEvent()
        {
            AppSettings appSettings = GetAppsettings();
            appSettings.UnityLogType = LogType.Log;
#if DEV_MODE
            DefineManager.RemoveDefine("DEV_MODE");
#endif
            EditorUtility.SetDirty(appSettings);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }
        
        [MenuItem("Build/Build Addressables")]
        private static void BuildAddressables()
        {
            AddressableAssetSettings.CleanPlayerContent();
            AddressableAssetSettings
                .BuildPlayerContent(out AddressablesPlayerBuildResult result);
            bool success = string.IsNullOrEmpty(result.Error);
        }

        private static void SetAppSettingsConfigurations()
        {
            AppSettings appSettings = GetAppsettings();
            appSettings.UnityLogType = LogType.Exception;

            if (isRelease)
            {
#if DEV_MODE
                DefineManager.RemoveDefine("DEV_MODE");
#endif
            }
            else
            {
#if !DEV_MODE
                DefineManager.AddDefine("DEV_MODE");
#endif
            }

            EditorUtility.SetDirty(appSettings);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }
        
        private static AppSettings GetAppsettings()
        {
            string[] appOptionGUID = AssetDatabase.FindAssets("t:AppSettings", null);
            if (appOptionGUID.Length > 1)
            {
                UnityEngine.Debug.Log($"Umm wtf? : {appOptionGUID.Length}");
            }
            string path = AssetDatabase.GUIDToAssetPath(appOptionGUID[0]);
            return AssetDatabase.LoadAssetAtPath<AppSettings>(path);
        }

        [MenuItem("Build/Test Arguments")]
        private static void CheckPlatformArguments()
        {
            string errorMsg = string.Empty;
            if (PlayerSettings.GetApplicationIdentifier(BuildTargetGroup.Android) == null
                || PlayerSettings.GetApplicationIdentifier(BuildTargetGroup.Android) == string.Empty
                || PlayerSettings.GetApplicationIdentifier(BuildTargetGroup.Android) == "com.GybeGames.StandardSetup"
                || PlayerSettings.GetApplicationIdentifier(BuildTargetGroup.iOS) == null
                || PlayerSettings.GetApplicationIdentifier(BuildTargetGroup.iOS) == string.Empty
                || PlayerSettings.GetApplicationIdentifier(BuildTargetGroup.iOS) == "com.GybeGames.StandardSetup")
            {
                errorMsg += "Bundle id is either null, empty or com.GybeGames.StandardSetup\n";
            }

            if ( PlayerSettings.productName == null || PlayerSettings.productName == "StandardSetup")
            {
                errorMsg += "ProductName is either null or StandardSetup.\n";
            }

            if ( PlayerSettings.iOS.iOSManualProvisioningProfileID == null || PlayerSettings.iOS.iOSManualProvisioningProfileID == string.Empty)
            {
                errorMsg += "iOS Provision profile has errors.\n";
            }

            if ( PlayerSettings.iOS.iOSManualProvisioningProfileType != ProvisioningProfileType.Distribution)
            {
                errorMsg += "Warning: iOS Provision profile type in iOS Player Settings is not Distribution. If this is intentional, please ignore.\n";
            }

            if ( errorMsg != string.Empty)
            {
                errorMsg = "In Player Settings:\n" + errorMsg + "¯\\_(ツ)_/¯";
                Debug.LogError( errorMsg);
                throw new Exception( errorMsg);
            }
            else
            {
                Debug.Log( "All good! Cheers.");
            }    
        }
    }
}
