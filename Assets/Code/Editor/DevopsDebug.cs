using System;
using System.IO;
using System.Text;
using System.Net.Http;
using System.Net.Http.Headers;

using UnityEngine;
using UnityEditor;

namespace GybeGames.Devops
{
    public class DevopsDebug
    {
        /**
   * Use only for debugging purposes on editor.
   */
        [MenuItem("Build/DebugProperties")]
        public static void DebugSettings()
        {
            Debug.Log( Directory.GetCurrentDirectory());
            Debug.Log(Directory.Exists( "../ProjectCaches"));
        }

        [MenuItem("Build/DebugIOSBuild")]
        public static void DebugIOSSettings()
        {

            BuildPlayerOptions buildPlayerOptions = new BuildPlayerOptions();
            buildPlayerOptions.targetGroup = BuildTargetGroup.iOS;
            buildPlayerOptions.target = BuildTarget.iOS;
            buildPlayerOptions.options = BuildOptions.None;
            buildPlayerOptions.scenes = File.Exists("Assets/Elephant/elephant_scene.unity") ?
                new[] { "Assets/Elephant/elephant_scene.unity", "Assets/Level/Scenes/MainScene.unity", "Assets/Level/Scenes/GameScene.unity" }
                : new[] { "Assets/Level/Scenes/MainScene.unity", "Assets/Level/Scenes/GameScene.unity" };
            buildPlayerOptions.locationPathName = $"../builds/debug-ios-build";

            /*
            OnBeforeBuildEvent();
            BuildProject(buildPlayerOptions);
            OnAfterBuildEvent();
            */
        }

        [MenuItem("Build/DebugRandomStuff")]
        public static void DebugRandom()
        {
            UnityEngine.Debug.Log(1);
            string notionTableId = "9fe0c3925dac4ad585acedc4387391fa";
            string notionToken = "secret_YECMCfTfOFTvlaJJbHZ32pKMtb9oXeeRQ6bOOnxM4Pf";

            using (var client = new HttpClient())
            {
                try
                {
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", notionToken);
                    client.DefaultRequestHeaders.Add("Notion-Version", "2021-08-16");
                    var task = client.PostAsync($"https://api.notion.com/v1/databases/{notionTableId}/query", new StringContent("", Encoding.UTF8, "application/json"));
                    task.Wait();

                    var readTask = task.Result.Content.ReadAsStringAsync();
                    readTask.Wait();

                    string values = string.Empty;
                    int lastIndex = 0;
                    string result = readTask.Result;
                    while (true)
                    {
                        int index = result.IndexOf("content", lastIndex);
                        if (index < 0)
                        {
                            break;
                        }

                        int commaIndex = result.IndexOf(",", index);
                        if (commaIndex < 0)
                            break;
                        values += result.Substring(index + 9, commaIndex - (index + 9)).Replace("\"", "").Replace("\"", "");

                        lastIndex = commaIndex;

                        index = result.IndexOf("content", lastIndex);
                        if (index < 0)
                        {
                            break;
                        }
                        commaIndex = result.IndexOf(",", index);
                        if (commaIndex < 0)
                            break;
                        values += " - " + result.Substring(index + 9, commaIndex - (index + 9)).Replace("\"", "").Replace("\"", "");

                        values += "\n";

                        lastIndex = commaIndex;
                    }

                    UnityEngine.Debug.Log(readTask.Result);
                    UnityEngine.Debug.Log(values);

                    int id = 1;
                    client.DefaultRequestHeaders.Clear();
                    var appleTask = client.GetAsync($"https://api.appstoreconnect.apple.com/v1/appStoreVersions/{id}");
                    appleTask.Wait();


                    //app store

                }
                catch (Exception e)
                {
                    UnityEngine.Debug.Log(e);
                }
            }
            UnityEngine.Debug.Log(2);


        }

        [MenuItem("Build/DebugAndroidBuild")]
        public static void DebugAndroidSettings()
        {
            // AndroidSettingsHandler.SetAndroidKeystorePasswords();
            UnityEngine.Debug.Log(PlayerSettings.Android.bundleVersionCode);
            UnityEngine.Debug.Log(PlayerSettings.bundleVersion);

            PlayerSettings.Android.useCustomKeystore = true;
            UnityEngine.Debug.Log(PlayerSettings.Android.keystoreName);
        }
    }
}