using System;
using System.IO;
using System.Text;

using UnityEditor;

namespace GybeGames.Devops.Android
{
    public class AndroidSettingsHandler
    {
        const string PASSWORD = "password";

        public static void SetAndroidKeystorePasswords()
        {
            string[] files = Directory.GetFiles(".");
            string passwordFile = string.Empty;
            for ( int i = 0; i < files.Length; ++i)
            {
                if ( files[ i].Contains( PASSWORD) && files[ i].Contains( ".txt"))
                {
                    passwordFile = files[ i];
                }
            }

            if (passwordFile == string.Empty)
                throw new Exception( "Password text file could not be found. Make sure the file has name password in it and has extension \".txt\"");

            string password = string.Empty;
            using ( FileStream f = new FileStream( passwordFile, FileMode.Open, FileAccess.Read))
            {
                using ( StreamReader r = new StreamReader( f, Encoding.UTF8))
                {
                    password = r.ReadToEnd();
                }
            }

            if ( password == string.Empty)
                throw new Exception( "Password found within text file is empty! Something is messed up! Check project folders in Gitlab...");

            string[] lines = password.Split('\n');
            password = lines[0];

            PlayerSettings.Android.keystorePass = password;
            PlayerSettings.Android.keyaliasPass = password;
        }

        public static void SetKeystoreAndAliasName()
        {
            string path = Directory.GetCurrentDirectory();
            string[] directories = path.Split(Path.DirectorySeparatorChar);
            string repoName = directories[directories.Length - 1];
            repoName = repoName.ToLower();
            PlayerSettings.Android.useCustomKeystore = true;
            PlayerSettings.Android.keystoreName = "user.keystore";
            PlayerSettings.Android.keyaliasName = $"{repoName}alias";
        }
    }
}