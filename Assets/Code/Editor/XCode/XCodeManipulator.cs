using System.IO;
using UnityEditor.iOS.Xcode;

namespace GybeGames.Devops.XCode
{
    public class XCodeManipulator
    {
        private const string SWIFT_LAN_VER = "5";
        private const string SWIFT_VER = "5.1";

        public static void ManipulateXCode( string path, bool launch = false)
        {
            string projectPath = path + "/Unity-iPhone.xcodeproj/project.pbxproj";

            PBXProject xcodeProj = new PBXProject();
            xcodeProj.ReadFromFile( projectPath);

            string unityFrameworkGUID = xcodeProj.GetUnityFrameworkTargetGuid();
            string unityIphoneGUID = xcodeProj.GetUnityMainTargetGuid();

            InfoPlistManipulator(path);

            xcodeProj.SetBuildProperty( unityFrameworkGUID, BuildSettings.ALWAYS_EMBED_SWIFT_STANDARD_LIBRARIES, "NO");
            //if ( launch)
            //{
                xcodeProj.SetBuildProperty( unityFrameworkGUID, BuildSettings.ENABLE_BITCODE, "NO");
                //xcodeProj.SetBuildProperty( unityFrameworkGUID, BuildSettings.SWIFT_LANGUAGE_VERSION, SWIFT_LAN_VER);

                xcodeProj.SetBuildProperty( unityIphoneGUID, BuildSettings.ENABLE_BITCODE, "NO");
                xcodeProj.AddFrameworkToProject( unityIphoneGUID, "UnityFramework.framework", false);
            //}

            xcodeProj.WriteToFile( projectPath);
        }

        private static void InfoPlistManipulator(string path)
        {
            string plistPath = path + BuildSettings.INFO_PLIST_POSTFIX;
            PlistDocument plist = new PlistDocument();
            plist.ReadFromString(File.ReadAllText(plistPath));

            // Get root
            PlistElementDict rootDict = plist.root;

            rootDict.SetBoolean(BuildSettings.ITS_APP_USES_NON_EXEMPT_ENCRYPTION, false);

            // Write to file
            File.WriteAllText(plistPath, plist.WriteToString());
        }
    }
}
