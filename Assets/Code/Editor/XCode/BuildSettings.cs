using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GybeGames.Devops.XCode
{
    public class BuildSettings
    {
        public const string SWIFT_LANGUAGE_VERSION = "SWIFT_LANGUGE_VERSION";
        public const string ALWAYS_EMBED_SWIFT_STANDARD_LIBRARIES = "ALWAYS_EMBED_SWIFT_STANDARD_LIBRARIES";
        public const string SWIFT_VERSION = "SWIFT_VERSION";
        public const string ENABLE_BITCODE = "ENABLE_BITCODE";

        #region Info plist

        public const string INFO_PLIST_POSTFIX = "/Info.plist";

        public const string LS_APPLICATION_QUERIES_SCHEMES = "LSApplicationQueriesSchemes";

        public const string ITS_APP_USES_NON_EXEMPT_ENCRYPTION = "ITSAppUsesNonExemptEncryption";

        #endregion
    }
}
